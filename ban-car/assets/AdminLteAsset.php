<?php

use yii\web\AssetBundle;

class AdminLtePluginAsset extends AssetBundle
{
    public $sourcePath = '@vendor/almasaeed2010/adminlte/plugins';
    public $js = [
        'datatables/dataTables.bootstrap.min.js',
        '@app/web/js/main-login.js',
    ];
    public $css = [
        'datatables/dataTables.bootstrap.css',
        'js/jquery-1.11.0.min.js',
    ];
    public $depends = [
        'dmstr\web\AdminLteAsset',
    ];
}
