<?php    
namespace app\components;
use Yii;
use app\models\Audit;
use app\models\AuditHistory;

class RegistroMovimientos
{
    public function init(){
        parent::init();
    }

    public function registrarMovimiento($legajo,$accion,$modulo, $descripcion) {
        
        $modelAud = new Audit();
        $audit_Tabla = Audit::tableName();

        $control  = Audit::find()->where("usuario_legajo_AUD=:usuario_legajo_AUD", [":usuario_legajo_AUD" => $legajo])->one();
        if (isset($control)) {
          
          Yii::$app->db->createCommand()->update($audit_Tabla, 
            ['accion_AUD'              =>$accion,
            'modulo_AUD'               =>$modulo, 
            'fecha_AUD'                =>date('Y-m-d'),
            'hora_AUD'                 => new \yii\db\Expression('NOW()'),
            'ip_AUD'                   =>Yii::$app->request->userIP,
            'descripcion_AUD'          =>$descripcion], 
            ['usuario_legajo_AUD' =>$legajo])
              ->execute();

        }elseif (!isset($control)) {
         $InsertArray[] = ['usuario_legajo_AUD'       =>$legajo, 
                          'accion_AUD'               =>$accion,
                          'modulo_AUD'               =>$modulo, 
                          'fecha_AUD'                =>date('Y-m-d'),
                          'hora_AUD'                 =>new \yii\db\Expression('NOW()'),
                          'ip_AUD'                   =>Yii::$app->request->userIP,
                          'descripcion_AUD'          =>$descripcion,
                    ];

        $AuditArray  = ['usuario_legajo_AUD','accion_AUD','modulo_AUD','fecha_AUD','hora_AUD','ip_AUD','descripcion_AUD'];
                
        $InsertarCampos = Yii::$app->db->createCommand()
              ->batchInsert($audit_Tabla, $AuditArray, $InsertArray)
              ->execute();
        }
        
    }

    public function history($usuario_insert){
      
      $modelHistory = new AuditHistory();
      $auditH_Tabla = AuditHistory::tableName();

      $usuario  = Audit::find()->where("usuario_legajo_AUD=:usuario_legajo_AUD", [":usuario_legajo_AUD" => $usuario_insert])->one();
      if (isset($usuario)) {
        
        $InsertArray[] = ['usuario_legajo_AUD'       =>$usuario_insert, 
                          'accion_AUD'               =>$usuario->accion_AUD,
                          'modulo_AUD'               =>$usuario->modulo_AUD, 
                          'fecha_AUD'                =>$usuario->fecha_AUD,
                          'hora_AUD'                 =>$usuario->hora_AUD,
                          'ip_AUD'                   =>$usuario->ip_AUD,
                          'descripcion_AUD'          =>$usuario->descripcion_AUD,
                    ];
         $AuditArray  = ['usuario_legajo_AUD','accion_AUD','modulo_AUD','fecha_AUD','hora_AUD','ip_AUD','descripcion_AUD'];
                
        $InsertarCampos = Yii::$app->db->createCommand()
              ->batchInsert($auditH_Tabla, $AuditArray, $InsertArray)
              ->execute();           
      }

    }
}