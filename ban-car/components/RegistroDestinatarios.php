<?php    
namespace app\components;
use Yii;
use app\models\Destinatarios;
use app\models\Localidades;

class RegistroDestinatarios
{
    public function init(){
        parent::init();
    }

    public function registrarDestinatarios($cuil) {
        
        $modelDest = new Destinatarios();
        $dest_Tabla = Destinatarios::tableName();

        $control  = Destinatarios::find()->where("Cuit_Cuil=:CUIL", [":CUIL" => $cuil])->one();
        
        if (!isset($control)) {
            $datos = "http://srv-wsl-prod:8080/rest-api/rest/index/users?cuil=".$cuil;
            $data = json_decode(file_get_contents($datos), true);
            
            foreach ($data as $key=>$value) {
                $id_localidad = Localidades::find()->where(['like', 'nombre', $data[$key]['localidad_nombre']])->one();
                $InsertArray[] = ['DNI'                             =>$data[$key]['numero_documento'],
                                 'Tipo_documento'                   =>$data[$key]['tipo_documento'],
                                 'Nombre_RazonSocial'               =>$data[$key]['nombre'],
                                 'Cuit_Cuil'                        =>$data[$key]['cuil'],
                                 'Sexo'                             =>$data[$key]['sexo'],
                                 'Fecha_Nacimiento'                 =>$data[$key]['FechNac'],
                                 'Ciudadania'                       =>$data[$key]['ciudadania'],
                                 'Telefono'                         =>$data[$key]['telefono'],
                                 'Celular'                          =>$data[$key]['celular'],
                                 'Cod_postal'                       =>$data[$key]['codigo_postal'],
                                 'Email'                            =>$data[$key]['mail'],
                                 'Estado_civil'                     =>$data[$key]['estado_civil'],
                                 'Cod_actividad'                    =>$data[$key]['cod_actividad'],
                                 'Direccion'                        =>$data[$key]['direccion'], 
                                 'Sucursal'                         =>$data[$key]['sucursal'],
                                 'Cuenta'                           =>$data[$key]['cuenta'],
                                 'Grupo'                            =>$data[$key]['grupo'],
                                 'Subgrupo'                         =>$data[$key]['subgrupo'],
                                 'Tipo_cuenta'                      =>$data[$key]['tipocuenta'],
                                 'Codigo_pais'                      =>$data[$key]['pais_codigo'],
                                 'ID_Provincia'                     =>$data[$key]['provincia_codigo'],
                                 'ID_Localidad'                     =>$id_localidad->id,
                                 'Tipo_domicilio'                   =>$data[$key]['tipo_domicilio'],
                           ];
          
        }
        $DestArray  = ['DNI','Tipo_documento','Nombre_RazonSocial','Cuit_Cuil','Sexo','Fecha_Nacimiento','Ciudadania','Telefono','Celular',
        'Cod_postal','Email','Estado_civil','Cod_actividad','Direccion','Sucursal','Cuenta',
        'Grupo','Subgrupo','Tipo_cuenta','Codigo_pais','ID_Provincia','ID_Localidad','Tipo_domicilio'];
        
        $InsertarCampos = Yii::$app->db->createCommand()
              ->batchInsert($dest_Tabla, $DestArray, $InsertArray)
              ->execute(); 
        }
        

    }
}
