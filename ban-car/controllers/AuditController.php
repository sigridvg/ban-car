<?php

namespace app\controllers;

use Yii;
use app\models\Audit;
use app\models\AuditSearch;
use app\models\AuditSearchLogin;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use app\controllers\UsuariosController;
use app\models\Usuarios;
use yii\data\SqlDataProvider;
use app\models\AuditHistory;

/**
 * AuditController implements the CRUD actions for Audit model.
 */
class AuditController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
       return [
                'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout','login','index','view','create','update','delete'],
                    'rules' => [
                            [
                                
                                'actions' => ['logout','index','login'],
                                'allow' => true,
                                'matchCallback' => function ($rule, $action) {
                                    return Usuarios::isUserAdmin(Yii::$app->user->identity->id);
                                },
                            ],
                            [
                                'actions' => ['create','update','delete','view'],
                                'allow' => false,
                                'roles' => function ($rule, $action) {
                                    Usuarios::isUserAdmin();
                                    Usuarios::isUserOperador();
                                    Usuarios::isUserfirmante();
                                    return Yii::$app->user->identity->id;
                                },
                                'denyCallback' => function()
                                {
                                    Yii::$app->session->setFlash('danger', \Yii::t('user', 'No posee permisos para acceder a esta sección.'));
                                    return $this->redirect(['site/index']);
                                }
                            ],
                 ],
             ],
             'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    //'delete' => ['POST'],
                ],
           ],
        ];
    }

    /**
     * Lists all Audit models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new AuditSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
     
        $searchLogin = new AuditSearchLogin();
        $dataproviderLogin = $searchLogin->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider, 'dataProviderLogin' => $dataproviderLogin
        ]);
/*
         $sql = new SqlDataProvider(['sql'=>'select * from audit 
                union
                select * from audit_history ']) ;
         $dataProvider = $sql->getModels();*/


        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Audit model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Audit model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Audit();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->ID_AUD]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Audit model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->ID_AUD]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Audit model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Audit model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Audit the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        $model_h = new AuditHistory();
        if (($model = Audit::findOne($id)) !== null or ($model_h = AuditHistory::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
