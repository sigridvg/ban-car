<?php

namespace app\controllers;

use Throwable;
use Yii;
use PhpOffice\PhpWord\PhpWord;
use PhpOffice\PhpWord\Style\Language;
use app\models\CartasDocumento;
use app\models\CartasSearch;
use app\models\DespachoSearch;
use app\models\Destinatarios;
use app\models\Localidades;
use app\models\Plantillas;
use app\models\Provincias;
use app\models\Sucursales;
use app\models\Sectores;
use PhpOffice\PhpWord\Exception\CopyFileException;
use PhpOffice\PhpWord\Exception\CreateTemporaryFileException;
use PhpOffice\PhpWord\TemplateProcessor;
use Picqer\Barcode\BarcodeGeneratorPNG;
use app\models\Usuarios;
use PhpOffice\PhpWord\Settings;
use yii\db\StaleObjectException;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use app\components\RegistroMovimientos;
use app\components\RegistroDestinatarios;
use yii\web\Response;
use yii\helpers\FileHelper;
use yii\i18n\Formatter;
use Luecano\NumeroALetras\NumeroALetras;

/**
 * CartasController implements the CRUD actions for Cartas model.
 */
class CartasController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return ['access' => [
            'class' => AccessControl::className(),
            'only' => ['logout', 'login', 'index', 'create', 'delete', 'update'],
            'rules' => [
                [
                    'actions' => ['logout', 'index', 'create', 'delete', 'update', 'PendingDelivery', 'PendingRevision'],
                    'allow' => true,
                    'roles' => ['@'],
                    'matchCallback' => function ($rule, $action) {
                        return Usuarios::isUserOperador(Yii::$app->user->identity->id);
                    },
                ],
                [
                    'actions' => ['logout', 'index', 'create', 'delete', 'Pending','update'],
                    'allow' => true,
                    'roles' => ['@'],
                    'matchCallback' => function ($rule, $action) {
                        return Usuarios::isUserFirmante(Yii::$app->user->identity->id);
                    },
                ],
                [
                    'actions' => ['logout', 'index', 'create', 'delete', 'update', 'PendingDelivery', 'PendingRevision', 'Pending'],
                    'allow' => true,
                    'roles' => ['@'],
                    'matchCallback' => function ($rule, $action) {
                        return Usuarios::isUserAdmin(Yii::$app->user->identity->id);
                    },
                ],
                [
                    'actions' => ['login', 'logout'],
                    'allow' => true,
                    'roles' => ['?'],

                ],
            ],
        ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all CartaDocumento models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CartasSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionPending()
    {
        $searchModel = new CartasSearch();
        $dataProvider = $searchModel->search(['CartasSearch' => ['status' => CartasDocumento::STATUS_PENDING_SIGNATURE, 'id_firmante' => Yii::$app->user->identity->getId()]]);

        return $this->render('pending', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionPendingDelivery()
    {

        $searchModel = new DespachoSearch();
       $dataProvider = $searchModel->search(['CartasSearch' => ['status' => CartasDocumento::STATUS_SIGNED]]);

        return $this->render('pendingDelivery', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionPendingRevision()
    {
        $searchModel = new CartasSearch();
        $dataProvider = $searchModel->search(['CartasSearch' => ['status' => CartasDocumento::STATUS_PENDING_REVISION]]);

        return $this->render('pendingRevision', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
 
    /**
     * Updates an existing Usuarios model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $modelDestinatarios = new Destinatarios();
        
        
        $a = explode(';', $model->campos);
        $b = implode(':',array_values($a));
        $c = explode(':', $b);
        
     
        $par = [];
        $impar = [];
        $i = 1;

        foreach ($c as $key) { 
            if (is_int($i/2) ) {
             $i--;
             array_push($par, $c[$i]); 
             $i++;
            }elseif (is_float($i/2)) {
            $i--;
              array_push($impar, $c[$i]);
            $i++;
            };
            $i++;
        };
        if ($impar != null && $par != null) {
          $datos = array_combine($impar,$par);
        }else{
          $datos = null;
        }
          
        $model->updateAttributes(['status' => CartasDocumento::STATUS_PENDING_REVISION]);
        
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            
            $c = str_replace(array(','), '.', Yii::$app->request->getBodyParams());
            $campos = json_encode($c);
            $c = explode(',', $campos);
            $campos2 = array_slice($c, 7); 
            $var =str_replace(array('}','"','    '), '', $campos2);
            $campos3 = implode(';', $var); 
 
            $model->updateAttributes(['campos' => $campos3, 'status' => CartasDocumento::STATUS_PENDING_SIGNATURE,'update'=> 1]); 

            //GRABA REGISTRO EN AUDIT ---------------------------------------------------------------------------

            $destinatario = Destinatarios::find()->where("Cuit_Cuil=:Cuit_Cuil", [":Cuit_Cuil" => $model->destinatario])->andWhere("Cuenta=:cuenta", [":cuenta" => $model->nro_cuenta])->one();
            $destinatario_nombre = $destinatario->Nombre_RazonSocial;

            $usuario = Usuarios::find()->where("IdUsuarios=:IdUsuarios", [":IdUsuarios" => Yii::$app->user->identity->id])->one();
            $legajo = $usuario->LegajoUs;
            $usuario_insert = $legajo;

            RegistroMovimientos::history($usuario_insert);
            RegistroMovimientos::registrarMovimiento($legajo, 'ACTUALIZAR', 'CARTA DOCUMENTO', 'Actualizo Carta Nro° ' . $model->id . ' Destinatario: ' . $destinatario_nombre);
            //------------------------- ---------------------------------------------------------------------------
            
            
             return $this->redirect(['index']);
        }
        return $this->render('update', [
               'model' => $model,'modelDest'=>$modelDestinatarios, 'campos' =>$datos]);
        
}

    /**
     * Deletes an existing Usuarios model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     * @throws Throwable
     * @throws StaleObjectException
     */
    public function actionDelete($id)
    {
        

        //GRABA REGISTRO EN AUDIT --------------------------------------------------------------------------
        $destinatario = Destinatarios::find()->where("Cuit_Cuil=:Cuit_Cuil", [":Cuit_Cuil" => $model->destinatario])->andWhere("Cuenta=:cuenta", [":cuenta" => $model->nro_cuenta])->one();
        $destinatario_nombre = $destinatario->Nombre_RazonSocial;

        $usuario = Usuarios::find()->where("IdUsuarios=:IdUsuarios", [":IdUsuarios" => Yii::$app->user->identity->id])->one();
        $legajo = $usuario->LegajoUs;
        $usuario_insert = $legajo;

        RegistroMovimientos::history($usuario_insert);
        RegistroMovimientos::registrarMovimiento($legajo, 'BORRAR', 'CARTA DOCUMENTO', 'borró carta documento N° ' . $model->id . ' Destinatario: ' . $destinatario_nombre);
        //---------------------------------------------------------------------------------------------------
        $this->findModel($id)->delete();
        return $this->redirect(['index']);
    }

    /**
     * Finds the Usuarios model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CartasDocumento the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CartasDocumento::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    protected function findPlantilla($id_plantilla)
    {
        if (($model = Plantillas::findOne($id_plantilla)) !== null) {
            return $model->id_plantilla;
        }
    }

    public function actionArmarPlantilla(){
        $model = new CartasDocumento();
        $modelDestinatarios = new Destinatarios();
        $modelPlantilla = new Plantillas();
        
        //$plantilla = Yii::$app->request->get('id_plantilla');

            if ($model->load(Yii::$app->request->post()) ) {
               

                $model->id_usuario = Yii::$app->user->identity->getId();
                $sector = Usuarios::find()->where(['IdUsuarios' => $model->id_usuario])->one();
                $doc = str_replace(array(' '), ',', $model->documentacion);
                

                $model->sector = $sector->IdSectores;
                $model->id_plantilla = $model->id_plantilla;
                

                $c = str_replace(array('.'), '', Yii::$app->request->getBodyParams());
                $d = str_replace(array(','), '.', $c);
                $campos = json_encode($d);
                $c2 = explode(',', $campos);
                $campos2 = array_slice($c2, 7); 
                $var =str_replace(array('}','"'), '', $campos2);
                $campos3 = implode(';', $var); 
                               
                $model->campos = $campos3;   

               
                if ($model->save()) {
                    //$model->documentacion = $documentacion;
                    var_dump($doc);
                    exit();
                   
                    Yii::$app->session->setFlash('cartaCreada');
                    $firmante = Usuarios::find()->where(['IdUsuarios' => $model->id_firmante])->one();

                    if ($model->id_firmante == null) {
                        $model->updateAttributes(['status' => CartasDocumento::STATUS_SIGNED]);

                    } else {
                        $this->notificarFirmante($firmante, $model);
                    }
                    
                    $model->id_OCA = self::createID($model->id);
                    
                    //GRABA REGISTRO EN AUDIT --------------------------------------------------------------------------
                    $destinatario = Destinatarios::find()->where("Cuit_Cuil=:Cuit_Cuil", [":Cuit_Cuil" => $model->destinatario])->andWhere("Cuenta=:cuenta", [":cuenta" => $model->nro_cuenta])->one();
                    $destinatario_nombre = $destinatario->Nombre_RazonSocial;

                    $usuario = Usuarios::find()->where("IdUsuarios=:IdUsuarios", [":IdUsuarios" => Yii::$app->user->identity->getId()])->one();
                    $legajo = $usuario->LegajoUs;
                    $usuario_insert = $legajo;

                    RegistroMovimientos::history($usuario_insert);
                    RegistroMovimientos::registrarMovimiento($legajo, 'CREAR', 'CARTA DOCUMENTO', 'Creacion de carta documento. Destinatario: ' . $destinatario_nombre);
                    //--------------------------------------------------------------------------------------------------- 

                } else {    
                    $plantilla = Plantillas::find()->where(['id' => $model->id_plantilla])->one();
                    foreach($plantilla as $k=>$v) {
                        $arr[$k] = $v;
                    }
                    unset($arr['__PHP_Incomplete_Class_Name']);
                    $c2 = array_slice($arr, 5);

                   return $this->render('create', ['model' => $model, 'plantilla' =>$plantilla,'campos'=>$c2,'modelDest'=>$modelDestinatarios]);
                }
            }
      
          if (isset($_POST['depdrop_parents'])) {
            $parents = $_POST['depdrop_parents'];
            if ($parents != null) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                $out = self::getPlantillas($parents[0], Yii::$app->user->identity->getIdSectores());

                return ['output' => $out, 'selected' => ' '];
            }
        }

          return $this->render('motivo', ['model' => $model,'modelPlantilla'=>$modelPlantilla,'modelDest'=>$modelDestinatarios]);
     }  

    public function actionCreate()
    {
        $model = new CartasDocumento();
        $modelDestinatarios = new Destinatarios();
        $plantilla = $this->findPlantilla(Yii::$app->request->get('id_plantilla'));
        
        return $this->render('create', ['model' => $model,'plantilla' =>$plantilla, 'modelDest'=>$modelDestinatarios]);
    }

    public function actionFirmar($id)
    {
        try {
            $model = $this->findModel($id);
            Yii::$app->session->setFlash('statusUpdated');
            $model->updateAttributes(['status' => CartasDocumento::STATUS_SIGNED]);
           

            //GRABA REGISTRO EN AUDIT --------------------------------------------------------------------------
             $destinatario = Destinatarios::find()->where("Cuit_Cuil=:Cuit_Cuil", [":Cuit_Cuil" => $model->destinatario])->andWhere("Cuenta=:cuenta", [":cuenta" => $model->nro_cuenta])->one();
            $destinatario_nombre = $destinatario->Nombre_RazonSocial;

            $usuario = Usuarios::find()->where("IdUsuarios=:IdUsuarios", [":IdUsuarios" => Yii::$app->user->identity->id])->one();
            $legajo = $usuario->LegajoUs;
            $usuario_insert = $legajo;

            RegistroMovimientos::history($usuario_insert);
            RegistroMovimientos::registrarMovimiento($legajo, 'FIRMAR', 'CARTA DOCUMENTO', 'Firmó carta documento N° ' . $model->id . ' Destinatario: ' . $destinatario_nombre);
            //--------------------------------------------------------------------------------------------------- 


        } catch (NotFoundHttpException $e) {
            Yii::$app->session->setFlash('deleteFailed');
        }
        return $this->redirect(['index']);
    }

    public function actionFirmaMultiple()
    {
        $id = Yii::$app->request->get('item');
        var_dump($id);
        exit();
        try {
            $model = $this->findModel($id);
            Yii::$app->session->setFlash('statusUpdated');
            $model->updateAttributes(['status' => CartasDocumento::STATUS_SIGNED]);
           

            //GRABA REGISTRO EN AUDIT --------------------------------------------------------------------------
             $destinatario = Destinatarios::find()->where("Cuit_Cuil=:Cuit_Cuil", [":Cuit_Cuil" => $model->destinatario])->andWhere("Cuenta=:cuenta", [":cuenta" => $model->nro_cuenta])->one();
            $destinatario_nombre = $destinatario->Nombre_RazonSocial;

            $usuario = Usuarios::find()->where("IdUsuarios=:IdUsuarios", [":IdUsuarios" => Yii::$app->user->identity->id])->one();
            $legajo = $usuario->LegajoUs;
            $usuario_insert = $legajo;

            RegistroMovimientos::history($usuario_insert);
            RegistroMovimientos::registrarMovimiento($legajo, 'FIRMAR', 'CARTA DOCUMENTO', 'Firmó carta documento N° ' . $model->id . ' Destinatario: ' . $destinatario_nombre);
            //--------------------------------------------------------------------------------------------------- 


        } catch (NotFoundHttpException $e) {
            Yii::$app->session->setFlash('deleteFailed');
        }
        return $this->redirect(['index']);
    }

    
    
    public function actionRevisar($id)
    {
        $model = $this->findModel($id);
  
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
                    
        try {

            Yii::$app->session->setFlash('statusUpdated', "Carta enviada a revision con éxito. ");

            $model->updateAttributes(['status' => CartasDocumento::STATUS_PENDING_REVISION]);

            //GRABA REGISTRO EN AUDIT --------------------------------------------------------------------------
            $destinatario = Destinatarios::find()->where("Cuit_Cuil=:Cuit_Cuil", [":Cuit_Cuil" => $model->destinatario])->andWhere("Cuenta=:cuenta", [":cuenta" => $model->nro_cuenta])->one();
            $destinatario_nombre = $destinatario->Nombre_RazonSocial;

            $usuario = Usuarios::find()->where("IdUsuarios=:IdUsuarios", [":IdUsuarios" => Yii::$app->user->identity->id])->one();
            $legajo = $usuario->LegajoUs;
            $usuario_insert = $legajo;

            RegistroMovimientos::history($usuario_insert);
            RegistroMovimientos::registrarMovimiento($legajo, 'REVISO', 'CARTA DOCUMENTO', 'Revisó carta documento N° ' . $model->id . ' Destinatario: ' . $destinatario_nombre);
            //--------------------------------------------------------------------------------------------------- 


        } catch (NotFoundHttpException $e) {
            Yii::$app->session->setFlash('revisionFailed');
        }
         return $this->redirect(['pending']);
         
        }
        
        return $this->render('revision', ['model' => $model]);
     
    }
    
       public function actionConfigurar(){
        
        $id = CartasDocumento::find()->max('id');
        
        $model = $this->findModel($id);
  
        if ($model->load(Yii::$app->request->post()) && $model->save()) {   
            
             //GRABA REGISTRO EN AUDIT --------------------------------------------------------------------------
            $destinatario = Destinatarios::find()->where("Cuit_Cuil=:Cuit_Cuil", [":Cuit_Cuil" => $model->destinatario])->andWhere("Cuenta=:cuenta", [":cuenta" => $model->nro_cuenta])->one();
            $destinatario_nombre = $destinatario->Nombre_RazonSocial;

            $usuario = Usuarios::find()->where("IdUsuarios=:IdUsuarios", [":IdUsuarios" => Yii::$app->user->identity->id])->one();
            $legajo = $usuario->LegajoUs;
            $usuario_insert = $legajo;

            RegistroMovimientos::history($usuario_insert);
            RegistroMovimientos::registrarMovimiento($legajo, 'CONFIGURAR', 'CARTA DOCUMENTO', 'Se modifica el numero de seria/kit/Prox Carta ');
            //--------------------------------------------------------------------------------------------------- 
            
            Yii::$app->session->setFlash('configuracionExit');
        }
        
        return $this->render('configurar', ['model' => $model]);
    }
    
    public function actionDescargar($id)
    {
        Settings::setTempDir(Yii::getAlias('@app') . '/tmp');
        try {
            $model = $this->findModel($id);
            $this->createBarcode($model->id_OCA);
            $path = Yii::getAlias('@app') . '/cartas/';
            $filename = Yii::getAlias('@app') . '/cartas/' . $id . '.docx';
            try {
                if (FileHelper::createDirectory($path, $mode =0775, $recursive = \true)) {
                    if ($model->update == 1 and file_exists($filename)){
                            unlink($filename); 
                            $templateWord = $this->setTemplate($model, $id);
                            $templateWord->saveAs($filename);                       
                    } else {
                        $templateWord = $this->setTemplate($model, $id);
                        $templateWord->saveAs($filename);
                    }
                }
                if (file_exists($filename)) {
                    return Yii::$app->response->sendFile($filename, 'Carta Documento N°: ' . $id . '.docx');
                }

                //GRABA REGISTRO EN AUDIT --------------------------------------------------------------------------
                $destinatario = Destinatarios::find()->where("Cuit_Cuil=:Cuit_Cuil", [":Cuit_Cuil" => $model->destinatario])->andWhere("Cuenta=:cuenta", [":cuenta" => $model->nro_cuenta])->one();
                $destinatario_nombre = $destinatario->Nombre_RazonSocial;

                $usuario = Usuarios::find()->where("IdUsuarios=:IdUsuarios", [":IdUsuarios" => Yii::$app->user->identity->id])->one();
                $legajo = $usuario->LegajoUs;
                $usuario_insert = $legajo;

                RegistroMovimientos::history($usuario_insert);
                RegistroMovimientos::registrarMovimiento($legajo, 'DESCARGO', 'CARTA DOCUMENTO', 'Descargó carta documento N° ' . $model->id . ' Destinatario: ' . $destinatario_nombre);
                //---------------------------------------------------------------------------------------------------


            } catch (CopyFileException $e) {
                Yii::$app->session->setFlash('CopyFileException');
            } catch (CreateTemporaryFileException $e) {
                Yii::$app->session->setFlash('CreateTemporaryFileException');
            }
        } catch (NotFoundHttpException $e) {
            Yii::$app->session->setFlash('NotFound');
        }

        return $this->redirect(['pending-delivery']);
    }

    public function actionView($id)
    {
        $model = $this->findModel($id);
        $carta = CartasDocumento::find()->where("id=:id", [":id" => $model->id])->one();
        $numeroCuenta = $carta->nro_cuenta;

        //GRABA REGISTRO EN AUDIT ---------------------------------------------------------------------------
        $usuario = Usuarios::find()->where("IdUsuarios=:IdUsuarios", [":IdUsuarios" => Yii::$app->user->identity->id])->one();
        $legajo = $usuario->LegajoUs;
        $usuario_insert = $legajo;

        RegistroMovimientos::history($usuario_insert);
        RegistroMovimientos::registrarMovimiento($legajo, 'VISTA', 'CARTAS DOCUMENTO', 'Visualizacion Carta Documento N°: '. $model->id . ' cuenta: '. $numeroCuenta);
        //----------------------------------------------------------------------------------------------------
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionImprimir($id)
    {
        // CREATE DOCX---------------------------------------------------------------------------------------
        $model = $this->findModel($id);
        $this->createBarcode($model->id_OCA);
        //$templateWord = $this->setTemplate($model, $id);
            
        $path = Yii::getAlias('@app') . '/cartas/';
        $filename = Yii::getAlias('@app') . '/cartas/' . $id . '.docx';
        
        if (FileHelper::createDirectory($path, $mode =0775, $recursive = \true)) {
            if ($model->update == 1 and file_exists($filename)){
                unlink($filename); 
                $templateWord = $this->setTemplate($model, $id);
                $templateWord->saveAs($filename);                       
            } else {
                $templateWord = $this->setTemplate($model, $id);
                $templateWord->saveAs($filename);
            }
        }

        //GRABA REGISTRO EN AUDIT --------------------------------------------------------------------------
        $carta = CartasDocumento::find()->where("id=:id", [":id" => $model->id])->one();
        $numeroCuenta = $carta->nro_cuenta;

        $usuario = Usuarios::find()->where("IdUsuarios=:IdUsuarios", [":IdUsuarios" => Yii::$app->user->identity->id])->one();
        $legajo = $usuario->LegajoUs;
        $usuario_insert = $legajo;

        RegistroMovimientos::history($usuario_insert);
        RegistroMovimientos::registrarMovimiento($legajo, 'IMPRIMIR', 'CARTA DOCUMENTO', 'Imprimió carta documento N° ' . $model->id . ' Cuenta: ' . $numeroCuenta);
        //--------------------------------------------------------------------------------------------------- 
        // CREATE PDF ----------------------------------------------------------------------------------------
        $output = Yii::getAlias('@app') . '/cartas';
   
        $result = shell_exec("sudo soffice --headless --convert-to pdf:writer_pdf_Export $filename --outdir $output");
        
        if ($model->status == 'SIGNED') {
            $model->updateAttributes(['status' => CartasDocumento::STATUS_PRINTED]);
         }
        $pdfName = Yii::getAlias('@app') .'/cartas/'. $id .'.pdf';
        $file = 'Carta Documento N°: ' . $id . '.docx';
        return Yii::$app->response->sendFile($pdfName, '', ['inline' => true])->send();
    }

    /**
     * @param CartasDocumento $model
     * @param $id
     * @return TemplateProcessor
     * @throws CopyFileException
     * @throws CreateTemporaryFileException
     * @throws CopyFileException
     */
    public function setTemplate(CartasDocumento $model, $id)
    {
        $languageEnES = new Language(Language::ES_ES);
        $phpWord = new PhpWord();
        $phpWord->getSettings()->setThemeFontLang($languageEnES);

        $template = Plantillas::find()->where(['id' => $model->id_plantilla])->one();
        $templateWord = new TemplateProcessor(Yii::getAlias('@app') . '/plantillas/' . $template->archivo);

        $firmante = Usuarios::find()->where(['IdUsuarios' => $model->id_firmante])->one();
        $dest = Destinatarios::find()->where("Cuit_Cuil=:Cuit_Cuil", [":Cuit_Cuil" => $model->destinatario])->andWhere("Cuenta=:cuenta", [":cuenta" => $model->nro_cuenta])->one();
        $sucursal_cuenta= Sucursales::find()->where(['id' => $dest->Sucursal])->one();
        $sector = Sectores::find()->where(['ID_SECTORES' => $model->sector])->one();
        $id_oca = $model->id_OCA;
        //$destinatario = Destinatarios::find()->where(['ID_DES' => $dest->rio])->one();
        $sucursal = Sucursales::find()->where(['id' => $model->id_sucursal])->one();
        $localidad_sucursal = Localidades::find()->where(['id_pais' => $sucursal->id_pais])->andWhere(['codigo_postal'=> $sucursal->codigo_postal])->one();
        $provincia_sucursal = Provincias::find()->where(['id' => $localidad_sucursal->id_provincia])->one();
        
        $localidad_destinatario = Localidades::find()->where(['id' => $dest->ID_Localidad])->one();
        $provincia_destinatario = Provincias::find()->where(['id' => $dest->ID_Provincia])->one();

        $templateWord->setValue('emisor', $sucursal->nombre);
        $templateWord->setValue('domicilioEmisor', $sucursal->direccion);
        $templateWord->setValue('localidadEmisor', $localidad_sucursal->nombre);
        $templateWord->setValue('provinciaEmisor', $provincia_sucursal->nombre);
        $templateWord->setValue('CPEmisor', $localidad_sucursal->codigo_postal);
        
        $templateWord->setValue('fecha', $model->fecha);
        $templateWord->setValue('plantilla', $model->id_plantilla);
        $templateWord->setValue('sucursal', $sucursal->nombre);
        $templateWord->setValue('destinatario', $dest->Nombre_RazonSocial);
        $templateWord->setValue('domicilioDestinatario', $dest->Direccion);
        $templateWord->setValue('localidadDestinatario', $localidad_destinatario->nombre);
        $templateWord->setValue('provinciaDestinatario', $provincia_destinatario->nombre);
        $templateWord->setValue('CPDest', $dest->Cod_postal);
        $templateWord->setValue('id', $id_oca);
        $templateWord->setValue('nombre', $dest->Nombre_RazonSocial);
        $templateWord->setValue('cuil', $dest->Cuit_Cuil);
        $templateWord->setValue('cuenta', $dest->Cuenta);
        $templateWord->setValue('sector', $sector->Sectores);
        $templateWord->setValue('sucursal_cuenta', $sucursal_cuenta->id);
        $templateWord->setValue('tipo_cuenta', $dest->Tipo_cuenta == 'AC'? 'Caja de Ahorro':'Cuenta Corriente');
     
        
        $templateWord->setImageValue('barcode', Yii::getAlias('@app') . '/barcodes' . "/" . $id_oca . ".jpg");
        
        if (!is_null($firmante) AND $model->status == 'SIGNED') {
            $templateWord->setValue('nombre_firmante', $firmante->NombreUs . ' '. $firmante->ApellidoUs);
            $templateWord->setValue('documento_firmante', $firmante->documento);
            if ($firmante->signature != null) {
                $templateWord->setImageValue('firma', array('path' => Yii::getAlias('@webroot') . '/firmas' . "/" . $firmante->signature,'ratio' => false));
            }else{
                $templateWord->setValue('firma', '   ');
               
            }
        } else {
            $templateWord->setValue('firma', '   ');
             $templateWord->setValue('nombre_firmante', ' ');
            $templateWord->setValue('documento_firmante', ' ');
        }
        $formatter = new NumeroALetras();
        $d = str_replace('\r\n', ', ', $model->campos);
        $a = explode(';', $d);
        $b = implode(':',array_values($a));
        $c = explode(':', $b);
        
      
        
        $par = [];
        $impar = [];
        $i = 1;

        foreach ($c as $key) { 
            if (is_int($i/2) ) {
                $i--;
                array_push($par, $c[$i]); 
                $i++;
            }elseif (is_float($i/2)) {
                $i--;
                array_push($impar, $c[$i]);
                $i++;
            }
            $i++;
        }
          if ($impar != null && $par != null) {
            
            $datos = array_combine($impar,$par);
            
            foreach ($datos as $k => $v) {
                
                $templateWord->setValue("{$k}", htmlspecialchars($v));
                
                if($k == 'saldo'){
                    $float = (float)$v;
                    $templateWord->setValue('saldo_letras', strtolower($formatter->toMoney($float, 2, 'pesos', 'centavos')));
                }elseif($k == 'cant_dias'){
                    //$templateWord->setValue('dias_letras', strtolower(NumerosEnLetras::convertir($v, 'dias', true)));
                }elseif($k=='saldo_garantia'){
                    $float = (float)$v;
                    $templateWord->setValue('saldo_garantia_letras', strtolower($formatter->toMoney($float, 2, 'pesos', 'centavos')));
                }elseif($k == 'saldo_comision'){
                    $float = (float)$v;
                    $templateWord->setValue('saldo_comision_letras',strtolower($formatter->toMoney($float, 2, 'pesos', 'centavos')));
                }elseif($k == 'saldo_deudor' ){
                    $float = (float)$v;
                    $templateWord->setValue('saldo_deudor_letras', strtolower($formatter->toMoney($float, 2, 'pesos', 'centavos')));
                }elseif($k == 'saldo_acreedor'){
                    $float = (float)$v;
                    $templateWord->setValue('saldo_acreedor_letras', strtolower($formatter->toMoney($float, 2, 'pesos', 'centavos')));
                }elseif($k == 'documentacion'){
                    $value1 = str_replace('\r\n', ', ', $v);
                    $templateWord->setValue($k, $v);
                  
                }
             } 
          }
 
        return $templateWord;
    }

    public function createBarcode($id_oca)
    {
        $generator = new BarcodeGeneratorPNG();
        $barcode = $generator->getBarcode($id_oca, $generator::TYPE_CODE_128_A, 0.5, 10);
        
        $path = Yii::getAlias('@app') . '/barcodes/';
        if (FileHelper::createDirectory($path, $mode =0775, $recursive = \true)) {
           file_put_contents(Yii::getAlias('@app') . '/barcodes' . "/" . $id_oca . ".jpg", $barcode);
        }
        
    }
    
    public function createID($id){
        $model = $this->findModel($id);
        $carta = CartasDocumento::find()->where(['id' => ($id)])->one();  
        $numRandom = rand(0,10);
        if($id == 1){
            $id_oca = $carta->serie.$carta->kit.$carta->num_carta.'-'.$numRandom;
             $model->updateAttributes(['id_OCA'=>$id_oca]);
        }elseif($id>1){
            $cartaAnterior = CartasDocumento::find()->where(['id' => ($id)-1])->one();
            $id_oca = $cartaAnterior->serie.$cartaAnterior->kit.$cartaAnterior->num_sig_carta.'-'.$numRandom; 
            $model->updateAttributes(['id_OCA'=>$id_oca,'num_carta' => $cartaAnterior->num_sig_carta,'num_sig_carta'=>($cartaAnterior->num_sig_carta)+1,'serie'=>$cartaAnterior->serie, 'kit'=>$cartaAnterior->kit]);
        }
        return $id_oca;

    }

    public function notificarFirmante(Usuarios $firmante, CartasDocumento $carta)
    {
        $subject = "Cartas documento pendientes de aprobación";
        
        $body = "<br><br>Hola " . $firmante->NombreUs . "!<br><br>";
        $body .= "      Han solicitado tu aprobaci&oacute;n de la carta documento n&uacute;mero " . $carta->id_OCA . ". Para verla ingrese a BANCAR. hac&eacute; click " . "<a href='http://srv-bnc-prod/'>ac&aacute;</a><br>";
        $body .= "<br>Banco Rioja S.A.U.";
        $content = "<p>" . $subject . "</p>";
        $content .= "<p>" . $body . "</p>";

        $this->sendEmail($body, $subject, $content, $firmante->emailUs);
    }

    public function notificarRedactor(Usuarios $redactor, CartasDocumento $carta)
    {
        $subject = "Cartas documento pendientes de revisión";
        
        $body = "Hola " . $redactor->NombreUs . "!<br><br>";
        $body .= "      Han solicitado tu revisi&oacute;n de la carta documento n&uacute;mero " . $carta->id_OCA . ". Para verla ingrese a la solapa Pendientes de Revision.  hac&eacute; click aqui " . "<a href='http://srv-bnc-prod/'>Bancar</a><br>";
        $body .= "<br>Banco Rioja S.A.U.";
        $content = "<p>" . $subject . "</p>";
        $content .= "<p>" . $body . "</p>";

        $this->sendEmail($body, $subject, $content, $redactor->emailUs);
    }

    public function sendEmail($body, $subject, $content, $email)
    {
        Yii::$app->mailer->compose("@app/mail/layouts/html", ["content" => $content])
            ->setFrom(["noreply.bancarsystem@bancorioja.com" => "Notificaciones BAN.CAR"])
            ->setTo($email)
            ->setSubject($subject)
            ->setHtmlBody(nl2br($body))
            ->send();
    }

    public function getPlantillas($id, $sector){
        $data = Plantillas::find()->where(['motivo_plantilla' => $id])->andFilterWhere(['sector'=>$sector])->select(['id', 'nombre_archivo'])->asArray()->orderBy('id')->all();
        $out = array();

        for ($i = 0; $i < count($data); $i++) {
            $out[$i]['id'] = $data[$i]['id'];
            $out[$i]['name'] = $data[$i]['nombre_archivo'];
        }
        return $out;
    }

    public function actionDestinatariosList($q) {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            $out = ['results' => ['id' => '', 'text' => '','cuil' => '', 'cuenta' => '', 'tipocuenta' => '']];
            
            $destinatario = Destinatarios::find()->where(['Cuit_Cuil' => $q])->count();
            
            if (!is_null($q) && $destinatario>0) {
                $data = self::getDestinatario($q);
                $out['results'] = array_values($data);
            }
            elseif ($destinatario == 0) {
               //Carga en la BD
                RegistroDestinatarios::registrarDestinatarios($q);
                $data = self::getDestinatario($q);
                $out['results'] = array_values($data);            
               
            }
            return $out;
        }
        
        public function actionDestinatariosCuentas() {
          if (isset($_POST['depdrop_parents'])) {
            $parents = $_POST['depdrop_parents'];
            if ($parents != null) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                $out = self::getCuenta($parents[0]);
                
                return ['output' => $out, 'selected' => ''];
            }
        }
        }

        
        public function getCuenta($cuil){
            $data = Destinatarios::find()->where(['Cuit_Cuil' => $cuil])->select(['ID_DES','Cuit_Cuil', 'Nombre_RazonSocial','Cuenta'])->asArray()->all();
            $out = array();

            for ($i = 0; $i < count($data); $i++) {
                $out[$i]['id'] = $data[$i]['Cuenta'];
                $out[$i]['name'] = $data[$i]['Cuenta'];
            }
            return $out;
        }

        
 
        public function getDestinatario($cuil){
                $query = new \yii\db\Query();
                $query->select('Cuit_Cuil as id, Nombre_RazonSocial AS text, DNI as dni, Cuenta as cuenta, Tipo_cuenta as tipocuenta')
                    ->from('destinatarios')
                    ->where(['Cuit_Cuil'=>$cuil])
                    ->groupBy('Cuit_Cuil');
                $command = $query->createCommand();
                $data = $command->queryAll();
                
        return $data;
        }
}
        
        



