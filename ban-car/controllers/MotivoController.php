<?php

namespace app\controllers;

use Yii;
use app\models\Motivo;
use app\models\motivosSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use app\models\Usuarios;
use app\components\RegistroMovimientos;

/**
 * MotivoController implements the CRUD actions for motivo model.
 */
class MotivoController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return ['access' => [
            'class' => AccessControl::className(),
            'only' => ['index', 'create', 'delete', 'update'],
            'rules' => [
                [
                    'actions' => ['index', 'create', 'delete', 'update'],
                    'allow' => true,
                    'roles' => ['@'],
                    'matchCallback' => function ($rule, $action) {
                        return Usuarios::isUserAdmin(Yii::$app->user->identity->id);
                    },
                ],
            ],
        ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all motivo models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new motivosSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single motivo model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
            $model = $this->findModel($id);
         //GRABA REGISTRO EN AUDIT ---------------------------------------------------------------------------
            $motivo = Motivo::find()->where("id=:id", [":id" => $model->id])->one();
             
            $usuario = Usuarios::find()->where("IdUsuarios=:IdUsuarios", [":IdUsuarios" => Yii::$app->user->identity->id])->one();
            $legajo = $usuario->LegajoUs;
            $usuario_insert = $legajo;

            RegistroMovimientos::history($usuario_insert);
            RegistroMovimientos::registrarMovimiento($legajo, 'VISUALIZAR', 'MOTIVO', 'Visualizar motivo° ' . $motivo->id . ' Descripcion: ' . $motivo->motivos);
            //------------------------- ---------------------------------------------------------------------------
        
       
             
        return $this->render('view', [
            'model' => $model,
        ]);
        
        
    }

    /**
     * Creates a new motivo model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Motivo();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
         
         //GRABA REGISTRO EN AUDIT ---------------------------------------------------------------------------
            $motivo = Motivo::find()->where("id=:id", [":id" => $model->id])->one();
             
            $usuario = Usuarios::find()->where("IdUsuarios=:IdUsuarios", [":IdUsuarios" => Yii::$app->user->identity->id])->one();
            $legajo = $usuario->LegajoUs;
            $usuario_insert = $legajo;

            RegistroMovimientos::history($usuario_insert);
            RegistroMovimientos::registrarMovimiento($legajo, 'CREAR', 'MOTIVO', 'Cargo nuevo motivo definidos para plantillas. Motivo° ' . $motivo->id . ' Descripcion: ' . $motivo->motivos);
            //------------------------- ---------------------------------------------------------------------------
        
            return $this->redirect(['view', 'id' => $model->id]);
        }    
        return $this->render('create', [
            'model' => $model,
        ]);
        
        
    }

    /**
     * Updates an existing motivo model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            
          //GRABA REGISTRO EN AUDIT ---------------------------------------------------------------------------
            $motivo = Motivo::find()->where("id=:id", [":id" => $model->id])->one();
             
            $usuario = Usuarios::find()->where("IdUsuarios=:IdUsuarios", [":IdUsuarios" => Yii::$app->user->identity->id])->one();
            $legajo = $usuario->LegajoUs;
            $usuario_insert = $legajo;

            RegistroMovimientos::history($usuario_insert);
            RegistroMovimientos::registrarMovimiento($legajo, 'ACTUALIZAR', 'MOTIVO', 'Actualizo motivo definidos para plantillas. Motivo° ' . $motivo->id . ' Descripcion: ' . $motivo->motivos);
            //------------------------- ---------------------------------------------------------------------------
            return $this->redirect(['view', 'id' => $model->id]);
            
            
        }    
        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing motivo model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
      
        
          //GRABA REGISTRO EN AUDIT ---------------------------------------------------------------------------
            $motivo = Motivo::find()->where(["id" => $id])->one();
                       
            $usuario = Usuarios::find()->where("IdUsuarios=:IdUsuarios", [":IdUsuarios" => Yii::$app->user->identity->id])->one();
            $legajo = $usuario->LegajoUs;
            $usuario_insert = $legajo;

            RegistroMovimientos::history($usuario_insert);
            RegistroMovimientos::registrarMovimiento($legajo, 'BORRAR', 'MOTIVO', 'Elimino motivo definido para plantillas. Motivo° ' . $id . ' Descripcion: ' . $motivo->motivos);
           //----------------------------------------------------------------------------------------------------
           $this->findModel($id)->delete();
        return $this->redirect(['index']);
    }

    /**
     * Finds the motivo model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return motivo the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Motivo::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
