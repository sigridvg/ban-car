<?php

namespace app\controllers;

use app\models\FirstLogin;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\Usuarios;
use yii\web\UploadedFile;
use yii\widgets\ActiveForm;
use app\traits\AjaxValidationTrait;
use app\models\RecuperarContra;
use yii\web\Session;
use app\models\RestablecerContra;
use yii\helpers\Url;
use app\components\RegistroMovimientos;
use app\models\Destinatarios;
use app\models\CartasDocumento;
use yii\helpers\FileHelper;
use app\models\Sectores;

class SiteController extends Controller
{
    use AjaxValidationTrait;

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'login', 'operador', 'firmante', 'admin', 'registro', 'firstlogin'],
                'rules' => [
                    [
                        'actions' => ['login', 'logout', 'registro','firstlogin'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout', 'admin','firstlogin'],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            return Usuarios::isUserAdmin(Yii::$app->user->identity->id);
                        },
                    ],
                    [
                        'actions' => ['logout', 'operador','firstlogin'],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            return Usuarios::isUserOperador(Yii::$app->user->identity->id);
                        },
                    ],
                    [
                        'actions' => ['logout', 'firmante','firstlogin'],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            return Usuarios::isUserFirmante(Yii::$app->user->identity->id);
                        },
                    ],
                ],
            ],
            //Controla el modo en que se accede a las acciones, en este ejemplo a la acción logout
            //sólo se puede acceder a través del método post
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {

        if (isset(Yii::$app->user->identity->id)) {
            if (Usuarios::isUserAdmin(Yii::$app->user->identity->id)) {
                $cantidadUsuarios = Usuarios::count();
                $cantidadDestinatarios = Destinatarios::count();
                $usuariosLegales = Usuarios::countLegales();
                $usuariosBP = Usuarios::countBP();
                $usuariosCC = Usuarios::countCC();
                $usuariosKYC = Usuarios::countKYC();
                $arrayBP = Usuarios::cantidadLogueosBP();
                $arrayLG = Usuarios::cantidadLogueosLG();
                $arrayCC = Usuarios::cantidadLogueosCC();
                $arrayKYC = Usuarios::cantidadLogueosKYC();
                $id_sector =Sectores::find()->where("ID_SECTORES=:id", [":id" => Yii::$app->user->identity->getIdSectores()])->one();
                $sectores = $id_sector->Sectores;

                return $this->render("admin", [
                    'cantidadUsuarios' => $cantidadUsuarios,
                    'cantidadDestinatarios' => $cantidadDestinatarios,
                    'usuariosLegales' => $usuariosLegales,
                    'usuariosBP' => $usuariosBP,
                    'usuariosCC' => $usuariosCC,
                    'usuariosKYC' => $usuariosKYC,
                    'cantidadBP' => $arrayBP,
                    'cantidadLG' => $arrayLG,
                    'cantidadCC' => $arrayCC,
                    'cantidadKYC' => $arrayKYC,
                    'sector' => $sectores,
                ]);


            } elseif (Usuarios::isUserOperador(Yii::$app->user->identity->id)) {
                $cantidadFirmantes = Usuarios::firmante();
                $cantidadOperadores = Usuarios::operadores();
                $cantidadCartas = CartasDocumento::cantidadCartas();
                $id_sector =Sectores::find()->where("ID_SECTORES=:id", [":id" => Yii::$app->user->identity->getIdSectores()])->one();
                $sectores = $id_sector->Sectores;

                return $this->render("operador", [
                    'cantidadFirmantes' => $cantidadFirmantes, 'cantidadOperadores' => $cantidadOperadores, 'cantidadCartas' => $cantidadCartas, 'sector' => $sectores]);

            } elseif (Usuarios::isUserFirmante(Yii::$app->user->identity->id)) {
                $cantidadFirmantes = Usuarios::firmante();
                $cantidadOperadores = Usuarios::operadores();
                $cantidadCartas = CartasDocumento::cantidadCartas();
                $id_sector =Sectores::find()->where("ID_SECTORES=:id", [":id" => Yii::$app->user->identity->getIdSectores()])->one();
                $sectores = $id_sector->Sectores;

                return $this->render("firmante", [
                    'cantidadFirmantes' => $cantidadFirmantes, 'cantidadOperadores' => $cantidadOperadores, 'cantidadCartas' => $cantidadCartas, 'sector' => $sectores]);
            }
        } elseif (!isset(Yii::$app->user->identity->id)) {

            return $this->redirect(['site/login']);
        }

        return $this->render("index");
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        $this->layout = 'main-login';
        $model = new LoginForm();
        $session = Yii::$app->session;
        $session->open();

        if ($model->load(Yii::$app->request->post())) {
            if ($model->login()) {
                $usuario = Usuarios::find()->where("IdUsuarios=:IdUsuarios", [":IdUsuarios" => Yii::$app->user->identity->id])->one();

                //Graba registro de auditoria----------------------------------------------------------
                $legajo = $usuario->LegajoUs;
                $usuario_insert = $legajo;

                RegistroMovimientos::history($usuario_insert);
                RegistroMovimientos::registrarMovimiento($legajo, 'LOGUEO', 'USUARIOS', 'Logueo del usuario');
                //--------------------------------------------------------------------------------------------------------

                if ($model->isFirstLogin()) {
                    $session["user_id"] = $model->user->id;

                    return $this->redirect('site/firstlogin');
                }
                return $this->goHome();

            } else {
                Yii::$app->cache->flush();
                $session->destroy();
                Yii::$app->session->setFlash('error', "Usuario o contraseña incorrecta, Vuelva a intentarlo.");

                $this->refresh();
            }


        } else {
            return $this->render('login', [
                'model' => $model]);
        }

        if (!\Yii::$app->user->isGuest) {
            if (Usuarios::isUserAdmin(Yii::$app->user->identity->id)) {
                return $this->redirect(['admin']);
            } elseif (Usuarios::isUserOperador(Yii::$app->user->identity->id)) {
                return $this->redirect(['operador']);
            } elseif (Usuarios::isUserFirmante(Yii::$app->user->identity->id)) {
                return $this->redirect(['firmante']);
            }
        }
    }

    public function actionLogout($id)
    {
        $model = new Usuarios();

        //Graba registro de auditoria-------------------------------------------------------------
        $usuario = Usuarios::find()->where("IdUsuarios=:IdUsuarios", [":IdUsuarios" => $id])->one();
        $legajo = $usuario->LegajoUs;
        $usuario_insert = $legajo;

        RegistroMovimientos::history($usuario_insert);
        RegistroMovimientos::registrarMovimiento($legajo, 'DESLOGEO', 'USUARIOS', 'Deslogueo del usuario');
        //----------------------------------------------------------------------------------------

        Yii::$app->session->destroy();
        Yii::$app->user->logout(false);

        return $this->redirect(Yii::$app->user->loginUrl);
    }

    public function actionFirstlogin()
    {
        $this->layout = 'main-login';
        $model = new FirstLogin();
        $session = new Session;
        $session->open();

        if (empty($session["user_id"])) {
            return $this->redirect(["site/login"]);
        }
        $user_id = $session["user_id"];

        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()) {
                $usuario = Usuarios::findOne(["IdUsuarios" => $user_id]);
                $hash = Yii::$app->getSecurity()->generatePasswordHash($model->new_password);

                Yii::$app->db->createCommand()->update(Usuarios::tableName(),
                    ['tiempo_ultima_modificacion' => date('d-M-Y H:i:s'), 'ip_registrada' => Yii::$app->request->userIP, 'password_hash' => $hash],
                    ['IdUsuarios' => $user_id])
                    ->execute();

                if (isset($usuario)) {
                    //Graba registro de auditoria-------------------------------------------------------------
                    $legajo = 0;
                    RegistroMovimientos::history(0);
                    RegistroMovimientos::registrarMovimiento($legajo, 'CONFIGURAR CONTRASEÑA', 'USUARIOS', 'Configurar contraseña. Usuario = ' . $user_id);
                    //----------------------------------------------------------------------------------------

                    $session->destroy();
                    Yii::$app->session->setFlash('success', "Contraseña cambiada correctamente. Redireccionando a la página de login.");
                    return $this->render('firstlogin', ['model' => $model]);
                }
            }
        }

        return $this->render('firstlogin', ['model' => $model]);

    }

    public function actionRegistro()
    {
        $msg = null;
        $model = new Usuarios();
        $model->scenario= Usuarios::SCENARIO_INSERT;
        $this->layout = 'main-login';

        if ($model->load(Yii::$app->request->post())) {
                $model->password_hash = Yii::$app->getSecurity()->generatePasswordHash(Usuarios::DEFAULT_PASS);
                $apellido = $model->ApellidoUs;
                $arr = explode(' ', trim($apellido));
                $model->usernameUs = $model->NombreUs[0] . $arr[0] . $model->LegajoUs;
                $model->tiempo_creacion = date('d-M-Y H:i:s');
                $model->ip_registrada = Yii::$app->request->userIP;

                $signature = UploadedFile::getInstance($model, 'signature');
                $model->signature = $signature;
                if ($model->save()) {
                    $model->updateAttributes(['signature' => $model->usernameUs . '.' . $signature->extension]);
                    if ($signature) {
                        $path = Yii::getAlias('@app') . '/firmas/';
                        if (FileHelper::createDirectory($path, $mode =0775, $recursive = \true)) {
                            $signature->saveAs(Yii::getAlias('@app') . '/firmas/' . $model->usernameUs . '.' . $signature->extension);
                        }
                    }
                    
                    $this->notificarUsuario($model);
                    Yii::$app->session->setFlash(
                        'success', "Registro exitoso. Redireccionando a la página de login.
              <meta http-equiv='refresh' content='3; " . Url::toRoute("site/login") . "'>");                } else {
                    Yii::$app->session->setFlash('saveFailure');
                }
        }

        return $this->render('registro', ['model' => $model, 'msg' => $msg]);
    }

    public function actionRecuperarcontra()
    {
        $msg = null;
        $modelRC = new RecuperarContra();
        $this->layout = 'main-login';

        $session = Yii::$app->session;
        $session->open();

        if (Yii::$app->request->isAjax && $modelRC->load(Yii::$app->request->post())) {

            Yii::$app->response->format = Response::FORMAT_JSON;
            ActiveForm::validate($modelRC);

            if ($modelRC->validate()) {

                $table = Usuarios::find()->where("emailUs=:emailUs", [":emailUs" => $modelRC->emailUs])->one();

                if (isset($table)) {

                    $session["recover"] = $modelRC->randKey("abcdef0123456789", 200);
                    $recover = $session["recover"];


                    $session["id_recover"] = $table->IdUsuarios;
                    $id = $session["id_recover"];


                    $codigo_verificacion = $modelRC->randKey("abcdef0123456789", 8);
                    $table->updateAttributes(['codigo_verificacion' => $codigo_verificacion]);

                    $subject = "Recuperar Contraseña";
                    $body = "<p>Use el siguiente c&oacute;digo para reestablecer su contrase&ntilde;a: ";
                    $body .= "<strong>" . $codigo_verificacion . "</strong></p>";

                    $content = "<p>" . $subject . "</p>";
                    $content .= "<p>" . $body . "</p>";

                    Yii::$app->mailer->compose("@app/mail/layouts/html", ["content" => $content])
                        ->setFrom([Yii::$app->params["adminEmail"] => Yii::$app->params["title"]])
                        ->setTo($modelRC->emailUs)
                        ->setSubject($subject)
                        ->setHtmlBody('<b>' . nl2br($body) . '</b>')
                        ->send();

                    Yii::$app->session->setFlash('warning');

                    //Graba registro de auditoria----------------------------------------------------------------------
                    $legajo = 0;
                    RegistroMovimientos::history(0);
                    RegistroMovimientos::registrarMovimiento($legajo, 'RECUPERAR CONTRASEÑA', 'USUARIOS', 'Recuperar contraseña de: ' . $modelRC->emailUs);
                    //--------------------------------------------------------------------------------------------------

                    return $this->redirect('restablecercontra');

                }
                if (!isset($table)) {
                    $session->destroy();
                    $modelRC->emailUs = null;
                    $this->refresh();
                    return Yii::$app->session->setFlash('error');
                }
            }
        }
        return $this->render('recuperarcontra', ['modelRC' => $modelRC, 'msg' => $msg,]);

    }

    public
    function actionRestablecercontra()
    {
        $model = new RestablecerContra();
        $msg = null;
        $this->layout = 'main-login';

        $session = new Session;
        $session->open();

        if (empty($session["recover"]) || empty($session["id_recover"])) {
            return $this->redirect(["site/index"]);
        } else {
            $recover = $session["recover"];

            $model->recover = $recover;
            $id_recover = $session["id_recover"];
        }

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()) {
                if ($recover == $model->recover) {

                    $usuario = Usuarios::findOne(["IdUsuarios" => $id_recover, "codigo_verificacion" => $model->codigo_verificacion]);
                    $hash = Yii::$app->getSecurity()->generatePasswordHash($model->passwordUs);

                    Yii::$app->db->createCommand()->update(Usuarios::tableName(),
                        ['tiempo_ultima_modificacion' => date('d-M-Y H:i:s'), 'ip_registrada' => Yii::$app->request->userIP, 'password_hash' => $hash],
                        ['IdUsuarios' => $id_recover])
                        ->execute();

                    if (isset($usuario)) {

                        //Graba registro de auditoria-------------------------------------------------------------
                        $legajo = 0;
                        RegistroMovimientos::history(0);
                        RegistroMovimientos::registrarMovimiento($legajo, 'RESTABLECER CONTRASEÑA', 'USUARIOS', 'Restablecer contraseña. Codigo Verificacion = ' . $model->codigo_verificacion);
                        //----------------------------------------------------------------------------------------

                        $session->destroy();
                        $model->passwordUs = null;
                        $model->password_repeat = null;
                        $model->recover = null;
                        $model->codigo_verificacion = null;

                        Yii::$app->session->setFlash(
                            'success', "Contraseña cambiada correctamente. Redireccionando a la página de login.
              <meta http-equiv='refresh' content='3; " . Url::toRoute("site/login") . "'>");
                    } else {
                        Yii::$app->session->setFlash('wrong_code');
                        $msg = "Ha ocurrido un error";
                    }
                }
            }
        }

        return $this->render('restablecercontra', ['model' => $model, 'msg' => $msg]);
    }

    public
    function notificarUsuario(Usuarios $usuario)
    {
        $default_pass = Usuarios::DEFAULT_PASS;
        $subject = "Bienvenido a BAN.CAR!";
        $body = '<div class="img-fluid"><img src=" " class="img-circle user-image img-rounded img-responsive" alt="Bancar Logo"/>
            </div>';
        $body .= "<br><br>Hola " . $usuario->NombreUs . "!<br><br>";
        $body .= "      Tu usuario se cre&oacute; exitosamente. Acced&eacute; al sistema con &eacute;stas credenciales para configurar tu nueva contrase&ntilde;a:<br>";
        $body .= "      <br>Usuario: $usuario->usernameUs<br>";
        $body .= "      Contrase&ntilde;a: $default_pass<br>";
        $body .= "<br>Banco Rioja S.A.U.";
        $content = "<p>" . $subject . "</p>";
        $content .= "<p>" . $body . "</p>";

        $this->sendEmail($body, $subject, $content, $usuario->emailUs);
    }

    public
    function sendEmail($body, $subject, $content, $email)
    {
        Yii::$app->mailer->compose("@app/mail/layouts/html", ["content" => $content])
            ->setFrom(["noreply.bancarsystem@bancorioja.com" => "Notificaciones BAN.CAR"])
            ->setTo($email)
            ->setSubject($subject)
            ->setHtmlBody(nl2br($body))
            ->send();
    }

}
