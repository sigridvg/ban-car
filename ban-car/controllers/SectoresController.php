<?php

namespace app\controllers;

use Yii;
use app\models\Sectores;
use app\models\sectoresSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use app\models\Usuarios;
use app\components\RegistroMovimientos;


/**
 * SectoresController implements the CRUD actions for sectores model.
 */
class SectoresController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return ['access' => [
            'class' => AccessControl::className(),
            'only' => ['index', 'create', 'delete', 'update'],
            'rules' => [
                [
                    'actions' => ['index', 'create', 'delete', 'update'],
                    'allow' => true,
                    'roles' => ['@'],
                    'matchCallback' => function ($rule, $action) {
                        return Usuarios::isUserAdmin(Yii::$app->user->identity->id);
                    },
                ],
            ],
        ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all sectores models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new sectoresSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single sectores model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new sectores model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Sectores();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            //GRABA REGISTRO EN AUDIT ---------------------------------------------------------------------------
            $sector = Sectores::find()->where("ID_SECTORES=:id", [":id" => $model->ID_SECTORES])->one();
             
            $usuario = Usuarios::find()->where("IdUsuarios=:IdUsuarios", [":IdUsuarios" => Yii::$app->user->identity->id])->one();
            $legajo = $usuario->LegajoUs;
            $usuario_insert = $legajo;

            RegistroMovimientos::history($usuario_insert);
            RegistroMovimientos::registrarMovimiento($legajo, 'CREAR', 'SECTORES', 'Carga de nuevo sector. Sector: ' . $sector->Sectores);
           //----------------------------------------------------------------------------------------------------
            
            
            return $this->redirect(['view', 'id' => $model->ID_SECTORES]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing sectores model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
           //GRABA REGISTRO EN AUDIT ---------------------------------------------------------------------------
            $sector = Sectores::find()->where("ID_SECTORES=:id", [":id" => $model->ID_SECTORES])->one();
             
            $usuario = Usuarios::find()->where("IdUsuarios=:IdUsuarios", [":IdUsuarios" => Yii::$app->user->identity->id])->one();
            $legajo = $usuario->LegajoUs;
            $usuario_insert = $legajo;

            RegistroMovimientos::history($usuario_insert);
            RegistroMovimientos::registrarMovimiento($legajo, 'ACTUALIZAR', 'SECTORES', 'Actualizar sector. Sector: ' . $sector->Sectores);
           //----------------------------------------------------------------------------------------------------
            
        return $this->redirect(['view', 'id' => $model->ID_SECTORES]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing sectores model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        
        
         //GRABA REGISTRO EN AUDIT ---------------------------------------------------------------------------
            $sector = Sectores::find()->where("ID_SECTORES=:id", [":id" => $id])->one();
             
            $usuario = Usuarios::find()->where("IdUsuarios=:IdUsuarios", [":IdUsuarios" => Yii::$app->user->identity->id])->one();
            $legajo = $usuario->LegajoUs;
            $usuario_insert = $legajo;

            RegistroMovimientos::history($usuario_insert);
            RegistroMovimientos::registrarMovimiento($legajo, 'BORRAR', 'SECTORES', 'Eliminar sector. Sector: ' . $sector->Sectores);
           //----------------------------------------------------------------------------------------------------
            
        $this->findModel($id)->delete();
        return $this->redirect(['index']);
    }

    /**
     * Finds the sectores model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return sectores the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = sectores::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
