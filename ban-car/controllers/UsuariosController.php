<?php

namespace app\controllers;

use Exception;
use Yii;
use app\models\Usuarios;
use app\models\UsuariosSearch;
use yii\log\Logger;
use yii\web\Controller;
use yii\web\ErrorHandler;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\data\SqlDataProvider;
use app\components\RegistroMovimientos;
use yii\web\UploadedFile;
use yii\helpers\FileHelper;
use app\models\UsuariosSearchAdmin;


/**
 * UsuariosController implements the CRUD actions for Usuarios model.
 */
class UsuariosController extends Controller
{


    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return ['access' => [
            'class' => AccessControl::className(),
            'only' => ['logout', 'login', 'index', 'create', 'delete', 'update', 'profile', 'view'],
            'rules' => [
                [
                    'actions' => ['logout', 'index', 'create', 'delete', 'update', 'view', 'profile'],
                    'allow' => true,
                    'roles' => ['@'],
                    'matchCallback' => function ($rule, $action) {
                        return Usuarios::isUserAdmin(Yii::$app->user->identity->id);
                    },
                ],
                [
                    'actions' => ['profile'],
                    'allow' => true,
                    'roles' => ['@'],
                    'matchCallback' => function ($rule, $action) {
                        return Usuarios::isUserOperador(Yii::$app->user->identity->id);
                    },

                ],
                [
                    'actions' => ['profile'],
                    'allow' => true,
                    'roles' => ['@'],
                    'matchCallback' => function ($rule, $action) {
                        return Usuarios::isUserFirmante(Yii::$app->user->identity->id);
                    },

                ],
                [
                    //El administrador tiene permisos sobre las siguientes acciones
                    'actions' => ['login'],
                    //Esta propiedad establece que tiene permisos
                    'allow' => true,
                    //Usuarios autenticados, el signo ? es para invitados
                    'roles' => ['?'],

                ],
            ],
        ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Usuarios models.
     * @return mixed
     */
    public function actionIndex()
    {
        
        $searchModel = new UsuariosSearch();
        $searadmin = new UsuariosSearchAdmin();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataproviderAdmin = $searadmin->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider, 'dataProviderAdmin' => $dataproviderAdmin
        ]);
    }

    /**
     * Displays a single Usuarios model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {

        $vista = Usuarios::find()->where("IdUsuarios=:IdUsuarios", [":IdUsuarios" => $id])->one();
        $username = $vista->usernameUs;
        $legajo_vista = $vista->LegajoUs;

        //GRABA REGISTRO EN AUDIT ---------------------------------------------------------------------------

        $usuario = Usuarios::find()->where("IdUsuarios=:IdUsuarios", [":IdUsuarios" => Yii::$app->user->identity->id])->one();
        $legajo_usuario = $usuario->LegajoUs;

        RegistroMovimientos::history($legajo_usuario);
        RegistroMovimientos::registrarMovimiento($legajo_usuario, 'VISTA', 'USUARIOS', 'Vista del usuario: ' . $username);

        //----------------------------------------------------------------------------------------------------

        $sql = 'SELECT * FROM audit WHERE usuario_legajo_AUD = :LegajoUs UNION SELECT * FROM audit_history WHERE usuario_legajo_AUD = :LegajoUs';

        $count = Yii::$app->db->createCommand('SELECT COUNT(*) FROM (SELECT * FROM audit WHERE usuario_legajo_AUD = :LegajoUs UNION SELECT * FROM audit_history WHERE usuario_legajo_AUD = :LegajoUs ORDER BY fecha_AUD DESC)x')->bindParam(":LegajoUs", $legajo_vista)->queryScalar();

        $dataProvider = new SqlDataProvider(['sql' => $sql,
            'params' => [':LegajoUs' => $legajo_vista],
            'totalcount' => $count,
            'sort' => [
                'attributes' => [
                    'fecha_AUD' => [
                        'default' => SORT_DESC,
                        'label' => 'Name',
                    ],
                ],
            ],
            'pagination' => [
                'pageSize' => 7
            ]]);


        $models = $dataProvider->getModels();
        $totalcount = $dataProvider->getTotalCount();
        $pages = $dataProvider->getPagination();

        if ($totalcount >= 7) {
            $limite = 7;
        } elseif ($totalcount < 7) {
            $limite = $totalcount;
        }


        return $this->render('view', [
            'model' => $this->findModel($id), 'dataProvider' => $models, 'totalcount' => $totalcount, 'pagination' => $pages, 'limite' => $limite,
        ]);
    }

    //PERFIL DEL USUARIO
    public function actionProfile($id)
    {
        $usuario = Usuarios::find()->where("IdUsuarios=:IdUsuarios", [":IdUsuarios" => Yii::$app->user->identity->id])->one();
        $legajo = $usuario->LegajoUs;

        $sql = 'SELECT * FROM audit WHERE usuario_legajo_AUD = :LegajoUs UNION SELECT * FROM audit_history WHERE usuario_legajo_AUD = :LegajoUs';

        $count = Yii::$app->db->createCommand('SELECT COUNT(*) FROM (SELECT * FROM audit WHERE usuario_legajo_AUD = :LegajoUs UNION SELECT * FROM audit_history WHERE usuario_legajo_AUD = :LegajoUs ORDER BY fecha_AUD DESC)x')->bindParam(":LegajoUs", $legajo)->queryScalar();

        $dataProvider = new SqlDataProvider(['sql' => $sql,
            'params' => [':LegajoUs' => $legajo],
            'totalcount' => $count,
            'sort' => [
                'attributes' => [
                    'fecha_AUD' => [
                        'default' => SORT_ASC,
                        'label' => 'Name',
                    ],
                ],
            ],
            'pagination' => [
                'pageSize' => 7
            ]]);


        $models = $dataProvider->getModels();
        $totalcount = $dataProvider->getTotalCount();
        $pages = $dataProvider->getPagination();

        if ($totalcount >= 7) {
            $limite = 7;
        } elseif ($totalcount < 7) {
            $limite = $totalcount;
        }

        //CONFIGURE--------------------------------------
        $model = $this->findModel($id);
        $prevFilename = $model->signature;
        $model->scenario = Usuarios::SCENARIO_UPDATE;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $signature = UploadedFile::getInstance($model, 'signature');
            if (!empty($signature->extension)) {
                $filename = $model->usernameUs . '.' . $signature->extension;
                $path = Yii::getAlias('@app') . '/firmas/' . $filename;
                if (file_exists($path)) {
                    chmod($path, 0777);
                    unlink($path);
                }
                if ($signature) {
                    $signature->saveAs($path);
                }
            } else {
                $filename = $prevFilename;
            }
            $model->updateAttributes(['signature' => $filename, 'tiempo_ultima_modificacion' => date('d-M-Y H:i:s')]);

            return $this->refresh();

        }
        //----------------------------------------------

        return $this->render('profile', [
            'model' => $this->findModel($id), 'dataProvider' => $models, 'totalcount' => $totalcount, 'pagination' => $pages, 'limite' => $limite,
        ]);
    }

    //-------------------

    public function actionShow($id)
    {
        $profile = $this->finder->finduserById($id);
        if ($profile === null) {
            throw new NotFoundHttpException();
        }

        return $this->render('view', [
            'usuarios' => $profile,
        ]);
    }

    /**
     * Creates a new Usuarios model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     * @throws \yii\base\Exception
     */
    public function actionCreate()
    {
        $model = new Usuarios();
        $model->scenario= Usuarios::SCENARIO_INSERT;
        
        if ($model->load(Yii::$app->request->post())) {
            $model->password_hash = Yii::$app->getSecurity()->generatePasswordHash(Usuarios::DEFAULT_PASS);
            $apellido = $model->ApellidoUs;
            $arr = explode(' ', trim($apellido));
            $model->usernameUs = $model->NombreUs[0] . $arr[0] . $model->LegajoUs;
            $model->tiempo_creacion = date('d-M-Y H:i:s');
            $model->ip_registrada = Yii::$app->request->userIP;

            $model->signature = UploadedFile::getInstance($model, 'signature');
            if ($model->save()) {
               
                //GRABA REGISTRO EN AUDIT ---------------------------------------------------------------------------

                $username = $model->usernameUs;

                $usuario = Usuarios::find()->where("IdUsuarios=:IdUsuarios", [":IdUsuarios" => Yii::$app->user->identity->id])->one();
                $legajo = $usuario->LegajoUs;
                $usuario_insert = $legajo;

                RegistroMovimientos::history($usuario_insert);
                RegistroMovimientos::registrarMovimiento($legajo, 'CREAR', 'USUARIOS', 'Creacion del usuario: ' . $username);

                //----------------------------------------------------------------------------------------------------
               
                if ($model->signature = UploadedFile::getInstance($model,'signature')) {
                    $path = Yii::getAlias('@webroot') . '/firmas/';
                    if (FileHelper::createDirectory($path, $mode =0775, $recursive = \true)) {
                        $model->signature->saveAs(Yii::getAlias('@webroot') . '/firmas/' . $model->usernameUs . '.' . $model->signature->extension);
                        $model->updateAttributes(['signature' => $model->usernameUs . '.' . $model->signature->extension]);
                    }
                }
    
                $this->notificarUsuario($model);
                return $this->redirect(['view', 'id' => $model->IdUsuarios]);
            } else {
                Yii::$app->session->setFlash('saveFailure');
            }

        } else {
            Yii::$app->session->setFlash('LoadFailure');
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Usuarios model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $prevFilename = $model->signature;

        $model->scenario = Usuarios::SCENARIO_UPDATE;
        
        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            //Actualiza archivo
            if($model->signature = UploadedFile::getInstance($model,'signature')){
                //Busca firma y la borra
                 $path = Yii::getAlias('@webroot') . '/firmas/' . $prevFilename;
                if ($prevFilename != null) {
                    chmod($path, 0775);
                    unlink($path);
                }else{
                    Yii::$app->session->setFlash('DeleteImageFailure');
                }
                
                $model->signature->saveAs(Yii::getAlias('@webroot') . '/firmas/' . $model->usernameUs . '.' . $model->signature->extension);
                $model->updateAttributes(['signature' => $model->usernameUs . '.' . $model->signature->extension]);
            }

            $model->updateAttributes(['tiempo_ultima_modificacion' => date('d-M-Y H:i:s')]);


            //GRABA REGISTRO EN AUDIT ---------------------------------------------------------------------------

            $username = $model->usernameUs;

            $usuario = Usuarios::find()->where("IdUsuarios=:IdUsuarios", [":IdUsuarios" => Yii::$app->user->identity->id])->one();
            $legajo = $usuario->LegajoUs;
            $usuario_insert = $legajo;

            RegistroMovimientos::history($usuario_insert);
            RegistroMovimientos::registrarMovimiento($legajo, 'ACTUALIZAR', 'USUARIOS', 'Actualizo el usuario: ' . $username);

            //----------------------------------------------------------------------------------------------------

            return $this->redirect(['view', 'id' => $model->IdUsuarios]);
        }
          
            return $this->render('update', [
                'model' => $model
            ]);
    }

    /**
     * Deletes an existing Usuarios model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        //GRABA REGISTRO EN AUDIT ---------------------------------------------------------------------------

        $vista = Usuarios::find()->where("IdUsuarios=:IdUsuarios", [":IdUsuarios" => $id])->one();
        $username = $vista->usernameUs;

        $usuario = Usuarios::find()->where("IdUsuarios=:IdUsuarios", [":IdUsuarios" => Yii::$app->user->identity->id])->one();
        $legajo = $usuario->LegajoUs;
        $usuario_insert = $legajo;

        RegistroMovimientos::history($usuario_insert);
        RegistroMovimientos::registrarMovimiento($legajo, 'BORRAR', 'USUARIOS', 'Borrar usuario: ' . $username);

        //----------------------------------------------------------------------------------------------------

        $model = $this->findModel($id);

        $path = Yii::getAlias('@webroot') . '/firmas/' . $model->signature;
        if ($model->signature != null) {
            chmod($path, 0777);
            unlink($path);
        }
        $model->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Usuarios model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Usuarios the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Usuarios::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function notificarUsuario(Usuarios $usuario)
    {
        $default_pass = Usuarios::DEFAULT_PASS;
        $subject = "Bienvenido a BAN.CAR!";
        $body = '<div class="img-fluid"><img src="https://svgshare.com/i/Md7.svg" class="img-circle user-image img-rounded img-responsive" alt="Bancar Logo"/>
            </div>';
        $body .= "<br><br>Hola " . $usuario->NombreUs . "!<br><br>";
        $body .= "      Tu usuario se cre&oacute; exitosamente. Acced&eacute; al sistema con &eacute;stas credenciales para configurar tu nueva contrase&ntilde;a:<br>";
        $body .= "      <br>Usuario: $usuario->usernameUs<br>";
        $body .= "      Contrase&ntilde;a: $default_pass<br>";
        $body .= "<br>Banco Rioja S.A.U.";
        $content = "<p>" . $subject . "</p>";
        $content .= "<p>" . $body . "</p>";

        $this->sendEmail($body, $subject, $content, $usuario->emailUs);
    }

    public function sendEmail($body, $subject, $content, $email)
    {
        Yii::$app->mailer->compose("@app/mail/layouts/html", ["content" => $content])
            ->setFrom(["noreply.bancarsystem@bancorioja.com" => "Notificaciones BAN.CAR"])
            ->setTo($email)
            ->setSubject($subject)
            ->setHtmlBody(nl2br($body))
            ->send();
    }
    
    
    public function notificarResetClave(Usuarios $usuario)
    {
        $default_pass = Usuarios::DEFAULT_PASS;
        $subject = "Reseteo de Clave - BANCAR";
 
        $body = "<br><br>Hola " . $usuario->NombreUs . "!<br><br>";
        $body .= "      Se reseteo tu clave exitosamente. Acced&eacute; al sistema con &eacute;stas credenciales para configurar tu nueva contrase&ntilde;a:<br>";
        $body .= "      <br>Usuario: $usuario->usernameUs<br>";
        $body .= "      Contrase&ntilde;a: $default_pass<br>";
        $body .= "<br>Banco Rioja S.A.U.";
        $content = "<p>" . $subject . "</p>";
        $content .= "<p>" . $body . "</p>";

        $this->sendEmail($body, $subject, $content, $usuario->emailUs);
    }

    
   public function actionReset($id){
        $model= $this->findModel($id);
        $hash = Yii::$app->getSecurity()->generatePasswordHash(Usuarios::DEFAULT_PASS);
        
        Yii::$app->db->createCommand()->update(Usuarios::tableName(),
            ['tiempo_ultimo_logueo' => 0, 'password_hash' => $hash],
            ['IdUsuarios' => $id])
         ->execute();
        
        $this->notificarResetClave($model);
        return $this->redirect(['index']);
    }
        
    public function actionViewAdmin($idrol)
    {

        //$vista = Usuarios::find()->where("IdRol=:IdRol", [":IdRol" => $idrol])->all();
        $searchModel = new UsuariosSearchAdmin();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);

    }
}
