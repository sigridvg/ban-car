<?php

namespace app\controllers;

use app\models\Localidades;
use Yii;
use app\models\Destinatarios;
use yii\filters\AccessControl;
use app\models\DestinatariosSearch;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use app\models\Usuarios;
use app\components\RegistroMovimientos;
use yii\web\Request;

/**
 * DestinatariosController implements the CRUD actions for Destinatarios model.
 */
class DestinatariosController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return ['access' =>
            [
                'class' => AccessControl::className(),
                'only' => ['index', 'create', 'delete', 'update', 'view'],
                'rules' => [
                    [
                        'actions' => ['index', 'update', 'view', 'delete', 'create'],

                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            return Usuarios::isUserAdmin(Yii::$app->user->identity->id);
                        },
                    ],
                    [
                        'actions' => ['index', 'update', 'view', 'create','delete'],

                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            return Usuarios::isUserFirmante(Yii::$app->user->identity->id);
                        },
                    ],
                    [
                        'actions' => [ 'index', 'update', 'view', 'create','delete'],

                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            return Usuarios::isUserOperador(Yii::$app->user->identity->id);
                        },
                    ],
                    [
                        'actions' => ['login'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Destinatarios models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new DestinatariosSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        if (!Yii::$app->user->isGuest) {
            return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        } else {
            return $this->redirect(['site/login']);
        }


    }

    /**
     * Displays a single Destinatarios model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        
        //GRABA REGISTRO EN AUDIT ---------------------------------------------------------------------------
        $vista = Destinatarios::find()->where("ID_DES=:ID_DES", [":ID_DES" => $id])->one();
        $nombre = $vista->Nombre_RazonSocial;

        $usuario = Usuarios::find()->where("IdUsuarios=:IdUsuarios", [":IdUsuarios" => Yii::$app->user->identity->id])->one();
        $legajo = $usuario->LegajoUs;
        $usuario_insert = $legajo;

        RegistroMovimientos::history($usuario_insert);
        RegistroMovimientos::registrarMovimiento($legajo, 'VISTA', 'DESTINATARIOS', 'Vista del destinatario: ' . $nombre);
        //----------------------------------------------------------------------------------------------------
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Destinatarios model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */

    public function actionCreate()
    {
        $model = new Destinatarios();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            //$model->updateAttributes(['tiempo_creacion' => date('d-M-Y H:i:s'),'ip_registrada'=>Yii::$app->request->userIP]);

            //GRABA REGISTRO EN AUDIT ---------------------------------------------------------------------------
            $nombre = Destinatarios::findOne(Yii::$app->request->get('id'))->Nombre_RazonSocial;

            $usuario = Usuarios::find()->where("IdUsuarios=:IdUsuarios", [":IdUsuarios" => Yii::$app->user->identity->id])->one();
            $legajo = $usuario->LegajoUs;
            $usuario_insert = $legajo;

            RegistroMovimientos::history($usuario_insert);
            RegistroMovimientos::registrarMovimiento($legajo, 'CREAR', 'DESTINATARIOS', 'Creacion del usuario: ' . $nombre);
            //----------------------------------------------------------------------------------------------------

            return $this->redirect(['view', 'id' => $model->ID_DES]);
        }

        if (isset($_POST['depdrop_parents'])) {
            $parents = $_POST['depdrop_parents'];
            if ($parents != null) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                $out = self::getLocalidades($parents[0]);

                return ['output' => $out, 'selected' => ' '];
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Destinatarios model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        //$localidad = Destinatarios::find()->where(['ID_DES'=>$id])->one();
        //$bandera = 0; 
        $model->scenario =  Destinatarios::SCENARIO_UPDATE;        
        
        if ($model->load(Yii::$app->request->post())) {
           
            $model->ID_Localidad = self::getParam() == 0 ? Destinatarios::findOne($id)->ID_Localidad : Self::getParam();
          
            if ($model->save()) {
                           
                //GRABA REGISTRO EN AUDIT ---------------------------------------------------------------------------
                $nombre = Destinatarios::findOne(Yii::$app->request->get('id'))->Nombre_RazonSocial;

                $usuario = Usuarios::find()->where("IdUsuarios=:IdUsuarios", [":IdUsuarios" => Yii::$app->user->identity->id])->one();
                $legajo = $usuario->LegajoUs;
                $usuario_insert = $legajo;

                RegistroMovimientos::history($usuario_insert);
                RegistroMovimientos::registrarMovimiento($legajo, 'ACTUALIZAR', 'DESTINATARIOS', 'Actualizo el usuario: ' . $nombre . ' Cambió: ');

                //----------------------------------------------------------------------------------------------------

                 return $this->redirect(['view', 'id' => $model->ID_DES]);
        }
       }
       
         if (isset($_POST['depdrop_parents'])) {
            $parents = $_POST['depdrop_parents'];
            if ($parents != null) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                $out = self::getLocalidades($parents[0]);

                return ['output' => $out, 'selected' => ' '];
            }
        }
        
        return $this->render('update', [
            'model' => $model,
        ]);
    }
    
    public function getParam() {
        $campos = json_encode(Yii::$app->request->getBodyParams());
        $c = explode(',', $campos);
        $campos2 = array_slice($c, 5); 
        $var =str_replace(array('}','"'), '', $campos2);
        $campos3 = implode(',', $var); 
        $var2 =str_replace(array('actualizar-button:', ',', ':', 'ID_Localidad'), '', $campos3);      
       
        return intval($var2);
    }
    /**
     * Deletes an existing Destinatarios model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        //GRABA REGISTRO EN AUDIT ---------------------------------------------------------------------------
        $borrar = Destinatarios::find()->where("ID_DES=:ID_DES", [":ID_DES" => $id])->one();
        $nombre = $borrar->Nombre_RazonSocial;

        $usuario = Usuarios::find()->where("IdUsuarios=:IdUsuarios", [":IdUsuarios" => Yii::$app->user->identity->id])->one();
        $legajo = $usuario->LegajoUs;
        $usuario_insert = $legajo;

        RegistroMovimientos::history($usuario_insert);
        RegistroMovimientos::registrarMovimiento($legajo, 'BORRAR', 'DESTINATARIOS', 'Borrar usuario: ' . $nombre);
        //----------------------------------------------------------------------------------------------------

        $this->findModel($id)->delete();


        return $this->redirect(['index']);
    }

    /**
     * Finds the Destinatarios model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Destinatarios the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Destinatarios::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }


    public function actionCarga()
    {
        $contador = 0;
        $contador2 = 0;

        //CAMBIAR NOMBRE DEL ARCHIVO Y LA RUTA
        $file = fopen("../web/sideba/sideba_dump.txt", "r", true) or die ("Error");

        while (!feof($file)) {

            $destinatarios_Tabla = Destinatarios::tableName();

            $archivo = fgets($file);
            $filas = explode("%", $archivo);

            $duplicado = Destinatarios::find()->where(['Cuit_Cuil' => $filas[0]])->one();

            if (isset($duplicado)) {
                $contador++;
                continue;

            } else {
                $contador2++;
                $InsertArray[] = [
                    'Cuit_Cuil' => $filas[0],
                    'Nombre_RazonSocial' => $filas[1],
                    'Direccion' => $filas[3],
                    'Email' => $filas[5],
                    'Codigo_pais' => $filas[6],
                    'ID_Provincia' => $filas[7],
                    'ID_Localidad' => $filas[8],
                ];

                $DestinatariosArray = ['Cuit_Cuil', 'Nombre_RazonSocial', 'Direccion', 'Email', 'Codigo_pais', 'ID_Provincia', 'ID_Localidad'];

                $InsertarCampos = Yii::$app->db->createCommand()
                    ->batchInsert($destinatarios_Tabla, $DestinatariosArray, $InsertArray)
                    ->execute();

                $InsertArray = [];

            }
        }
        fclose($file);
        Yii::$app->session->setFlash('carga', "Archivo Procesado. <br><br> " . $contador2 . " Cantidad de Destinatarios Cargados <br><br>" . $contador . " Filas duplicadas" ."<meta http-equiv='refresh' content='3; " . Url::toRoute("destinatarios/index") . "'>");

        //GRABA REGISTRO EN AUDIT ---------------------------------------------------------------------------
        $usuario = Usuarios::find()->where("IdUsuarios=:IdUsuarios", [":IdUsuarios" => Yii::$app->user->identity->id])->one();
        $legajo = $usuario->LegajoUs;
        $usuario_insert = $legajo;

        RegistroMovimientos::history($usuario_insert);
        RegistroMovimientos::registrarMovimiento($legajo, 'CARGA', 'DESTINATARIOS', 'Se cargaron ' . $contador2 . ' destinatario desde SIDEBA');
        //----------------------------------------------------------------------------------------------------

        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('carga');
        }
    }

    public function getLocalidades($id)
    {
        $data = Localidades::find()->where(['id_provincia' => $id])->select(['id', 'nombre'])->asArray()->orderBy('nombre')->all();
        $out = array();

        for ($i = 0; $i < count($data); $i++) {
            $out[$i]['id'] = $data[$i]['id'];
            $out[$i]['name'] = $data[$i]['nombre'];
        }
        return $out;
    }
}
