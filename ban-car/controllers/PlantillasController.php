<?php

namespace app\controllers;

use app\models\Plantillas;
use app\models\PlantillasSearch;
use Yii;
use yii\db\StaleObjectException;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;
use app\models\Usuarios;
use app\components\RegistroMovimientos;
use app\models\Sectores;
use app\models\Motivo;
use yii\helpers\FileHelper;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\base\DynamicModel;


/**
 * PlantillasController implements the CRUD actions for plantillas model.
 */
class PlantillasController extends Controller
{
    /**
     * Lists all plantillas models.
     * @return mixed
     */
    
    public function behaviors()
    {
        return ['access' => [
            'class' => AccessControl::className(),
            'only' => ['index', 'create', 'delete', 'update', 'view'],
            'rules' => [
                [
                    'actions' => ['index','view'],
                    'allow' => true,
                    'roles' => ['@'],
                    'matchCallback' => function ($rule, $action) {
                        return Usuarios::isUserOperador(Yii::$app->user->identity->id);
                    },
                ],
                [
                    'actions' => ['index,view'],
                    'allow' => true,
                    'roles' => ['@'],
                    'matchCallback' => function ($rule, $action) {
                        return Usuarios::isUserFirmante(Yii::$app->user->identity->id);
                    },
                ],
                [
                    'actions' => ['index', 'create', 'delete', 'update', 'view'],
                    'allow' => true,
                    'roles' => ['@'],
                    'matchCallback' => function ($rule, $action) {
                        return Usuarios::isUserAdmin(Yii::$app->user->identity->id);
                    },
                ],
                [
                    'actions' => ['login', 'logout'],
                    'allow' => true,
                    'roles' => ['?'],

                ],
            ],
        ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }
    
    public function actionIndex()
    {
        $searchModel = new PlantillasSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single plantillas model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new plantillas model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
     public function actionCreate()
    {
        $model = new Plantillas();

        if (Yii::$app->request->isPost) {
            if ($model->load(Yii::$app->request->post())) {
                $archivo = UploadedFile::getInstance($model, 'archivo');

            if ($archivo): 
                $path = Yii::getAlias('@app') . '/plantillas/';
                if (FileHelper::createDirectory($path, $mode =0775, $recursive = \true)) {
                    $archivo->saveAs(Yii::getAlias('@app') . '/plantillas/' . $archivo->baseName . '.' . $archivo->extension);
                }else{
                    $archivo->saveAs(Yii::getAlias('@app') . '/plantillas/' . $archivo->baseName . '.' . $archivo->extension);
                }
                
            endif;
            $model->archivo = $archivo->baseName . '.' . $archivo->extension;
            $model->nombre_archivo = $archivo->baseName;
            $model->updateAttributes(['sector' => $model->sector, 'motivo_plantilla'=>$model->motivo_plantilla]);
                      
            //GRABA REGISTRO EN AUDIT --------------------------------------------------------------------------
                    $usuario = Usuarios::find()->where("IdUsuarios=:IdUsuarios", [":IdUsuarios" => Yii::$app->user->identity->id])->one();
                    $legajo = $usuario->LegajoUs;
                    $usuario_insert = $legajo;

                    RegistroMovimientos::history($usuario_insert);
                    RegistroMovimientos::registrarMovimiento($legajo, 'CREAR', 'PLANTILLA', 'Creó plantilla N°: ' . $model->id);
            //---------------------------------------------------------------------------------------------------
            
                $plantilla  = Plantillas::find()->where("nombre_archivo=:NombreArchivo", [":NombreArchivo" => $archivo->baseName])->andWhere("sector=:SectorPlantilla",[':SectorPlantilla'=>$model->sector])->one();
                if ($plantilla):
                    Yii::$app->session->setFlash('yaExiste');
                    $this->refresh();
                else:
                    $model->save(false); 
                    Yii::$app->session->setFlash('plantillaCreada');
                    return $this->redirect(['update', 'id' => $model->id]);
                endif;      
            }

        } else {
            return $this->render('create', ['model'=>$model]);
        }
    }
    
    public function actionCreateColumn($campo) {
  
         $model = DynamicModel::validateData(compact('campo'), [
                [['name'], 'string', 'max' => 128],
            ]);
            if ($model->hasErrors()) {
                 Yii::$app->session->setFlash('campoError');
            } else {
                Yii::$app->session->setFlash('campoCreado');
            
                if ($model->load(Yii::$app->request->post())) {
            
                    var_dump(json_encode(Yii::$app->request->getBodyParams()));
                    exit();

                    Yii::$app->db->createCommand()->addColumn('plantillas',  Yii::$app->request->getBodyParams(), 'INT');

                    return $this->redirect(['index']);
                } 
            }
        return $this->render('create_field', [
           'model' => $model
        ]);
        
       
    }


    /**
     * Updates an existing plantillas model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) ) {

            $sector =  Sectores::find()->where("ID_SECTORES=:IdSector", [":IdSector" => $model->sector])->one();
            $sectorID = $sector->ID_SECTORES;

            $motivo = Motivo::find()->where("id=:IdMotivo", [":IdMotivo" => $model->motivo_plantilla])->one();

            if ($model->save()) {
                         
             //GRABA REGISTRO EN AUDIT --------------------------------------------------------------------------
                    $usuario = Usuarios::find()->where("IdUsuarios=:IdUsuarios", [":IdUsuarios" => Yii::$app->user->identity->id])->one();
                    $legajo = $usuario->LegajoUs;
                    $usuario_insert = $legajo;

                    RegistroMovimientos::history($usuario_insert);
                    RegistroMovimientos::registrarMovimiento($legajo, 'ACTUALIZAR', 'PLANTILLA', 'Actualizó plantilla N°: ' . $model->id);
             //---------------------------------------------------------------------------------------------------
            
            return $this->redirect(['view', 'id' => $model->id]);
            
            }                 
        }
        


        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing plantillas model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        if (unlink(Yii::getAlias('@app') . '/plantillas/' . $model->archivo) == true) {
            //GRABA REGISTRO EN AUDIT --------------------------------------------------------------------------
            $usuario = Usuarios::find()->where("IdUsuarios=:IdUsuarios", [":IdUsuarios" => Yii::$app->user->identity->id])->one();
            $legajo = $usuario->LegajoUs;
            $usuario_insert = $legajo;

            RegistroMovimientos::history($usuario_insert);
            RegistroMovimientos::registrarMovimiento($legajo, 'BORRAR', 'PLANTILLA', 'Borró plantilla N°: ' . $model->id);
            //---------------------------------------------------------------------------------------------------
            $model->delete();
        }
        return $this->redirect(['index']);
    }

    public function savePlantillaFile()
    {
        $model = new Plantillas();

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Finds the plantillas model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return plantillas the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = plantillas::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
