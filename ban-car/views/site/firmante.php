<?php

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use app\models\Sectores;

/* @var $this yii\web\View */
AppAsset::register($this);


$this->title = 'BAN.CAR | Firmante';
?>

<div class="contenter">
    <!-- Content Header (Page header) -->
    <section>

        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>


        <section>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.bundle.min.js"></script>

            <div class="box box-warning">

                <div class="box-header with-border">
                    <h3 class="box-title">Cartas creadas</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                    class="fa fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i>
                        </button>
                    </div>
                </div>
                <div class="box-body">
                    <div class="chart">
                        <canvas id="myChart1" style="height:230px"></canvas>
                    </div>
                </div>
                <!--/.box-body-->
            </div>


        </section>
        <section class="contenter-wrapper">
            <div class="row">
                <header style="text-align: center !important;font-size: 13pt !important; color:#6f6a6a;padding: 5px;">
                    ---------------------- Firmantes y Operadores x Sector ----------------------
                </header>
                <div class="col-lg-6 ROW">
                    <div class="info-box">
                        <span class="info-box-icon bg-yellow"><i class="fa fa-user"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-text">FIRMANTES</span>
                            <span class="info-box-number"><?= $cantidadFirmantes ?></span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6 ROW">
                        <div class="info-box">
                            <span class="info-box-icon bg-yellow"><i class="fa fa-user"></i></span>

                            <div class="info-box-content">
                                <span class="info-box-text">OPERADORES</span>
                                <span class="info-box-number"><?= $cantidadOperadores ?></span>
                            </div>
                        </div>
                    </div>
                    <!-- ---------------------------------------------- -->
        </section>
    </section>


    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<script type="text/javascript">
    var myChart1 = document.getElementById('myChart1').getContext('2d');

    var massPopChart = new Chart(myChart1, {
        type: 'bar', // bar, horizontalBar, pie, line, doughnut, radar, polarArea
        data: {
            labels: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
            datasets: [
                {
                    label: "<?= $sector ?>",

                    data: [<?= $cantidadCartas[0]?>,
                        <?= $cantidadCartas[1]?>,
                        <?= $cantidadCartas[2]?>,
                        <?= $cantidadCartas[3]?>,
                        <?= $cantidadCartas[4]?>,
                        <?= $cantidadCartas[5]?>,
                        <?= $cantidadCartas[6]?>,
                        <?= $cantidadCartas[7]?>,
                        <?= $cantidadCartas[8]?>,
                        <?= $cantidadCartas[9]?>,
                        <?= $cantidadCartas[10]?>,
                        <?= $cantidadCartas[11]?>]

                    ,
                    backgroundColor: [
                        "#84888a", "#84888a", "#84888a", "#84888a", "#84888a", "#84888a", "#84888a", "#84888a", "#84888a", "#84888a", "#84888a", "#84888a", "#84888a"],
                    //borderWidth:1,
                    //borderColor:'#777',
                    //hoverBorderWidth:3,
                    //hoverBorderColor:'#000'
                }]
        },
        options: {
            title: {
                display: false,
                text: 'Largest Cities In Massachusetts',
                fontSize: 25,
                responsive: true
            },
            legend: {
                display: true,
                position: 'bottom',
                labels: {
                    fontColor: '#000'
                }
            },
            layout: {
                padding: {
                    left: 50,
                    right: 0,
                    bottom: 0,
                    top: 0
                }
            },
            tooltips: {
                enabled: true
            }


        }
    });


</script>