<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'CONFIGURAR NUEVA CONTRASEÑA';
?>

<body class="login-page">
 <div class="login-logo">
        <?= Html::img('@web/img/BAN.CAR2.png', ['class' => 'rounded', 'style'=>'max-height:80px;']); ?>
        <br>         
        <a href="#"><b>BAN</b>CAR</a>
    </div>
<?php if (Yii::$app->session->hasFlash('success')) { ?>
    <div class="alert alert-success alert-dismissable">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
        <h5><i class="icon fa fa-check"></i>Correcto!</h5>
        <?= Yii::$app->session->getFlash('success') ?>
    </div>
<?php } ?>

<div class="row">
    <div class="col-md-8 col-md-offset-2 col-sm-6 col-sm-offset-9">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h1 class="panel-title" style="text-align:center" ><?= Html::encode($this->title) ?></h1>
            </div>
            <div class="panel-body">
                <?php $form = ActiveForm::begin([
                    'id' => 'first-login-form',
                    'method' => 'post',
                    'enableClientValidation' => true,
                    'enableAjaxValidation' => true,
                ]);
                ?>

                <div class="form-group col-md-12">
                    <?= $form->field($model, "new_password")->input("password") ?>
                </div>
                <div class="form-group col-md-12">
                    <?= $form->field($model, "new_password_repeat")->input("password") ?>
                </div>

                <div class="form-group">
                    <div class="col-lg-offset-3 col-lg-6">
                        <?= Html::submitButton('Confirmar', ['class' => 'btn bg-navy btn-block']) ?>
                    </div>
                </div>

            </div>
        </div>
        <?php ActiveForm::end(); ?>

    </div>

</div>
</body>
</html>
   
