<?php

use yii\helpers\Html;
use yii\helpers\ModalAjax;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Modal;
use app\view\usuarios\RecuperarContra;
use yii\helpers\Url;
use yii\widgets\Pjax;
use yii\db\Query;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */
/* @var $modelRC \common\models\RecuperarContra */

$this->title = 'INICIAR SESION';

$fieldOptions1 = [
    'options' => ['class' => 'form-group has-feedback'],
    'inputTemplate' => "{input}<span class='glyphicon glyphicon-user form-control-feedback'></span>"
];

$fieldOptions2 = [
    'options' => ['class' => 'form-group has-feedback'],
    'inputTemplate' => "{input}<span class='glyphicon glyphicon-lock form-control-feedback'></span>"
];
?>

<div class="login-box">
    <div class="login-logo">
        <?= Html::img('@web/img/BAN.CAR2.png', ['class' => 'rounded', 'style'=>'max-height:80px;']); ?>
        <br>         
        <a href="#"><b>BAN</b>CAR</a>
    </div>
    <!-- /.login-logo -->
    <div class="login-box-body">
        <p class="login-box-msg">Iniciar Sesion </p>

        <?php if ($this->beginCache(['duration' => 5])) {
            if (Yii::$app->session->hasFlash('error')) { ?>
                <div class="alert alert-danger">
                    <h6><?= Yii::$app->session->getFlash('error') ?></h6>
                </div>
            <?php }
            $this->endCache();
        } ?>

        <?php $form = ActiveForm::begin([
            'id' => 'login-form',
            'enableAjaxValidation' => true,
            'enableClientValidation' => false,
            'validateOnBlur' => false,
            'validateOnType' => false,
            'validateOnChange' => false,
        ]); ?>

        <?= $form
            ->field($model, 'usernameUs', $fieldOptions1)
            ->label(false)
            ->textInput(['placeholder' => $model->getAttributeLabel('usernameUs')]) ?>

        <?= $form
            ->field($model, 'passwordUs', $fieldOptions2)
            ->label(false)
            ->passwordInput(['placeholder' => $model->getAttributeLabel('passwordUs')]) ?>

        <div class="row">
            <div class="col-xs-8">
                <?php $form->field($model, 'rememberMe')->checkbox() ?>
            </div>
            <div class="col-xs-4">
                <?= Html::submitButton('Ingresar', ['class' => 'btn bg-purple ', 'name' => 'login-button']) ?>
            </div>
        </div>

        <?php ActiveForm::end(); ?>

        <?php html::a('Registro', ['site/registro'], ['class' => 'profile-link'])  ?>
     
        <?= html::a('Recuperar Contraseña', ['site/recuperarcontra'], ['class' => 'profile-link'])  ?>
    </div>
</div>
