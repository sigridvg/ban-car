<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;



$this->title = 'RESTABLECER CONTRASEÑA';
?>

<body class="login-page">
     <div class="login-logo">
        <?= Html::img('@web/img/BAN.CAR2.png', ['class' => 'rounded', 'style'=>'max-height:80px; margin:5px;']); ?>
        <br>
        <a href="#"><b>BAN</b>CAR</a>
    </div>
	<?php if (Yii::$app->session->hasFlash('success')){ ?>
		<div class="alert alert-success alert-dismissable">
         <h5><i class="icon fa fa-check"></i>Correcto!</h5>
         <?= Yii::$app->session->getFlash('success') ?>
    </div>
	<?php }elseif (Yii::$app->session->hasFlash('warning')) {?>
		<div class="alert alert-warning">
         	<h5><i class="icon fa fa-check"></i>REVISE SU CORREO</h5>
		    Hemos enviado un correo con el c&oacute;digo de verificaci&oacute;n.
	    </div>
     <?php }elseif (Yii::$app->session->hasFlash('wrong_code')) {?>
		<div class="alert alert-danger">
         	<h5><i class="icon fa fa-close"></i>C&oacute;digo inv&aacute;lido</h5>
		    Revisa tu casilla de correo.
	    </div>
	<?php }?>

<div class="row">
    <div class="col-md-8 col-md-offset-2 col-sm-6 col-sm-offset-9">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h1 class="panel-title" style="text-align:center"><?= Html::encode($this->title) ?></h1>
            </div>
            <div class="panel-body">
			<?php $form = ActiveForm::begin([
				'id' => 'restablecer-contra-form',
			    'method' => 'post',
			    'enableClientValidation' => true,
	            'enableAjaxValidation' => true,
			]);
			?>

			<div class="form-group col-md-12">
			 <?= $form->field($model, "passwordUs")->input("password") ?>
			</div>
			 
			<div class="form-group col-md-12">
			 <?= $form->field($model, "password_repeat")->input("password")->label('Repetir Contraseña') ?>  
			</div>

			<div class="form-group col-md-12">
			 <?= $form->field($model, "codigo_verificacion")->input("text") ?>  
			</div>
			<div class="form-group">
			 	<?= $form->field($model, "recover")->input("hidden")->label(false) ?>  
			</div>


			<div class="form-group">
                    <div class="col-lg-offset-3 col-lg-6">
                        <?= Html::submitButton('Confirmar', ['class' => 'btn bg-navy btn-block']) ?>
                    </div>
            </div>
            
        </div>
        <p class="text-center">
            		<?= Html::a('Iniciar Sesion', ['/site/login']) ?>
        </p>
    </div>
        <?php ActiveForm::end(); ?>
        
</div>
  
</div>
</body>
</html>
   
