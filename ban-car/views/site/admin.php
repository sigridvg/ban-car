<?php

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */

$this->title = 'BAN.CAR | ADMINISTRADOR';

?>
<script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
<div >
  <!-- Content Header (Page header) -->
  <section class="content-header">

    <?= Breadcrumbs::widget([
      'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
    ]) ?>

  </section>

  <!-- Main content -->

  <section>

    <div class="row">
      <div class="col-lg-6 col-xs-24">
        <div class="info-box">
          <span class="info-box-icon bg-yellow"><i class="fa fa-user"></i></span>

          <div class="info-box-content">
            <span class="info-box-text">USUARIOS</span>
            <span class="info-box-number"><?= $cantidadUsuarios ?></span>
          </div>
        </div>
      </div>
      <!-- ---------------------------------------------- -->
      <div class="col-lg-6 col-xs-24">
        <div class="info-box">
          <span class="info-box-icon bg-yellow"><i class="fa fa-user"></i></span>

          <div class="info-box-content">
            <span class="info-box-text">DESTINATARIOS</span>
            <span class="info-box-number"><?= $cantidadDestinatarios ?></span>
          </div>
        </div>
      </div>


    </div>

    <div class="row">
      <section style="text-align: center !important;font-size: 13pt !important; color:#6f6a6a;padding: 5px 15px 15px 15px;">
        Usuarios por Sector
      </section>
      <div class="col-lg-3 col-xs-24">
        <div class="info-box">
          <span class="info-box-icon bg-purple"><i class="fa fa-bank"></i></span>

          <div class="info-box-content">
            <span class="info-box-text">BANCA PRIVADA</span>
            <span class="info-box-number"><?= $usuariosBP ?></span>
          </div>
        </div>
      </div>
        <!-- ---------------------------------------------- -->
        <div class="col-lg-3 col-xs-24">
            <div class="info-box">
                <span class="info-box-icon bg-purple"><i class="fa fa-bank"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">CONOZCA A SU CLIENTE</span>
                    <span class="info-box-number"><?= $usuariosKYC ?></span>
                </div>
            </div>
        </div>
      <!-- ---------------------------------------------- -->
      <div class="col-lg-3 col-xs-24">
        <div class="info-box">
          <span class="info-box-icon bg-purple"><i class="fa fa-bank"></i></span>

          <div class="info-box-content">
            <span class="info-box-text">LEGALES</span>
            <span class="info-box-number"><?= $usuariosLegales ?></span>
          </div>
        </div>
      </div>
      <!-- ---------------------------------------------- -->
      <div class="col-lg-3 col-xs-24">
        <div class="info-box">
          <span class="info-box-icon bg-purple"><i class="fa fa-bank"></i></span>

          <div class="info-box-content">
            <span class="info-box-text">CUENTA CORRIENTE</span>
            <span class="info-box-number"><?= $usuariosCC ?></span>
          </div>
        </div>
      </div>

    </div>
  </section>
  <!-- /.content -->
  <section>
   <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.bundle.min.js"></script>
   <section style="text-align: center !important;font-size: 13pt !important; color:#6f6a6a;padding: 5px 15px 15px 15px;">
     Usuarios logueados por mes
   </section>
   <div class="box box-warning">

    <div class="box-header with-border">
      <h3 class="box-title">Usuarios por Sector </h3>

      <div class="box-tools pull-right">
        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
        </button>
        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
      </div>
    </div>
    <div class="box-body">
      <div class="chart">
        <canvas id="myChart1" style="height:230px"></canvas>
      </div>
    </div>
    <!--/.box-body-->
  </div>



</section> 
</div>
<script type="text/javascript">
  var myChart1 = document.getElementById('myChart1').getContext('2d');

  var massPopChart = new Chart(myChart1, {
  type:'bar', // bar, horizontalBar, pie, line, doughnut, radar, polarArea
  data:{
    labels:['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
    datasets:[
    {
      label:"BANCA PRIVADA",
      
      data:[<?= $cantidadBP[0]?>,
      <?= $cantidadBP[1]?>,
      <?= $cantidadBP[2]?>,
      <?= $cantidadBP[3]?>,
      <?= $cantidadBP[4]?>,
      <?= $cantidadBP[5]?>,
      <?= $cantidadBP[6]?>,
      <?= $cantidadBP[7]?>,
      <?= $cantidadBP[8]?>,
      <?= $cantidadBP[9]?>,
      <?= $cantidadBP[10]?>,
      <?= $cantidadBP[11]?>]

      ,
      backgroundColor:[
      "#84888a", "#84888a","#84888a","#84888a","#84888a","#84888a","#84888a","#84888a","#84888a","#84888a","#84888a","#84888a","#84888a"],
      //borderWidth:1,
      //borderColor:'#777',
      //hoverBorderWidth:3,
      //hoverBorderColor:'#000'
    },
    {
            label: "Legales",
            backgroundColor: ["#EBA10E","#EBA10E","#EBA10E","#EBA10E","#EBA10E","#EBA10E","#EBA10E","#EBA10E","#EBA10E","#EBA10E","#EBA10E","#EBA10E","#EBA10E"],
            data:[<?= $cantidadLG[0]?>,
                <?= $cantidadLG[1]?>,
                <?= $cantidadLG[2]?>,
                <?= $cantidadLG[3]?>,
                <?= $cantidadLG[4]?>,
                <?= $cantidadLG[5]?>,
                <?= $cantidadLG[6]?>,
                <?= $cantidadLG[7]?>,
                <?= $cantidadLG[8]?>,
                <?= $cantidadLG[9]?>,
                <?= $cantidadLG[10]?>,
                <?= $cantidadLG[11]?>]
        },
        {
            label: "Conozca a su Cliente",
            backgroundColor: ["#3A3433","#3A3433","#3A3433","#3A3433","#3A3433","#3A3433","#3A3433","#3A3433","#3A3433","#3A3433","#3A3433","#3A3433","#3A3433"],
            data:[<?= $cantidadKYC[0]?>,
                <?= $cantidadKYC[1]?>,
                <?= $cantidadKYC[2]?>,
                <?= $cantidadKYC[3]?>,
                <?= $cantidadKYC[4]?>,
                <?= $cantidadKYC[5]?>,
                <?= $cantidadKYC[6]?>,
                <?= $cantidadKYC[7]?>,
                <?= $cantidadKYC[8]?>,
                <?= $cantidadKYC[9]?>,
                <?= $cantidadKYC[10]?>,
                <?= $cantidadKYC[11]?>]
        },
    {
      label: "Cuenta Corriente",
      backgroundColor: ["#625BA9", "#625BA9","#625BA9","#625BA9","#625BA9","#625BA9","#625BA9","#625BA9","#625BA9","#625BA9","#625BA9","#625BA9","#625BA9"],
      data:[<?= $cantidadCC[0]?>,
      <?= $cantidadCC[1]?>,
      <?= $cantidadCC[2]?>,
      <?= $cantidadCC[3]?>,
      <?= $cantidadCC[4]?>,
      <?= $cantidadCC[5]?>,
      <?= $cantidadCC[6]?>,
      <?= $cantidadCC[7]?>,
      <?= $cantidadCC[8]?>,
      <?= $cantidadCC[9]?>,
      <?= $cantidadCC[10]?>,
      <?= $cantidadCC[11]?>]
    }]
  },
  options:{
    title:{
      display:false,
      text:'Largest Cities In Massachusetts',
      fontSize:25,
      responsive: true
    },
    legend:{
      display:true,
      position:'bottom',
      labels:{
        fontColor:'#000'
      }
    },
    layout:{
      padding:{
        left:50,
        right:0,
        bottom:0,
        top:0
      }
    },
    tooltips:{
      enabled:true
    }



  }
});


</script>
