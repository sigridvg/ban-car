<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\RegistroForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;


$this->title = 'RECUPERAR CONTRASEÑA';
?>

<body class="login-page">
     <div class="login-logo">
        <?= Html::img('@web/img/BAN.CAR2.png', ['class' => 'rounded', 'style'=>'max-height:80px; margin:5px;']); ?>
        <br>
        <a href="#"><b>BAN</b>CAR</a>
    </div>
	<?php if (Yii::$app->session->hasFlash('error')){ ?>
		<div class="alert alert-danger ">
         	<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
         	<h5><i class="icon fa fa-times"></i>Error!</h5>
         	<?= Yii::$app->session->getFlash('error') ?>
         	Usuario no existente
    	</div>
	<?php }?>

<div class="row">
    <div class="col-md-8 col-md-offset-2 col-sm-6 col-sm-offset-9">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h1 class="panel-title" style="text-align:center"><?= Html::encode($this->title) ?></h1>
            </div>
            <div class="panel-body">
			<?php $form = ActiveForm::begin([
				'id' => 'recuperar-contra-form',
			    'method' => 'post',
			    'enableClientValidation' => true,
	            'enableAjaxValidation' => true,
			]);
			?>
			<div class="form-group col-md-12" >   
                <?=$form->field($modelRC, 'emailUs')->textInput()?>
             </div>
                
            <div class="form-group">
                <div class="col-lg-offset-2 col-lg-7">
                    <?= Html::submitButton('Enviar', ['class' => 'btn bg-purple btn-block','style'=>'margin-left:50px;margin-bottom:5px;', 'name' => 'recuperar-contra <div class="form-group"></div>-button']) ?>
                </div>
            </div>
        <?php ActiveForm::end(); ?>   
	</div>
  </div>
</body>
</html>
   
