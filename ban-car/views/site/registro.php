<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

/* @var $model app\models\RegistroForm */

use app\models\Sectores;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use app\models\Roles;
use yii\helpers\ArrayHelper;


$this->title = 'REGISTRO';
?>

<h4><?= $msg ?></h4>
<body class="login-page">
<div class="login-logo">
        <?= Html::img('@web/img/BAN.CAR2.png', ['class' => 'rounded', 'style'=>'max-height:80px;']); ?>
        <br>
    <a href="#"><b>BAN</b>CAR</a>
</div>
<?php if (Yii::$app->session->hasFlash('success')){ ?>
<div class="alert alert-success alert-dismissable">
    <h5><i class="icon fa fa-check"></i>Correcto!</h5>
    <?= Yii::$app->session->getFlash('success') ?>
    <?php } ?>
</div>
<div class="row">
    <div class="col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h1 class="panel-title col-lg-offset-5"><?= Html::encode($this->title) ?></h1>
            </div>
            <div class="panel-body">
                <?php $form = ActiveForm::begin([
                    'id' => 'registro-form',
                    'method' => 'post',
                    'options' => ['class' => 'form-horizontal'],
                ]); ?>

                <div class="form-group">
                    <div class="col-lg-4 col-md-2">
                        <?= $form->field($model, 'NombreUs', ['options' => ['class' => 'no-margin']])->textInput() ?>
                    </div>
                    <div class="col-lg-4 col-md-2">
                        <?= $form->field($model, 'ApellidoUs', ['options' => ['class' => 'no-margin']])->textInput() ?>
                    </div>
                    <div class="col-lg-4 col-md-2">
                        <?= $form->field($model, 'LegajoUs', ['options' => ['class' => 'no-margin']])->textInput() ?>
                    </div>

                </div>

                <div class="form-group">

                    <div class="col-lg-6 col-md-2">
                        <?= $form->field($model, 'IdRoles', ['options' => ['class' => 'no-margin']])
                            ->dropDownList(ArrayHelper::map(Roles::find()->where(['in', 'ID_ROLES', [2, 3]])
                                ->all(), 'ID_ROLES', 'Roles'))
                            ->label('Rol') ?>
                    </div>
                    <div class="col-lg-6 col-md-2">
                        <?= $form->field($model, 'IdSectores', ['options' => ['class' => 'no-margin']])
                            ->dropDownList(ArrayHelper::map(Sectores::find()
                                ->all(), 'ID_SECTORES', 'Sectores'))
                            ->label('Sector') ?>
                    </div>
                </div>
                <div class="form-group">
            <div class="col-md-6 col-md-6" >   
                <?=$form->field($model, 'emailUs', ['options' => ['class' => 'no-margin']])->textInput()?>
                </div>
               
                <div class="col-md-6 col-md-6" >   
                <?=$form->field($model, 'documento', ['options' => ['class' => 'no-margin']])?>
                </div>
           </div>

                <div class="form-group col-md-12">
                    <?= $form->field($model, 'signature', ['options' => ['class' => 'no-margin']])->fileInput() ?>
                </div>


                <div class="form-group">
                    <div class="col-lg-offset-2 col-lg-7">
                        <?= Html::submitButton('Registrarse', ['class' => 'btn btn-warning btn-block', 'name' => 'registro <div class="form-group"></div>-button']) ?>
                    </div>
                </div>
            </div>
            <p class="text-center">
                <?= Html::a('Ya te registraste? Inicia Sesion!', ['/site/login']) ?>
            </p>

        </div>
        <?php ActiveForm::end(); ?>
    </div>

</div>
</body>