<?php

use yii\helpers\Html;
use app\models\Pais;
use app\models\Provincias;
use app\models\Sucursales;
use app\models\Localidades;

/* @var $this yii\web\View */
/* @var $model app\models\Destinatarios */


$this->title = ' ';
$this->params['breadcrumbs'][] = ['label' => 'Destinatarios', 'url' => ['index']];
$this->params['breadcrumbs'][] = $model->Nombre_RazonSocial;
\yii\web\YiiAsset::register($this);
?>
<link rel="stylesheet" type="text/css" href="../web/css/titulos.css">


<br>
 
<section class="content">

      <div class="row">
        <div class="col-md-12">
          <div class="box box-primary">
            <div class="box-body box-profile">
             <!-- <img class="profile-user-img img-responsive img-circle" src="../../dist/img/user4-128x128.jpg" alt="User profile picture">-->

              <h3 class="profile-username text-center"><?= $model->Nombre_RazonSocial ?></h3>

              
              <ul class="list-group list-group-unbordered">
                 <?php if (!empty($model->Direccion)): ?>
                       <li class="list-group-item">
                        <b>DIRECCION</b><a class="pull-right"><?= Html::encode($model->Direccion). ' - '. Html::encode(Pais::findOne($model->Codigo_pais)->Nombre_Pais) ?></a>
                             
                        </li>
                <?php endif; ?>
                <li class="list-group-item">
                     <b>CUIT/CUIL</b> <a class="pull-right"><?= Html::encode(substr($model->Cuit_Cuil, 0,2).'-'.substr($model->Cuit_Cuil, 2,8) .'-'.substr($model->Cuit_Cuil, -1,1)) ?></a>
                </li> 
                <li class="list-group-item">
                      <b>FECHA NACIMIENTO</b> <a class="pull-right"><?=Html::encode($model->Fecha_Nacimiento) ?></a>
                </li>
                <li class="list-group-item">
                      <b>SEXO</b> <a class="pull-right"><?=Html::encode($model->Sexo) ?></a>
                </li>
                <li class="list-group-item">
                      <b>TELEFONO</b> <a class="pull-right"><?=Html::encode($model->Celular) ?></a>
                </li>
                <li class="list-group-item">
                      <b>CUIDADANIA</b> <a class="pull-right"><?=Html::encode($model->Ciudadania) ?></a>
                </li>
                 <li class="list-group-item">
                      <b>COD. ACTIVIDAD</b> <a class="pull-right"><?=Html::encode($model->Cod_actividad) ?></a>
                </li>
                <li class="list-group-item">
                      <b>EMAIL</b> <a class="pull-right"><?=Html::encode($model->Email) ?></a>
                </li>
                
                <li class="list-group-item">
                    <b>SUCURSAL</b> <a class="pull-right"><?=  'Sucursal: '. Html::encode(Sucursales::findOne($model->Sucursal)->nombre) ?></a>
                </li>
                <li class="list-group-item">
                    <b>CUENTA</b> <a class="pull-right"><?=   'Nro Cuenta: '. Html::encode($model->Cuenta). ' Tipo de cuenta: '. Html::encode($model->Tipo_cuenta)  ?></a>
                </li>
                <li class="list-group-item">
                      <b>GRUPO - SUBGRUPO </b> <a class="pull-right"><?= 'Grupo: '. Html::encode($model->Grupo). ' - Subgrupo: '. Html::encode($model->Subgrupo)  ?></a>
                </li>
                <li class="list-group-item">
                      <b>CUIDAD</b> <a class="pull-right"><?=Html::encode(Provincias::findOne($model->ID_Provincia)->nombre) ?></a>
                </li>
                <li class="list-group-item">
                  <?php  if($model->ID_Localidad >0){ ?>
                      <b>LOCALIDAD</b> <a class="pull-right"><?=Html::encode(Localidades::findOne($model->ID_Localidad)->nombre) ?></a>
                  <?php } else { ?>
                        <b>LOCALIDAD</b> <a class="pull-right">Sin definir</a>
                  <?php } ?>
                </li>
                <li class="list-group-item">
                      <b>CODIGO POSTAL</b> <a class="pull-right"><?=Html::encode($model->Cod_postal) ?></a>
                </li>
                <li class="list-group-item">
                      <b>TIPO DOMICILIO</b> <a class="pull-right"><?=Html::encode($model->Tipo_domicilio) ?></a>
                </li>
                
              </ul>

              <?= Html::a('Actualizar', ['update', 'id' => $model->ID_DES], ['class' => '.btn-sm btn bg-purple color-palette col-lg-6']) ?>  
                <?= Html::a('Borrar', ['delete', 'id' => $model->ID_DES], [
                'class' => '.btn-sm btn bg-navy color-palette col-lg-6','data' => [ 
                        'confirm' => 'Esta seguro que desea eliminar este Destinatario?',
                        'method' => 'post',
                      ],
                ]) ?>
            </div>
          </div>
          <!-- /.box -->

       
      </div>
      <!-- /.row -->

    </section>
    <!-- /.content -->
 
