<?php

use app\models\Provincias;
use kartik\depdrop\DepDrop;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use app\models\Pais;
use yii\helpers\ArrayHelper;
use app\models\Localidades;
use app\models\Sucursales;

/* @var $this yii\web\View */
/* @var $model app\models\Destinatarios */
/* @var $form yii\widgets\ActiveForm */
?>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
<div class="destinatarios-form">
    <div class="col-md-8 col-lg-12">
        <?php $form = ActiveForm::begin([
            'enableAjaxValidation' => false, 
            'id' => 'destinatarios-form',
            'method' => 'post',
        ]);
        $dataProvincias = ArrayHelper::map(Provincias::find()->asArray()->orderBy('nombre')->all(), 'id', 'nombre');
        ?>

        <div class="align-items-center">
            <div class="form-group">
                 <div class="col-auto col-md-6">
                    <?= $form->field($model, 'DNI', ['options' => ['class' => 'no-margin']])->textInput(['disabled' => true]) ?>
                </div>
                <div class="col-auto col-md-6">
                    <?= $form->field($model, 'Tipo_documento', ['options' => ['class' => 'no-margin']])->textInput(['disabled' => true]) ?>
                </div>
            </div>
             <div class="form-group">
                <div class="col-auto col-md-6">
                    <?= $form->field($model, 'Nombre_RazonSocial', ['options' => ['class' => 'no-margin']])->textInput(['maxlength' => true, 'disabled'=>true]) ?>
                </div>
                <div class="col-auto col-md-6">
                    <?= $form->field($model, 'Cuit_Cuil', ['options' => ['class' => 'no-margin']])->textInput(['disabled' => true]) ?>
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="col-lg-6 col-md-4">
                <?= $form->field($model, 'Email', ['options' => ['class' => 'no-margin']])->textInput() ?>
            </div>
            <div class="col-lg-6 col-md-4">
                <?= $form->field($model, 'Direccion', ['options' => ['class' => 'no-margin']])->textInput() ?>
            </div>
        </div>
        <div class="form-group">
            <div class="col-lg-3">
                <?= $form->field($model, 'Sucursal', ['options' => ['class' => 'no-margin']])->textInput(['value'=> Sucursales::findOne($model->Sucursal)->nombre, 'disabled'=>true])?>
            </div>
            <div class="col-lg-3">
                <?= $form->field($model, 'Cuenta', ['options' => ['class' => 'no-margin']])->textInput(['disabled' => true]) ?>
            </div>
             <div class="col-lg-2">
                <?= $form->field($model, 'Grupo', ['options' => ['class' => 'no-margin']])->textInput(['disabled' => true]) ?>
            </div>
             <div class="col-lg-2">
                <?= $form->field($model, 'Subgrupo', ['options' => ['class' => 'no-margin']])->textInput(['disabled' => true]) ?>
            </div>
             <div class="col-lg-2">
                <?= $form->field($model, 'Tipo_cuenta', ['options' => ['class' => 'no-margin']])->textInput(['disabled' => true]) ?>
            </div>
        </div>
        <div class="form-group">
            <div class="col-lg-4 col-md-4">
                <?= $form->field($model, 'Codigo_pais', ['options' => ['class' => 'no-margin']])->dropDownList(ArrayHelper::map(Pais::find()
                    ->all(), 'Cod_Pais', 'Nombre_Pais'))
                    ->label('Pais') ?>
            </div>
            <div class="col-lg-4 col-md-4">
                <?= $form->field($model, 'ID_Provincia', ['options' => ['class' => 'no-margin']])
                    ->dropDownList($dataProvincias, ['prompt' => 'Elija una provincia', 'id' => 'id_prov'])
                    ->label('Provincia'); ?>
            </div>

            <div class="col-lg-4 col-md-4">
                <?php if ($model->ID_Localidad>0){
                    $localidad = Localidades::find()->where("id=:id", [":id" => $model->ID_Localidad])->one();
                    echo $form->field($model, 'ID_Localidad')->widget(DepDrop::className(),
                        [
                            'data' => [$localidad->nombre],
                            'pluginOptions' => [
                                'placeholder' => 'Elija una localidad',
                                'depends' => ['id_prov'],
                                'url' => Url::to(['/destinatarios/create'])
                            ]
                        ]
                    )->label('Localidad'); 
                }else{
                 echo $form->field($model, 'ID_Localidad')->widget(DepDrop::className(),
                    [
                        'pluginOptions' => [
                            'placeholder' => 'Elija una localidad',
                            'depends' => ['id_prov'],
                            'url' => Url::to(['/destinatarios/create'])
                        ]
                    ]
                    )->label('Localidad');  
                } ?>
            </div>
        </div>
        <div class="col-lg-offset-2 col-lg-7">
           <?= Html::submitButton('Guardar', ['class' => 'btn bg-navy color-palette btn-block', 'name' => 'actualizar-button']) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>
