<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\destinatariosSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = '';
$this->params['breadcrumbs'][] = ['label' => 'Destinatarios', 'url' => ['index']];;

?>
<link rel="stylesheet" type="text/css" href="../web/css/titulos.css">
<div class="destinatarios-index">

    <h1 id="titulo"><?php echo '<b>' . 'DESTINA' . '</b>' . 'TARIOS' ?></h1>

 
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            
            'Nombre_RazonSocial',
            'DNI',
            'Tipo_documento',
            'Cuit_Cuil',
 
            [
                'attribute' => 'Sucursal',
                'headerOptions' => ['style' => 'text-align: center;justify-content:center;'],
                'label' => 'Sucursal',
                'value' => 'sucursales.nombre'
            ],
            'Cuenta',
            'Grupo',
            'Subgrupo',
            'Tipo_cuenta',


            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
