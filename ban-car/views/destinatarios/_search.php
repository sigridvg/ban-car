<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\destinatariosSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="destinatarios-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'ID_DES') ?>

    <?= $form->field($model, 'DNI') ?>

    <?= $form->field($model, 'Tipo_documento') ?>

    <?= $form->field($model, 'Cuit_Cuil') ?>

    <?= $form->field($model, 'Fecha_Nacimiento') ?>

    <?php // echo $form->field($model, 'Nombre_RazonSocial') ?>

    <?php // echo $form->field($model, 'Sexo') ?>

    <?php // echo $form->field($model, 'Ciudadania') ?>

    <?php // echo $form->field($model, 'Telefono') ?>

    <?php // echo $form->field($model, 'Celular') ?>

    <?php // echo $form->field($model, 'Cod_postal') ?>

    <?php // echo $form->field($model, 'Direccion') ?>

    <?php // echo $form->field($model, 'Email') ?>

    <?php // echo $form->field($model, 'Estado_civil') ?>

    <?php // echo $form->field($model, 'Cod_actividad') ?>

    <?php // echo $form->field($model, 'Sucursal') ?>

    <?php // echo $form->field($model, 'Cuenta') ?>

    <?php // echo $form->field($model, 'Grupo') ?>

    <?php // echo $form->field($model, 'Subgrupo') ?>

    <?php // echo $form->field($model, 'Tipo_cuenta') ?>

    <?php // echo $form->field($model, 'Codigo_pais') ?>

    <?php // echo $form->field($model, 'ID_Provincia') ?>

    <?php // echo $form->field($model, 'ID_Localidad') ?>

    <?php // echo $form->field($model, 'Tipo_domicilio') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
