<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\TipoDestinatario;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\DestinatariosSearch */
/* @var $form yii\widgets\ActiveForm */
?>
<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="estilo-buscador">
</head>
<body>

    <div class="destinatarios-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',

    ]); ?> 
    <div class="form-row align-items-center">
        <div class="col-lg-5">
            <?php echo $form->field($model, 'Cuit_Cuil_DES', [
                    'inputOptions' => [
                     'placeholder' => $model->getAttributeLabel('CUIT/CUIL'),
                     'class' => 'form-control mb-2'],
                    ])->label(false); ?>
        </div>
        <div class="col-lg-5">  
            <?= $form->field($model, 'ID_Tipo_DES')->dropDownList(ArrayHelper::map(TipoDestinatario::find()->all(), 'ID_TIPO', 'Tipo'),['prompt'=>'-- Selecciona tipo de Persona --'])->label(false);?>      
        </div>
        <div class="form-group">
            <?= Html::submitButton('Buscar', ['class' => 'btn bg-navy color-palette']) ?>
             <?= Html::a('Borrar', ['index'], ['class' => 'btn bg-purple-active color-palette']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>

</body>
</html>

