<?php

use app\models\Usuarios;
use yii\helpers\Html;
use yii\grid\GridView;
use app\models\pais;
use yii\bootstrap\Modal;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\models\DestinatariosSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */


$this->title = ' ';
$this->params['breadcrumbs'][] = 'Destinatarios';
?>
<link rel="stylesheet" type="text/css" href="../web/css/titulos.css">
<div class="destinatarios-index">


    <h1 id="titulo"><?php echo '<b>' . 'DESTIN' . '</b>' . 'ATARIOS' ?></h1>

    
    <div class="form-row align-items-center" style="padding-left: 15px">
        <p>
        <?php
        if (Usuarios::isUserOperador(Yii::$app->user->identity->id) or Usuarios::isUserFirmante(Yii::$app->user->identity->id)) { ?>
            <?= Html::a('CARGAR DESTINATARIOS DE BD BANCO RIOJA', '#', [
                'id' => 'activity-index-link',
                'class' => 'btn bg-purple color-palette',
                'data-toggle' => 'myModal',
                'data-target' => '#myModal',
                'data-url' => Url::to(['carga']),
                'data-pjax' => '0',
            ]);
        }?>
        <?= Html::a('CREAR', ['create'], ['class' => 'btn bg-navy color-palette']) ?>
    </div>
    </p>

    <?php echo $this->render('_search', ['model' => $searchModel]); ?>
    <?php if (Usuarios::isUserAdmin(Yii::$app->user->identity->getId())) { ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,

            'options' => ['style' => 'overflow-x:scroll;width:100%;'],

            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                'Nombre_RazonSocial_DES',
                'Cuit_Cuil_DES',
                [
                    'attribute' => 'ID_Tipo_DES',
                    'label' => 'Tipo',
                    'headerOptions' => ['style' => 'color:red'],
                    'value' => 'tipoDES.Tipo',
                ],
                'Numero_Casa_DES',
                'Email_DES',
                [
                    'attribute' => 'Codigo_Pais_DES',
                    'label' => 'Pais',
                    'value' => 'pais.Nombre_Pais',
                ],
                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]);
    } ?>

    <?php if (!Usuarios::isUserAdmin(Yii::$app->user->identity->getId())) { ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,

            'options' => ['style' => 'overflow-x:scroll;width:100%;'],

            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                'Nombre_RazonSocial_DES',
                'Cuit_Cuil_DES',
                [
                    'attribute' => 'ID_Tipo_DES',
                    'label' => 'Tipo',
                    'headerOptions' => ['style' => 'color:red'],
                    'value' => 'tipoDES.Tipo',
                ],
                'Numero_Casa_DES',
                'Email_DES',
                [
                    'attribute' => 'Codigo_Pais_DES',
                    'label' => 'Pais',
                    'value' => 'pais.Nombre_Pais',
                ],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{view} {update}',
                ],
            ]
        ]);
    } ?>

    <?php
    Modal::begin([
        'id' => 'myModal',
    ]); ?>

    <div id="content"></div>

    <?php Modal::end();
    ?>

</div>

<?php
$this->registerJs(
    "$(document).on('click', '#activity-index-link', (function() {
        $.get(
            $(this).data('url'),
            function (data) {
                $('#content').html(data);
                $('#myModal').modal('show');
            }
        );
    }));"

); ?>

