<?php

use yii\helpers\Html;
use app\models\TipoDestinatario;
use app\models\pais;
use app\models\provincias;

/* @var $this yii\web\View */
/* @var $model app\models\Destinatarios */


$this->title = ' ';
$this->params['breadcrumbs'][] = ['label' => 'Destinatarios', 'url' => ['index']];
$this->params['breadcrumbs'][] = $model->Nombre_RazonSocial_DES;
\yii\web\YiiAsset::register($this);
?>
<link rel="stylesheet" type="text/css" href="../web/css/titulos.css">


<br>
 
<section class="content">

      <div class="row">
        <div class="col-md-12">
          <div class="box box-primary">
            <div class="box-body box-profile">
             <!-- <img class="profile-user-img img-responsive img-circle" src="../../dist/img/user4-128x128.jpg" alt="User profile picture">-->

              <h3 class="profile-username text-center"><?= $model->Nombre_RazonSocial_DES ?></h3>

              <p class="text-muted text-center"><?= Html::encode(TipoDestinatario::findOne($model->ID_Tipo_DES)->Tipo) ?></p>

              <ul class="list-group list-group-unbordered">
                 <?php if (!empty($model->Direccion_DES)): ?>
                       <li class="list-group-item">
                        <b>DIRECCION</b><a class="pull-right"><?= Html::encode($model->Direccion_DES). ', '. Html::encode($model->Numero_Casa_DES).' - '. Html::encode(Pais::findOne($model->Codigo_Pais_DES)->Nombre_Pais) ?></a>
                             
                        </li>
                <?php endif; ?>
                <li class="list-group-item">
                      <b>CUIDAD</b> <a class="pull-right"><?=Html::encode(Provincias::findOne($model->ID_Provincia_DES)->nombre) ?></a>
                </li>    
               <li class="list-group-item">
                      <b>CUIT/CUIL</b> <a class="pull-right"><?= Html::encode(substr($model->Cuit_Cuil_DES, 0,2).'-'.substr($model->Cuit_Cuil_DES, 2,8) .'-'.substr($model->Cuit_Cuil_DES, -1,1)) ?></a>
                </li> 
                <li class="list-group-item">
                      <b>EMAIL</b> <a class="pull-right"><?=Html::encode($model->Email_DES) ?></a>
                </li>
              </ul>

              <?= Html::a('Actualizar', ['update', 'id' => $model->ID_DES], ['class' => '.btn-sm btn bg-purple color-palette col-lg-6']) ?>  
                <?= Html::a('Borrar', ['delete', 'id' => $model->ID_DES], [
                'class' => '.btn-sm btn bg-navy color-palette col-lg-6','data' => [ 
                        'confirm' => 'Esta seguro que desea eliminar este Destinatario?',
                        'method' => 'post',
                      ],
                ]) ?>
            </div>
          </div>
          <!-- /.box -->

       
      </div>
      <!-- /.row -->

    </section>
    <!-- /.content -->
 
