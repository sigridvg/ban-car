<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Destinatarios */


$this->title = '';
$this->params['breadcrumbs'][] = ['label' => 'Destinatarios', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->Nombre_RazonSocial_DES, 'url' => ['view', 'id' => $model->ID_DES]];
$this->params['breadcrumbs'][] = 'Actualizar';
?>
<link rel="stylesheet" type="text/css" href="../web/css/titulos.css">
<div class="destinatarios-index">


    <h1 id="titulo" style="text-align: center;"><?php echo '<b>' . 'ACTUA' . '</b>' . 'LIZAR' ?></h1>

 	</h1>


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
