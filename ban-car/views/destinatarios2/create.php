<?php

/* @var $this yii\web\View */
/* @var $model app\models\Destinatarios */

$this->title = ' ';
$this->params['breadcrumbs'][] = ['label' => 'Destinatarios', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;


?>
<link rel="stylesheet" type="text/css" href="../web/css/titulos.css">
<div class="destinatarios-create">

    <h1 id="titulo"><?php echo '<b>' . 'CREAR' . '</b>' . 'DESTINATARIOS' ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
