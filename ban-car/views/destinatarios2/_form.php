<?php

use app\models\Provincias;
use kartik\depdrop\DepDrop;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use app\models\TipoDestinatario;
use app\models\pais;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\Destinatarios */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="destinatarios-form">
    <div class="col-md-8 col-lg-12">
        <?php $form = ActiveForm::begin([
            'id' => 'destinatarios-form',
            'method' => 'post',
            'options' => ['class' => 'form-horizontal'],
        ]);
        $dataProvincias = ArrayHelper::map(Provincias::find()->asArray()->orderBy('nombre')->all(), 'id', 'nombre');
        ?>

        <div class="align-items-center">
            <div class="form-group">
                <div class="col-auto col-md-4">
                    <?= $form->field($model, 'Nombre_RazonSocial_DES', ['options' => ['class' => 'no-margin']])->textInput(['maxlength' => true]) ?>
                </div>
                <div class="col-auto col-md-4">
                    <?= $form->field($model, 'Cuit_Cuil_DES', ['options' => ['class' => 'no-margin']])->textInput() ?>
                </div>
                <div class="col-auto col-md-4">
                    <?= $form->field($model, 'ID_Tipo_DES', ['options' => ['class' => 'no-margin']])->dropDownList(ArrayHelper::map(TipoDestinatario::find()
                        ->all(), 'ID_TIPO', 'Tipo'))
                        ->label('Tipo de Destinatario') ?>
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="col-lg-4 col-md-4">
                <?= $form->field($model, 'Email_DES', ['options' => ['class' => 'no-margin']])->textInput() ?>
            </div>
            <div class="col-lg-4 col-md-4">
                <?= $form->field($model, 'Direccion_DES', ['options' => ['class' => 'no-margin']])->textInput() ?>
            </div>
            <div class="col-lg-4 col-md-4">
                <?= $form->field($model, 'Numero_Casa_DES', ['options' => ['class' => 'no-margin']])->textInput() ?>
            </div>
        </div>
        <div class="form-group">
            <div class="col-lg-4 col-md-4">
                <?= $form->field($model, 'Codigo_Pais_DES', ['options' => ['class' => 'no-margin']])->dropDownList(ArrayHelper::map(Pais::find()
                    ->all(), 'Cod_Pais', 'Nombre_Pais'))
                    ->label('Pais') ?>
            </div>
            <div class="col-lg-4 col-md-4">
                <?= $form->field($model, 'ID_Provincia_DES', ['options' => ['class' => 'no-margin']])
                    ->dropDownList($dataProvincias, ['prompt' => 'Elija una provincia', 'id' => 'id_prov'])
                    ->label('Provincia'); ?>
            </div>

            <div class="col-lg-4 col-md-4">
                <?= $form->field($model, 'ID_Localidad_DES')->widget(DepDrop::className(),
                    [
                        'pluginOptions' => [
                            'placeholder' => 'Elija una localidad',
                            'depends' => ['id_prov'],
                            'url' => Url::to(['/destinatarios/create'])
                        ]
                    ]
                )->label('Localidad'); ?>
            </div>

        </div>
        <div class="col-lg-offset-2 col-lg-7">
            <?= Html::submitButton('Guardar', ['class' => 'btn bg-navy color-palette btn-block', 'name' => 'actualizar-button']) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>


