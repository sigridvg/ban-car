<?php

use yii\helpers\Html;
use app\models\Roles;
use app\models\Sectores;
use yii\helpers\ArrayHelper;
use yii\base\Widget;
use yii\data\Pagination;
use yii\widgets\LinkPager;

/* @var $this yii\web\View */
/* @var $model app\models\Usuarios */

$this->title = ' ';
$this->params['breadcrumbs'][] = ['label' => 'Usuarios', 'url' => ['index']];
$this->params['breadcrumbs'][] = $model->NombreUs . ' ' . $model->ApellidoUs;
\yii\web\YiiAsset::register($this);
?>
<link rel="stylesheet" type="text/css" href="../web/css/titulos.css">
<br>
<div class="usuarios-view table-bordered table-hover " style="background:#dfdede;">

    <div class="table-bordered table-hover ">


        <section class="content">

            <div class="row">
                <div class="col-md-5">
                    <div class="box box-primary">
                        <div class="box-body box-profile">
                            <?= Html::img('@web/BAN-CAR2.png', ['alt'=>'Foto logo', 'class'=>'profile-user-img img-responsive img-circle']);?> 
                           
                            <h2 class="profile-username text-center"><?php echo '<b>' . $model->NombreUs . '</b>' . $model->ApellidoUs ?></h2>

                            <?php

                            if (!empty($model->IdSectores)): ?>
                                <p class="text-muted text-center">
                                    <i class="fa fa-fw fa-bank"></i> <?= Html::encode(Sectores::findOne($model->IdSectores)->Sectores) ?>
                                </p>
                            <?php endif; ?>

                            <ul class="list-group list-group-unbordered">
                                <?php if (!empty($model->IdRoles)): ?>
                                    <li class="list-group-item">
                                        <i class=" fa fa-fw fa-user"></i> <b>ROL</b> <a
                                                class="pull-right"><?= Html::encode(Roles::findOne($model->IdRoles)->Roles) ?> </a>
                                    </li>
                                <?php endif; ?>

                                <li class="list-group-item">
                                    <i class="fa fa-fw fa-italic"></i> <b>LEGAJO</b> <a class="pull-right">
                                        <?= Html::encode($model->LegajoUs) ?></a>
                                </li>
                                 <li class="list-group-item">
                                    <i class="fa fa-fw fa-calendar-check-o"></i><b>DOCUMENTO:</b><a
                                            class="pull-right"><?=Html::encode($model->documento)?></a>

                                </li>
                                <li class="list-group-item">
                                    <i class="fa fa-fw fa-calendar-check-o"></i><b>ULTIMA MODIFICACION:</b><a
                                            class="pull-right"><?= ' ' . Yii::$app->formatter->asDate($model->tiempo_ultima_modificacion, 'long'); ?> </a>

                                </li>

                               
                                <li class="list-group-item">
                                    <i class="fa fa-fw fa-calendar-check-o"></i> <b>ULTIMO ACCESO:</b> <a
                                            class="pull-right"><?= Yii::$app->formatter->asDate($model->tiempo_ultimo_logueo, 'long'); ?></a>

                                </li>
                                <li class="list-group-item">
                                    <i class="fa fa-fw fa-calendar-check-o"></i><b>ULTIMA MODIFICACION:</b><a
                                            class="pull-right"><?= ' ' . Yii::$app->formatter->asDate($model->tiempo_ultima_modificacion, 'long'); ?> </a>

                                </li>
                                <li class="list-group-item">
                                    <i class="fa fa-fw fa-cog"></i><b>IP DE REGISTRACION: </b><a
                                            class="pull-right"><?= Html::encode($model->ip_registrada) ?></a>
                                </li>
                               <?php if (!empty($model->signature)): ?>
                                <li class="list-group-item">
                                        <i class="fa fa-fw fa-file-picture-o"></i><b>FIRMA: </b>
                                    <?= Html::img( Yii::getAlias('@web') . '/firmas/' . $model->signature, ['alt' => 'firma',  'style' => 'max-width:100%; height: auto; align:right']); ?>
                                    </li>
                                <?php endif ?>
                                    
                               
                            </ul>

                            <?php 
                                
                            if ( $model->IdRoles != 1 ) {
                                echo Html::a('Actualizar', ['update', 'id' => $model->IdUsuarios], [
                                'class' => '.btn-sm btn bg-navy color-palette col-lg-4'
                               ]);
                            echo Html::a('Borrar', ['delete', 'id' => $model->IdUsuarios], [
                                'class' => '.btn-sm btn bg-purple color-palette col-lg-4',
                                'data' => [
                                    'confirm' => 'Esta seguro que desea eliminar este usuario?',
                                    'method' => 'post',
                                ],
                            ]); 
                            echo Html::a('Resetear Contraseña', ['reset', 'id' => $model->IdUsuarios], [
                                'class' => '.btn-sm btn bg-black color-palette col-lg-4',
                                'data' => [
                                    'confirm' => 'Resetear contraseña?',
                                    'method' => 'post',
                                        ],
                                    ]);
                                }
                                    
                            ?>

                        </div>
                    </div>
                    <!-- /.box -->


                    <!-- /.box -->
                </div>
                <!-- /.col -->
                <div class="col-md-7">
                    <div class="nav-tabs-custom">
                        <ul class="nav nav-tabs">

                            <li class="active"><a href="#timeline" data-toggle="tab">Timeline</a></li>
                        </ul>
                        <div class="tab-content">


                            <div class="active tab-pane" id="timeline">
                                <?php

                                if ($totalcount == 0) {
                                    echo "Sin registros";
                                } else {

                                    for ($n = 1; $n < $limite; $n++) { ?>
                                        <ul class="timeline timeline-inverse">
                                            <li class="time-label">
                        <span class="bg-purple">
                          <?php
                          print_r($dataProvider[$n]['fecha_AUD']);
                          ?>
                        </span>
                                            </li>
                                            <li>

                                                <?php if ($dataProvider[$n]['modulo_AUD'] == 'DESTINATARIOS' or $dataProvider[$n]['modulo_AUD'] == 'USUARIOS') { ?>
                                                    <i class="fa fa-user bg-navy"></i>
                                                <?php } elseif ($dataProvider[$n]['modulo_AUD'] == 'CARTA' or $dataProvider[$n]['modulo_AUD'] == 'PLANTILLA') { ?>
                                                    <i class="fa fa-envolope bg-navy"></i>
                                                <?php }; ?>


                                                <div class="timeline-item">
                                                    <span class="time"><i
                                                                class="fa fa-clock-o"></i><?php print_r(Yii::$app->formatter->asTime($dataProvider[$n]['hora_AUD'])); ?></span>

                                                    <h3 class="timeline-header"><a
                                                                href="#"><?php print_r($dataProvider[$n]['modulo_AUD']); ?></a> <?php print_r($dataProvider[$n]['accion_AUD']); ?>
                                                    </h3>

                                                    <div class="timeline-body">
                                                        <?php print_r($dataProvider[$n]['descripcion_AUD']); ?>
                                                    </div>
                                                </div>
                                            </li>
                                            <!-- END timeline item -->
                                        </ul>
                                    <?php }
                                }

                                echo LinkPager::widget([
                                    'pagination' => $pagination,
                                ]);
                                ?>
                            </div>

                            <!-- /.tab-pane -->


                            <!-- /.tab-pane -->
                        </div>
                        <!-- /.tab-content -->
                    </div>
                    <!-- /.nav-tabs-custom -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->

        </section>
        <!-- /.content -->
    </div>