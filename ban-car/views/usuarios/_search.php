<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Roles;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\UsuariosSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="usuarios-search">
    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => ['class' => 'form-horizontal'],
    ]); ?>
    <div class="form-row align-items-center">
        <div class="col-lg-4">
            <?php echo $form->field($model, 'emailUs', [
                'inputOptions' => [
                   'placeholder' => $model->getAttributeLabel('emailUs'),
                   'class' => 'form-control mb-2'],
                   'options' => ['class' => 'no-margin']
               ])->label(false); ?>
           </div>
           <div class="col-lg-4">   
            <?= $form->field($model, 'LegajoUs', [
                'inputOptions' => [
                   'placeholder' => $model->getAttributeLabel('LegajoUs'),
                   'class' => 'form-control mb-2 no-margin'],
                   'options' => ['class' => 'no-margin']
               ])->label(false); ?>
           </div>
           <div class="col-lg-4">  
            <?= $form->field($model, 'IdRoles', ['options' => ['class' => 'no-margin']])->dropDownList(ArrayHelper::map(Roles::find()
              ->all(), 'ID_ROLES', 'Roles'),['prompt'=>'-- Selecciona Rol --.'])
              ->label(false);?>
          </div>
          <div class="col-lg-4">  
             <?= Html::submitButton('Buscar', ['class' => 'btn bg-navy-active']) ?>
             <?= Html::a('Borrar', ['index'], ['class' => 'btn bg-purple-active color-palette']) ?>
         </div>
     </div>
     <?php ActiveForm::end(); ?>
 </div>

