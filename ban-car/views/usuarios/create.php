<?php

/* @var $this yii\web\View */
/* @var $model app\models\Usuarios */


$this->title = ' ';
$this->params['breadcrumbs'][] = ['label' => 'Usuarios', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Nuevo Usuario';
?>
<link rel="stylesheet" type="text/css" href="../web/css/titulos.css">
<div class="usuarios-create">

 	<h1 id="titulo" style="text-align: center;"><?php echo '<b>'.'CREAR'.'</b>'.'USUARIO' ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
