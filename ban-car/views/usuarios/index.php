<?php

/* @var $this yii\web\View */
/* @var $searchModel app\models\UsuariosSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
use yii\helpers\Html;
use yii\helpers\Url;
 use yii\grid\GridView;

$this->title = ' ';
$this->params['breadcrumbs'][] = ['label' => 'Usuarios', 'url' => ['index']];

?>
<link rel="stylesheet" type="text/css" href="../web/css/titulos.css">
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
<br>

 <?php echo $this->render('_search', ['model' => $searchModel]); ?>

<div class="table-bordered table-hover ">
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-body box-profile">
                            <div class="nav-tabs-custom">
                                <ul class="nav nav-tabs">

                                    <li class="active"><a href="#users" data-toggle="tab"><i class="fa fa-fw fa-user"></i>Usuarios</a>
                                    </li>
                                    <li><a href="#admin" data-toggle="tab"><i class="fa fa-fw fa-cog"></i>Administradores</a></li>
                                </ul>
                                <div class="tab-content">
                                    <!--Admin-->

                                    <div class="tab-pane" id="admin">
                                        <?= GridView::widget([
                                            'dataProvider' => $dataProviderAdmin,
                                               'options' => ['style' => 'overflow-x:scroll;width:100%;'],
                                            'columns' => [
                                                ['class' => 'yii\grid\SerialColumn'],

                                                [
                                                   'attribute' => 'NombreUs',
                                                   'headerOptions' => ['style' => 'text-align: center;'],
                                                    'label' => 'Nombre',
                                                ],
                                                [
                                                   'attribute' => 'ApellidoUs',
                                                   'headerOptions' => ['style' => 'text-align: center;justify-content:center;'],
                                                    'label' => 'Apellido',
                                                ],
                                                [
                                                   'attribute' => 'LegajoUs',
                                                   'headerOptions' => ['style' => 'text-align: center;'],
                                                    'label' => 'Legajo',
                                                ],
                                                [
                                                   'attribute' => 'IdRoles',
                                                   'headerOptions' => ['style' => 'text-align: center;'],
                                                    'label' => 'Rol',
                                                    'value' => 'roles.Roles',
                                                ],
                                                [
                                                   'attribute' => 'IdSectores',
                                                    'label' => 'Sector',                
                                                   'headerOptions' => ['style' => 'text-align: center;'],
                                                    'value' => 'sectores.Sectores',
                                                ],
                                                [
                                                   'attribute' => 'usernameUs',
                                                   'headerOptions' => ['style' => 'text-align: center;'],
                                                    'label' => 'Nombre de Usuario',
                                                ],
                                                [
                                                   'attribute' => 'emailUs',
                                                   'headerOptions' => ['style' => 'text-align:center;
                                                                                   width:30%;'],
                                                    'label' => 'Email',
                                                ],
                                                [
                                                   'attribute' => 'tiempo_creacion',
                                                   'headerOptions' => ['style' => 'text-align:center;
                                                                                   width:10%;'],
                                                    'label' => 'Tiempo de Creacion',
                                                ],
                                    /*           [
                                                   'attribute' => 'tiempo_ultima_modificacion',
                                                   'headerOptions' => ['style' => 'text-align: center;'],
                                                    'label' => 'Ultima Modificacion',
                                                ],*/
                                                [
                                                   'attribute' => 'tiempo_ultimo_logueo',
                                                   'headerOptions' => ['style' => 'text-align: center;'],
                                                    'label' => 'Ultimo logueo',
                                                ],
                                                [
                                                    'class' => 'yii\grid\ActionColumn',
                                                    'template' => '{view}{reset}',
                                                    'buttons' => [
                                                        'view' => function ($url) {
                                                            return Html::a('<span class="glyphicon glyphicon-eye-open "></span>', $url);
                                                        },
                                                        'reset' => function ($url) {
                                                            return Html::a('<span class="glyphicon glyphicon-repeat "></span>', $url,
                                                                [
                                                                    'title' => Yii::t('app', 'Resetear Clave'),             
                                                                ]);
                                                        }
                                                    ],
                                                    'urlCreator' => function ($action, $model) {
                                                        if ($action === 'reset') {
                                                            return Url::to([$action, 'id' => $model->id]);
                                                        } elseif($action === 'view') {
                                                            return Url::to([$action, 'id' => $model->id]);
                                                        }
                                                    }
                                                ],

                                            ],
                                        ]); ?>
                                    </div>


                                    <!-- Users -->
                                    <div class="active tab-pane" id="users">


                                        <?= GridView::widget([
                                            'dataProvider' => $dataProvider,
                                            'options' => ['style' => 'overflow-x:scroll;width:100%;'],
                                            'columns' => [
                                                ['class' => 'yii\grid\SerialColumn'],

                                                [
                                                   'attribute' => 'NombreUs',
                                                   'headerOptions' => ['style' => 'text-align: center;'],
                                                    'label' => 'Nombre',
                                                ],
                                                [
                                                   'attribute' => 'ApellidoUs',
                                                   'headerOptions' => ['style' => 'text-align: center;justify-content:center;'],
                                                    'label' => 'Apellido',
                                                ],
                                                [
                                                   'attribute' => 'LegajoUs',
                                                   'headerOptions' => ['style' => 'text-align: center;'],
                                                    'label' => 'Legajo',
                                                ],
                                                [
                                                   'attribute' => 'IdRoles',
                                                   'headerOptions' => ['style' => 'text-align: center;'],
                                                    'label' => 'Rol',
                                                    'value' => 'roles.Roles',
                                                ],
                                                [
                                                   'attribute' => 'IdSectores',
                                                    'label' => 'Sector',                
                                                   'headerOptions' => ['style' => 'text-align: center;'],
                                                    'value' => 'sectores.Sectores',
                                                ],
                                                [
                                                   'attribute' => 'usernameUs',
                                                   'headerOptions' => ['style' => 'text-align: center;'],
                                                    'label' => 'Nombre de Usuario',
                                                ],
                                                [
                                                   'attribute' => 'emailUs',
                                                   'headerOptions' => ['style' => 'text-align:center;
                                                                                   width:30%;'],
                                                    'label' => 'Email',
                                                ],
                                                [
                                                   'attribute' => 'tiempo_creacion',
                                                   'headerOptions' => ['style' => 'text-align:center;
                                                                                   width:10%;'],
                                                    'label' => 'Tiempo de Creacion',
                                                ],
                                    /*           [
                                                   'attribute' => 'tiempo_ultima_modificacion',
                                                   'headerOptions' => ['style' => 'text-align: center;'],
                                                    'label' => 'Ultima Modificacion',
                                                ],*/
                                                [
                                                   'attribute' => 'tiempo_ultimo_logueo',
                                                   'headerOptions' => ['style' => 'text-align: center;'],
                                                    'label' => 'Ultimo logueo',
                                                ],
                                                [
                                                    'class' => 'yii\grid\ActionColumn',
                                                    'template' => '{view}{update}{delete}{reset}',
                                                    'buttons' => [
                                                        'view' => function ($url) {
                                                            return Html::a('<span class="glyphicon glyphicon-eye-open "></span>', $url);
                                                        },

                                                        'update'=> function ($url) {
                                                               return Html::a('<span class="glyphicon glyphicon-pencil "></span>', $url);
                                                        },
                                                        'delete' => function ($url) {
                                                              return Html::a('<span class="glyphicon glyphicon-trash "></span>', $url,[
                                                                  'data-method' => 'post',
                                                                  'data-confirm' => 'Esta seguro que desea eliminar este usuario?',
                                                              ]);
                                                        },
                                                        'reset' => function ($url) {
                                                            return Html::a('<span class="glyphicon glyphicon-repeat "></span>', $url,
                                                                [
                                                                    'data' => [
                                                                            'confirm' => 'Resetear contraseña?',
                                                                            'method' => 'post',
                                                                                ],
                                                                    'title' => Yii::t('app', 'Resetear Clave'),             
                                                                ]);
                                                        }
                                                    ],
                                                    'urlCreator' => function ($action, $model) {
                                                        if ($action === 'reset') {
                                                            return Url::to([$action, 'id' => $model->id]);
                                                        } elseif($action === 'view') {
                                                            return Url::to([$action, 'id' => $model->id]);
                                                        } elseif ($action === 'update') {
                                                            return Url::to([$action, 'id' => $model->id]);
                                                        } elseif ($action === 'delete'){
                                                            return Url::to([$action, 'id' => $model->id]);
                                                        }
                                                    }
                                                ],

                                            ],
                                        ]); ?>
                                      </div>

                                </div>
                                <!-- /.tab-content -->
                            </div> 
                        <!-- /.nav tab custom -->
                    </div>
                    <!-- /. box profile -->
                </div>
                <!-- /.box.primary -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
