<?php

use yii\helpers\Html;
use app\models\Roles;
use app\models\Sectores;
use yii\helpers\ArrayHelper;
use yii\web\YiiAsset;
use yii\widgets\ActiveForm;
use yii\widgets\LinkPager;

/* @var $this yii\web\View */
/* @var $model app\models\Usuarios */

$this->title = ' ';
$this->params['breadcrumbs'][] = ['label' => 'Usuarios', 'url' => ['index']];
$this->params['breadcrumbs'][] = $model->NombreUs . ' ' . $model->ApellidoUs;
YiiAsset::register($this);
?>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
<br>


<div class="table-bordered table-hover ">
    <section class="content">
        <div class="row">
            <div class="col-md-5">
                <div class="box box-primary">
                    <div class="box-body box-profile">
                        <?= Html::img('@web/BAN-CAR2.png', ['alt'=>'Foto logo', 'class'=>'profile-user-img img-responsive img-circle']);?> 
                        <h2 class="profile-username text-center"><?php echo '<b>' . $model->NombreUs . '</b>' . $model->ApellidoUs ?></h2>

                        <?php if (!empty($model->IdSectores)): ?>
                            <p class="text-muted text-center">
                                <i class="fa fa-fw fa-bank"></i> <?= Html::encode(Sectores::findOne($model->IdSectores)->Sectores) ?>
                            </p>
                        <?php endif; ?>

                        <ul class="list-group list-group-unbordered">
                            <?php if (!empty($model->IdRoles)): ?>
                                <li class="list-group-item">
                                    <i class=" fa fa-fw fa-user"></i> <b>ROL</b> <a
                                            class="pull-right"><?= Html::encode(Roles::findOne($model->IdRoles)->Roles) ?> </a>
                                </li>
                            <?php endif; ?>
                            <li class="list-group-item">
                                <i class="fa fa-fw fa-italic"></i> <b>LEGAJO</b> <a class="pull-right">
                                    <?= Html::encode($model->LegajoUs) ?></a>
                            </li>

                            <li class="list-group-item">
                                <i class="fa fa-fw fa-calendar-check-o"></i><b>CREADO EL:</b><a
                                        class="pull-right"><?= Yii::$app->formatter->asDate($model->tiempo_creacion, 'long'); ?></a>

                            </li>
                            <li class="list-group-item">
                                <i class="fa fa-fw fa-calendar-check-o"></i> <b>ULTIMO ACCESO:</b> <a
                                        class="pull-right"><?= Yii::$app->formatter->asDate($model->tiempo_ultimo_logueo, 'long'); ?></a>

                            </li>
                            <li class="list-group-item">
                                <i class="fa fa-fw fa-calendar-check-o"></i><b>ULTIMA MODIFICACION:</b><a
                                        class="pull-right"><?= Yii::$app->formatter->asDate($model->tiempo_ultima_modificacion, 'long'); ?> </a>

                            </li>
                            <li class="list-group-item">
                                <i class="fa fa-fw fa-cog"></i><b>IP DE REGISTRACION: </b><a
                                        class="pull-right"><?= Html::encode($model->ip_registrada) ?></a>

                            </li>
                            <?php if (!empty($model->signature)): ?>
                                <li class="list-group-item">
                                    <i class="fa fa-fw fa-file-picture-o"></i><b>Firma: </b><?= Html::img( Yii::getAlias('@web') . '/firmas/' . $model->signature, ['alt' => 'firma', 'class' => 'user-image', 'style' => 'max-width:100%; height: auto; align:right']); ?>
                                </li>
                            <?php endif ?>

                        </ul>
                    </div>
                </div>
            </div>

            <!-- PANELES DEL LADO DERECHO -->
            <div class="col-md-7">
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">

                        <li class="active"><a href="#timeline" data-toggle="tab"><i class="fa fa-fw fa-clock-o"></i>Timeline</a>
                        </li>
                        <li><a href="#settings" data-toggle="tab"><i class="fa fa-fw fa-cog"></i>Configuracion</a></li>
                    </ul>
                    <div class="tab-content">

                        <!-- TIMELINE -->
                        <div class="active tab-pane" id="timeline">
                            <?php
                            if ($totalcount == 0) {
                                echo 'Sin registros';

                            } else {
                                //if ($dataProvider == $totalcount) {
                                for ($n = 1; $n < $limite; $n++) { ?>
                                    <ul class="timeline timeline-inverse">
                                        <li class="time-label">
                        <span class="bg-purple">
                          <?php
                          print_r($dataProvider[$n]['fecha_AUD']);
                          ?>
                        </span>
                                        </li>
                                        <li>

                                            <?php if ($dataProvider[$n]['modulo_AUD'] == 'DESTINATARIOS' or $dataProvider[$n]['modulo_AUD'] == 'USUARIOS') { ?>
                                                <i class="fa fa-user bg-navy"></i>
                                            <?php } elseif ($dataProvider[$n]['modulo_AUD'] == 'CARTA' or $dataProvider[$n]['modulo_AUD'] == 'PLANTILLA') { ?>
                                                <i class="fa fa-envelope bg-navy"></i>
                                            <?php } ?>


                                            <div class="timeline-item">
                                                <span class="time"><i
                                                            class="fa fa-clock-o"></i><?php print_r($dataProvider[$n]['hora_AUD']); ?></span>

                                                <h3 class="timeline-header"><?php print_r($dataProvider[$n]['modulo_AUD']); ?><?php echo "&nbsp;";
                                                    print_r($dataProvider[$n]['accion_AUD']); ?></h3>

                                                <div class="timeline-body">
                                                    <?php print_r($dataProvider[$n]['descripcion_AUD']); ?>
                                                </div>
                                            </div>
                                        </li>
                                        <!-- END timeline item -->
                                    </ul>
                                <?php }
                            }
                            echo LinkPager::widget([
                                'pagination' => $pagination,
                            ]); ?>
                        </div>


                        <!--CONFIGURACION-->

                        <div class="tab-pane" id="settings">
                            <?php $form = ActiveForm::begin([
                                'id' => 'usuario-form',
                                'method' => 'post',
                            ]); ?>
                            <div class="form-group">
                                <?= $form->field($model, 'NombreUs')->textInput(['autofocus' => true, 'disabled' => true]) ?>


                                <?= $form->field($model, 'ApellidoUs')->textInput(['autofocus' => true, 'disabled' => true]) ?>

                            </div>
                            <div class="form-group">
                                <?= $form->field($model, 'usernameUs')->textInput(['autofocus' => true, 'disabled' => true]) ?>
                            </div>
                            <div class="form-group">
                                <?= $form->field($model, 'LegajoUs')->textInput(['autofocus' => true, 'disabled' => true]) ?>

                            </div>
                            <div class="form-group">

                                <?= $form->field($model, 'IdSectores')->textInput(['value' => Sectores::findOne($model->IdSectores)->Sectores, 'disabled' => true])?>

                                <?= $form->field($model, 'emailUs')->textInput(['disabled' => true]) ?>
                            </div>
                            <div class="form-group col-md-12">
                                <?php if (!empty($model->signature)) { ?>
                                    <?= Html::img( Yii::getAlias('@web') . '/firmas/' . $model->signature, ['alt' => 'firma',  'style' => 'max-width:100%; height: auto; align:right']); ?>
                                    
                                    <?= $form->field($model, 'signature')->fileInput();
                                } ?>
                            </div>

                            <div class="form-group">
                               <?= Html::submitButton('Guardar', ['class' => 'btn bg-navy color-palette btn-block']) ?>
                            </div>
                            <?php ActiveForm::end(); ?>
                        </div>
                        <!-- /.tab-pane -->
                    </div>
                    <!-- /.tab-content -->
                </div>
                <!-- /.nav-tabs-custom -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->

    </section>
    <!-- /.content -->
</div>