<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Roles;
use app\models\Sectores;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\Usuarios */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="usuarios-form">
           <div class="col-md-auto col-lg-auto">
            <?php $form = ActiveForm::begin([
                'enableAjaxValidation' => false, 
                'id' => 'usuario-form',
                'method' =>'post',
                'options' => ['class' => 'form-horizontal'],
            ]); ?>
            <div class="table-bordered table-hover ">  
                <section class="content">
                    <div class="row">
              <div class="form-group">
                <div class="col-lg-4 col-md-2">
                    <?= $form->field($model, 'NombreUs', ['options' => ['class' => 'no-margin']])->textInput() ?>
                </div>
                <div class="col-lg-4 col-md-2">
                    <?= $form->field($model, 'ApellidoUs', ['options' => ['class' => 'no-margin']])->textInput() ?>
                </div>
                <div class="col-lg-4 col-md-2">
                    <?= $form->field($model, 'LegajoUs', ['options' => ['class' => 'no-margin']])->textInput() ?>
                </div>
                
              </div>
           
            <div class="form-group">
                
                <div class="col-lg-6 col-md-2">
                    <?= $form->field($model,  'IdRoles', ['options' => ['class' => 'no-margin']])
                      ->dropDownList(ArrayHelper::map(Roles::find()
                      ->all(), 'ID_ROLES', 'Roles'))
                      ->label('Rol')?>
                </div>
                 <div class="col-lg-6 col-md-2">
                    <?= $form->field($model,  'IdSectores', ['options' => ['class' => 'no-margin']])
                      ->dropDownList(ArrayHelper::map(Sectores::find()
                      ->all(), 'ID_SECTORES', 'Sectores'))
                      ->label('Sector')?>
                </div>
            </div>
                
           <div class="form-group">
            <div class="col-md-6 col-md-6" >   
                <?=$form->field($model, 'emailUs', ['options' => ['class' => 'no-margin']])->textInput()?>
                </div>
               
                <div class="col-md-6 col-md-6" >   
                <?=$form->field($model, 'documento', ['options' => ['class' => 'no-margin']])?>
                </div>
           </div>
               <div class="form-group">
                <?php 
                if (!empty($model->signature)) {?>
                    <div class="col-md-6 col-1">
                        <?php 
                          echo Html::img(Yii::getAlias('@web') . '/firmas/' . $model->signature); 
                        ?>
                    </div>
                    <div class="col-md-6">
                        <?php echo $form->field($model, 'signature', ['options' => ['class' => 'no-margin']])->fileInput();?>
                   </div>
          <?php } else { ?>
                   <div class="col-md-12">
                        <?php 
                        echo $form->field($model, 'signature', ['options' => ['class' => 'no-margin']])->fileInput();
                        ?>
                   </div>
               </div>
          <?php  } ?>
               
            <div class="col-lg-offset-2 col-lg-7">
              <?= Html::submitButton('Guardar', ['class' => 'btn dissable bg-navy btn-block', 'id' => 'create-button']) ?> 
            </div>

          <?php ActiveForm::end(); ?>
             </div>
        </section>
    </div>
  </div>
</div>
