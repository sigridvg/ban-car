<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Usuarios */

$this->title = $model->getFullName();
$this->params['breadcrumbs'][] = ['label' => 'Usuarios', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->NombreUs. ' ' .$model->ApellidoUs, 'url' => ['view', 'id' => $model->IdUsuarios]];
$this->params['breadcrumbs'][] = 'Actualizar';
?>
<link rel="stylesheet" type="text/css" href="../web/css/titulos.css">
<div class="usuarios-update">
<?php if (Yii::$app->session->hasFlash('DeleteImageFailure')) { ?>
        <div class="alert alert-success">
            <h6><?= "No se encontro imagen de firma" ?></h6>
        </div>  
<?php } ?>

    <h1 id="titulo">
    	<?php echo '<b>'.'ACTUA'.'</b>'.'LIZAR' ?> 
 	</h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
