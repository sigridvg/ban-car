<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\sectores */

$this->title = 'Sectores';
$this->params['breadcrumbs'][] = ['label' => 'Sectores', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sectores-create">

   <h1 id="titulo" style="text-align: center;"><?php echo '<b>CARGAR' . '' . '</b>SECTORES' . '' ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
