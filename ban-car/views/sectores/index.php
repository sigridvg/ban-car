<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\sectoresSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Sectores';
$this->params['breadcrumbs'][] = $this->title;
?>

<link rel="stylesheet" type="text/css" href="../web/css/titulos.css">
<div class="sectores-index">

     <h1 id="titulo"><?php echo '<b>' . 'SECTO' . '</b>' . 'RES' ?></h1>

    <p>
          <?= Html::a('Cargar Sector', ['create'], ['class' => 'btn bg-purple']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            

            'ID_SECTORES',
            'Sectores',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
