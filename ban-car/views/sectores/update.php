<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\sectores */

$this->title = '';
$this->params['breadcrumbs'][] = ['label' => 'Sectores', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->ID_SECTORES, 'url' => ['view', 'id' => $model->ID_SECTORES]];
$this->params['breadcrumbs'][] = 'Update';
?>
<link rel="stylesheet" type="text/css" href="../web/css/titulos.css">
<div class="sectores-update">

    <h1 id="titulo" style="text-align: center;"><?php echo '<b>' . 'ACTUALIZAR' . '</b>' . 'SECTOR' ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
