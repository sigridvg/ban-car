<?php

use yii\helpers\Html;
use app\models\Sectores;

/* @var $this yii\web\View */
/* @var $model app\models\motivo */

$this->title = '';
$this->params['breadcrumbs'][] = ['label' => 'Motivos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<link rel="stylesheet" type="text/css" href="../web/css/titulos.css">
<br>
<section class="content">

      <div class="row">
        <div class="col-md-12">
          <div class="box box-primary">
            <div class="box-body box-profile">
             <!-- <img class="profile-user-img img-responsive img-circle" src="../../dist/img/user4-128x128.jpg" alt="User profile picture">-->

              <h3 class="profile-username text-center">Motivos</h3>

              <ul class="list-group-unbordered">
                <li>
                    <i class="glyphicon glyphicon-user text-muted"></i> <strong>Motivo:</strong> 
                    <a class="pull-right"><?= Html::encode($model->motivos) ?></a>
                </li>
                
              </ul>
               <?= Html::a('Actualizar', ['update', 'id' => $model->id], ['class' => '.btn-sm btn bg-purple color-palette col-lg-6']) ?>  
                <?= Html::a('Borrar', ['delete', 'id' => $model->id], [
                'class' => '.btn-sm btn bg-navy color-palette col-lg-6','data' => [ 
                        'confirm' => 'Esta seguro que desea eliminar este Motivo?',
                        'method' => 'post',
                      ],
                ]) ?>
            </div>
          </div>
          <!-- /.box -->

       
      </div>
      <!-- /.row -->

    </section>
    <!-- /.content -->