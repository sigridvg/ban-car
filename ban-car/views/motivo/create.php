<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\motivo */

$this->title = 'Motivos';
$this->params['breadcrumbs'][] = ['label' => 'Motivos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="motivo-create">

    <h1 id="titulo" style="text-align: center;"><?php echo '<b>CARGAR' . '' . '</b>MOTIVOS' . '' ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
