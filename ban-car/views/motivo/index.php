<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\motivosSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Motivos';
$this->params['breadcrumbs'][] = $this->title;
?>
<link rel="stylesheet" type="text/css" href="../web/css/titulos.css">
<div class="motivo-index">

    <h1 id="titulo"><?php echo '<b>' . 'MOTI' . '</b>' . 'VOS' ?></h1>

    <p>
        <?= Html::a('Cargar Motivo', ['create'], ['class' => 'btn bg-purple']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'id',
            'motivos',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
