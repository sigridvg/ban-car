<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\motivo */

$this->title = '';
$this->params['breadcrumbs'][] = ['label' => 'Motivos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<link rel="stylesheet" type="text/css" href="../web/css/titulos.css">
<div class="motivo-update">

   <h1 id="titulo" style="text-align: center;"><?php echo '<b>' . 'ACTUALIZAR' . '</b>' . 'MOTIVO' ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
