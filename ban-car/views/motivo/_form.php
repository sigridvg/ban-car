<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;


/* @var $this yii\web\View */
/* @var $model app\models\motivo */
/* @var $form yii\widgets\ActiveForm */

?>
<div class="motivo-form">
<?php $form = ActiveForm::begin(); ?>
<section class="content">
  
            <div class="col-lg-offset-2 col-md-7 col-lg-7">
             <?= $form->field($model, 'motivos')->textInput(['maxlength' => true]) ?>
             
            </div>  
            <div class="col-lg-offset-2 col-lg-7">
              <?= Html::submitButton('Guardar', ['class' => 'btn dissable bg-navy btn-block']) ?>
            </div>

    <?php ActiveForm::end(); ?>
   
    </section>
</div>
