<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Motivo;
use app\models\Sectores;

/* @var $this yii\web\View */
/* @var $model app\models\CartasSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cartas-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>
    <div class="form-row align-items-center">
        <div class="col-lg-4">
           <?= $form->field($model,  'motivo_plantilla', ['options' => ['class' => 'no-margin']])
                      ->dropDownList(ArrayHelper::map(Motivo::find()
                      ->all(), 'id', 'motivos'))
                      ->label(false)?> 
        </div>
        <div class="col-lg-4">
           <?= $form->field($model,  'sector', ['options' => ['class' => 'no-margin']])
                      ->dropDownList(ArrayHelper::map(Sectores::find()
                      ->all(), 'ID_SECTORES', 'Sectores'))
                      ->label(false)?> 
        </div>
        <div class="col-lg-4">
            <?= $form->field($model, 'archivo', [
                    'inputOptions' => [
                     'placeholder' => $model->getAttributeLabel('plantilla'),
                     'class' => 'form-control mb-2',],
                    ])->label(false); ?>
        </div>
        <div class="form-group">
             <?= Html::submitButton('Buscar', ['class' => 'btn bg-navy-active']) ?>
            <?= Html::a('Limpiar', ['index'], ['class' => 'btn bg-purple-active color-palette']) ?>
        </div>
    </div>
    <?php ActiveForm::end(); ?>

</div>

