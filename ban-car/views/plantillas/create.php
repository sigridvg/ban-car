<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

/* @var $model app\models\Plantillas */

$this->title = ' ';
$this->params['breadcrumbs'][] = ['label' => 'Plantillas', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Nueva Plantilla';
?>
    <link rel="stylesheet" type="text/css" href="../web/css/titulos.css">
    <div class="plantillas-create">

<?php if (Yii::$app->session->hasFlash('plantillaCreada')): ?>
    <?php $this->title = 'Plantilla subida!' ?>

    <p>La plantilla se cre&oacute; exitosamente y esta lista para ser usada.</p>

    <?= Html::a('Volver a plantillas', ['/plantillas'], ['class' => 'btn dissable bg-navy']) ?>

<?php elseif (Yii::$app->session->hasFlash('yaExiste')): ?>
    <?php $this->title = 'Hubo un problema!' ?>

    <p>La plantilla que estas intentando subir ya existe.</p>

    <?= Html::a('Ver plantillas', ['/plantillas'], ['class' => 'btn btn-primary']) ?>
<?php else:?>
    <h1 id="titulo" style="text-align: center;"><?php echo '<b>' . 'CREAR' . '</b>' . 'PLANTILLAS' ?></h1>

    <?= $this->render('_formCreate', [
    'model' => $model,
]) ?>

    </div>
<?php endif; ?>
