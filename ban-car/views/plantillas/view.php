<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\Sectores;
use app\models\Motivo;
use app\models\Usuarios;
/* @var $this yii\web\View */
/* @var $model app\models\plantillas */

$this->title = 'VISUALIZAR';
$this->params['breadcrumbs'][] = ['label' => 'Plantillas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => 'Vista'];
$this->params['breadcrumbs'][] = $model->nombre_archivo;
?>
<link rel="stylesheet" type="text/css" href="../web/css/titulos.css">


<br>
 
<section class="content">

      <div class="row">
        <div class="col-md-12">
          <div class="box box-primary">
            <div class="box-body box-profile">
             <!-- <img class="profile-user-img img-responsive img-circle" src="../../dist/img/user4-128x128.jpg" alt="User profile picture">-->

              <h3 class="profile-username text-center"><?= $model->nombre_archivo ?></h3>

              <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        [
                            'attribute' => 'sector',
                            'label' => 'Sector',                
                            'value' => Html::encode(Sectores::findOne($model->sector)->Sectores) 
                        ],
                        'archivo',
                        [
                           'attribute' => 'motivo_plantilla',
                            'label' => 'Motivo',                
                           'headerOptions' => ['style' => 'text-align: center;'],
                            'value' => Html::encode(Motivo::findOne($model->motivo_plantilla)->motivos) 
                        ],
                        [
                            'attribute' => 'nombre',
                            'value' => function ($model) {
                                if ($model->nombre == 1) {
                                  return 'Habilitado';
                                }else{
                                    return 'Deshabilitado';
                                }
                            },
                        ],
                        [
                            'attribute' => 'cuenta',
                            'value' => function ($model) {
                                if ($model->cuenta == 1) {
                                  return 'Habilitado';
                                }else{
                                    return 'Deshabilitado';
                                }
                            },
                        ],
                        [
                            'attribute' => 'cuil',
                            'value' => function ($model) {
                                if ($model->cuil == 1) {
                                  return 'Habilitado';
                                }else{
                                    return 'Deshabilitado';
                                }
                            },
                        ],
                        [
                            'attribute' => 'sucursal',
                            'value' => function ($model) {
                                if ($model->sucursal == 1) {
                                  return 'Habilitado';
                                }else{
                                    return 'Deshabilitado';
                                }
                            },
                        ],
                        [
                            'attribute' => 'saldo',
                            'value' => function ($model) {
                                if ($model->saldo == 1) {
                                  return 'Habilitado';
                                }else{
                                    return 'Deshabilitado';
                                }
                            },
                        ],
                        [
                            'attribute' => 'saldo_comision',
                            'value' => function ($model) {
                                if ($model->saldo_comision == 1) {
                                  return 'Habilitado';
                                }else{
                                    return 'Deshabilitado';
                                }
                            },
                        ],
                        [
                            'attribute' => 'fecha_emision',
                            'value' => function ($model) {
                                if ($model->fecha_emision == 1) {
                                  return 'Habilitado';
                                }else{
                                    return 'Deshabilitado';
                                }
                            },
                        ],
                        [
                            'attribute' => 'fecha_rechazo',
                            'value' => function ($model) {
                                if ($model->fecha_rechazo == 1) {
                                  return 'Habilitado';
                                }else{
                                    return 'Deshabilitado';
                                }
                            },
                        ],
                        [
                            'attribute' => 'fecha_inhabilitacion',
                            'value' => function ($model) {
                                if ($model->fecha_inhabilitacion == 1) {
                                  return 'Habilitado';
                                }else{
                                    return 'Deshabilitado';
                                }
                            },
                        ],
                        [
                            'attribute' => 'fecha_cierre',
                            'value' => function ($model) {
                                if ($model->fecha_cierre == 1) {
                                  return 'Habilitado';
                                }else{
                                    return 'Deshabilitado';
                                }
                            },
                        ],
                        [
                            'attribute' => 'motivo_cierre',
                            'value' => function ($model) {
                                if ($model->motivo_cierre == 1) {
                                  return 'Habilitado';
                                }else{
                                    return 'Deshabilitado';
                                }
                            },
                        ],
                        [
                            'attribute' => 'cheque_diferido',
                            'value' => function ($model) {
                                if ($model->cheque_diferido == 1) {
                                  return 'Habilitado';
                                }else{
                                    return 'Deshabilitado';
                                }
                            },
                        ],
                        [
                            'attribute' => 'cheque_comun',
                            'value' => function ($model) {
                                if ($model->cheque_comun == 1) {
                                  return 'Habilitado';
                                }else{
                                    return 'Deshabilitado';
                                }
                            },
                        ],
                        [
                            'attribute' => 'documentacion',
                            'value' => function ($model) {
                                if ($model->documentacion == 1) {
                                  return 'Habilitado';
                                }else{
                                    return 'Deshabilitado';
                                }
                            },
                        ],
                        [
                            'attribute' => 'cant_dias',
                            'value' => function ($model) {
                                if ($model->cant_dias == 1) {
                                  return 'Habilitado';
                                }else{
                                    return 'Deshabilitado';
                                }
                            },
                        ],
                        [
                            'attribute' => 'saldo_letras',
                            'value' => function ($model) {
                                if ($model->saldo_letras == 1) {
                                  return 'Habilitado';
                                }else{
                                    return 'Deshabilitado';
                                }
                            },
                        ],  
                        [
                            'attribute' => 'operacion',
                            'value' => function ($model) {
                                if ($model->operacion == 1) {
                                  return 'Habilitado';
                                }else{
                                    return 'Deshabilitado';
                                }
                            },
                        ],
                        [
                            'attribute' => 'dias_letras',
                            'value' => function ($model) {
                                if ($model->dias_letras == 1) {
                                  return 'Habilitado';
                                }else{
                                    return 'Deshabilitado';
                                }
                            },
                        ], 
                        [
                            'attribute' => 'expediente',
                            'value' => function ($model) {
                                if ($model->expediente == 1) {
                                  return 'Habilitado';
                                }else{
                                    return 'Deshabilitado';
                                }
                            },
                        ],
                        [
                            'attribute' => 'caratulado',
                            'value' => function ($model) {
                                if ($model->caratulado == 1) {
                                  return 'Habilitado';
                                }else{
                                    return 'Deshabilitado';
                                }
                            },
                        ],
                        [
                            'attribute' => 'letra',
                            'value' => function ($model) {
                                if ($model->letra == 1) {
                                  return 'Habilitado';
                                }else{
                                    return 'Deshabilitado';
                                }
                            },
                        ],
                        [
                            'attribute' => 'año_deuda',
                            'value' => function ($model) {
                                if ($model->año_deuda == 1) {
                                  return 'Habilitado';
                                }else{
                                    return 'Deshabilitado';
                                }
                            },
                        ],
                        [
                            'attribute' => 'mes_deuda',
                            'value' => function ($model) {
                                if ($model->mes_deuda == 1) {
                                  return 'Habilitado';
                                }else{
                                    return 'Deshabilitado';
                                }
                            },
                        ],
                        [
                            'attribute' => 'cuotas',
                            'value' => function ($model) {
                                if ($model->cuotas == 1) {
                                  return 'Habilitado';
                                }else{
                                    return 'Deshabilitado';
                                }
                            },
                        ],
                        [
                            'attribute' => 'cuotas_vencidas',
                            'value' => function ($model) {
                                if ($model->cuotas_vencidas == 1) {
                                  return 'Habilitado';
                                }else{
                                    return 'Deshabilitado';
                                }
                            },
                        ],
                        [
                            'attribute' => 'fecha_vencimiento',
                            'value' => function ($model) {
                                if ($model->fecha_vencimiento == 1) {
                                  return 'Habilitado';
                                }else{
                                    return 'Deshabilitado';
                                }
                            },
                        ],
                        [
                            'attribute' => 'numero_multa',
                            'value' => function ($model) {
                                if ($model->numero_multa == 1) {
                                  return 'Habilitado';
                                }else{
                                    return 'Deshabilitado';
                                }
                            },
                        ],
                        [
                            'attribute' => 'saldo_garantia',
                            'value' => function ($model) {
                                if ($model->saldo_garantia == 1) {
                                  return 'Habilitado';
                                }else{
                                    return 'Deshabilitado';
                                }
                            },
                        ],
                        [
                            'attribute' => 'saldo_garantia_letras',
                            'value' => function ($model) {
                                if ($model->saldo_garantia_letras == 1) {
                                  return 'Habilitado';
                                }else{
                                    return 'Deshabilitado';
                                }
                            },
                        ],
                        [
                            'attribute' => 'tipo_garantia',
                            'value' => function ($model) {
                                if ($model->tipo_garantia == 1) {
                                  return 'Habilitado';
                                }else{
                                    return 'Deshabilitado';
                                }
                            },
                        ], 
                        [
                            'attribute' => 'motivo_rechazo',
                            'value' => function ($model) {
                                if ($model->motivo_rechazo == 1) {
                                  return 'Habilitado';
                                }else{
                                    return 'Deshabilitado';
                                }
                            },
                        ],
                        [
                            'attribute' => 'saldo_deudor',
                            'value' => function ($model) {
                                if ($model->saldo_deudor == 1) {
                                  return 'Habilitado';
                                }else{
                                    return 'Deshabilitado';
                                }
                            },
                        ],
                        [
                            'attribute' => 'saldo_acreedor',
                            'value' => function ($model) {
                                if ($model->saldo_acreedor == 1) {
                                  return 'Habilitado';
                                }else{
                                    return 'Deshabilitado';
                                }
                            },
                        ],
                        [
                            'attribute' => 'serie',
                            'value' => function ($model) {
                                if ($model->serie == 1) {
                                  return 'Habilitado';
                                }else{
                                    return 'Deshabilitado';
                                }
                            },
                        ],
                        [
                            'attribute' => 'a_favor',
                            'value' => function ($model) {
                                if ($model->a_favor == 1) {
                                  return 'Habilitado';
                                }else{
                                    return 'Deshabilitado';
                                }
                            },
                        ],
                        [
                            'attribute' => 'presentador',
                            'value' => function ($model) {
                                if ($model->presentador == 1) {
                                  return 'Habilitado';
                                }else{
                                    return 'Deshabilitado';
                                }
                            },
                        ],
                        [
                            'attribute' => 'libradores',
                            'value' => function ($model) {
                                if ($model->libradores == 1) {
                                  return 'Habilitado';
                                }else{
                                    return 'Deshabilitado';
                                }
                            },
                        ],
                         [
                            'attribute' => 'motivo',
                            'value' => function ($model) {
                                if ($model->motivo == 1) {
                                  return 'Habilitado';
                                }else{
                                    return 'Deshabilitado';
                                }
                            },
                        ],
                    ],
                ]) ?>
             <?php if (Usuarios::isUserAdmin(Yii::$app->user->identity->getId())) { ?>
              <?= Html::a('Actualizar', ['update', 'id' => $model->id], ['class' => '.btn-sm btn bg-purple color-palette col-lg-6']) ?>  
                <?= Html::a('Borrar', ['delete', 'id' => $model->id], [
                'class' => '.btn-sm btn bg-navy color-palette col-lg-6','data' => [ 
                        'confirm' => 'Esta seguro que desea eliminar esta Plantilla?',
                        'method' => 'post',
                      ],
                ]); 
             }?>
            </div>
          </div>
          <!-- /.box -->

       
      </div>
      <!-- /.row -->

    </section>
    <!-- /.content -->