<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Roles;
use app\models\Sectores;
use yii\helpers\ArrayHelper;
use app\models\Motivo;

/* @var $this yii\web\View */
/* @var $model app\models\plantillas */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="plantillas-form">

    
<?php $form = ActiveForm::begin(); ?>
 <div class="table-bordered table-hover ">  
    <section class="content">
        <div class="row">
    
        <div class="col-md-12">
          <div class="box box-primary">
            <div class="box-body box-profile">
            <div class="col-md-4 col-lg-4">
            <?= $form->field($model, 'sector', ['options' => ['class' => 'no-margin']])
                    ->dropDownList(ArrayHelper::map(Sectores::find()
                    ->all(), 'ID_SECTORES', 'Sectores'),['prompt' => 'Seleccione un sector'])
                ->label('Sector') ?>
            </div>  
             <div class="col-lg-4">
            <?= $form->field($model, 'nombre_archivo')->textInput(['maxlength' => true]) ?>
            </div>

            <div class=" col-lg-4">
            <?= $form->field($model, 'motivo_plantilla', ['options' => ['class' => 'no-margin']])->dropDownList(ArrayHelper::map(Motivo::find()
                    ->all(), 'id', 'motivos'),['prompt' => 'Seleccione un motivo de emision'])
                ->label('Motivo Carta Documento') ?>
            </div> 
            &nbsp; &nbsp;
            <h4 class=" text-center">CAMPOS</h4>
            <div class="form-group">
       
                <div class="custom-control custom-checkbox text-center col-sm-12 " >
                    <label class="col-sm-2">
                    <?= $form->field($model, 'nombre')->checkbox() ?>
                    </label>
                    <label class="col-sm-2">
                    <?= $form->field($model, 'cuenta')->checkbox() ?>
                    </label>
                    <label class="col-sm-2">
                    <?= $form->field($model, 'cuil')->checkbox() ?>
                    </label>
                    <label class="col-sm-2">
                    <?= $form->field($model, 'sucursal')->checkbox() ?> 
                    </label>
                    <label class="col-sm-2">
                    <?= $form->field($model, 'saldo')->checkbox() ?>
                    </label>
                
                    <label class="col-sm-2">
                    <?= $form->field($model, 'entidad')->checkbox() ?>
                    </label>
                </div>
             <div class="col-sm-12">
                <div class="custom-control custom-checkbox text-center" >
                   <label class="col-sm-2">
                    <?= $form->field($model, 'cheque_diferido')->checkbox() ?>
                    </label>
                    <label class="col-sm-2">
                    <?= $form->field($model, 'cheque_comun')->checkbox() ?>
                    </label>
                   <label class="col-sm-2">
                    <?= $form->field($model, 'motivo_cierre')->checkbox() ?>
                    </label>
                    <label class="col-sm-2">
                    <?= $form->field($model, 'documentacion')->checkbox() ?>
                    </label>
                   <label class="col-sm-2">
                    <?= $form->field($model, 'cant_dias')->checkbox() ?>
                    </label>
                    <label class="col-sm-2">
                    <?= $form->field($model, 'a_favor')->checkbox() ?>
                    </label>
                </div>
               </div>
               <div class="col-sm-12">
                <div class="custom-control custom-checkbox text-center" >
                    <label class="col-sm-2">
                    <?= $form->field($model, 'fecha_inhabilitacion')->checkbox() ?>
                    </label>
                    <label class="col-sm-2">
                    <?= $form->field($model, 'fecha_cierre')->checkbox() ?>
                    </label>
                    <label class="col-sm-2">
                    <?= $form->field($model, 'fecha_emision')->checkbox() ?>
                    </label>
                    <label class="col-sm-2">
                    <?= $form->field($model, 'fecha_rechazo')->checkbox() ?>
                    </label>
                    <label class="col-sm-2">
                    <?= $form->field($model, 'fecha_vencimiento')->checkbox() ?>
                    </label>
                     <label class="col-sm-2">
                    <?= $form->field($model, 'libradores')->checkbox() ?>
                    </label>
                    </div>
                </div>
                <div class="col-sm-12">
                <div class="custom-control custom-checkbox text-center" >
                    <label class="col-sm-2">
                    <?= $form->field($model, 'saldo_comision')->checkbox() ?>
                    </label>
                    <label class="col-sm-2">
                    <?= $form->field($model, 'cuotas')->checkbox() ?>
                    </label>
                    <label class="col-sm-2">
                    <?= $form->field($model, 'cuotas_vencidas')->checkbox() ?>
                    </label>
                    <label class="col-sm-2">
                    <?= $form->field($model, 'numero_multa')->checkbox() ?>
                    </label>
                    <label class="col-sm-2">
                    <?= $form->field($model, 'operacion')->checkbox() ?>
                    </label>
                    <label class="col-sm-2">
                    <?= $form->field($model, 'presentador')->checkbox() ?>
                    </label>
                </div>
                </div>
                <div class="col-sm-12">
                <div class="custom-control custom-checkbox text-center" >
                    <label class="col-sm-2">
                    <?= $form->field($model, 'expediente')->checkbox() ?>
                    </label>
                   <label class="col-sm-2">
                    <?= $form->field($model, 'caratulado')->checkbox() ?>
                    </label>
                    <label class="col-sm-2">
                    <?= $form->field($model, 'letra')->checkbox() ?>
                    </label>
                    <label class="col-sm-2">
                    <?= $form->field($model, 'año_deuda')->checkbox() ?>
                    </label>
                    <label class="col-sm-2">
                    <?= $form->field($model, 'mes_deuda')->checkbox() ?>
                    </label>
                     <label class="col-sm-2">
                    <?= $form->field($model, 'motivo',['options'=>['class'=>'custom-control-label']])->checkbox() ?>
                    </label>
                </div>
                </div>
               <div class="col-sm-12">
                <div class="custom-control custom-checkbox text-center" >
                    <label class="col-sm-2">
                    <?= $form->field($model, 'saldo_garantia')->checkbox() ?>
                    </label>
                    <label class="col-sm-2">
                    <?= $form->field($model, 'motivo_rechazo')->checkbox() ?>
                    </label>
                    <label class="col-sm-2">
                    <?= $form->field($model, 'saldo_deudor')->checkbox() ?>
                    </label>
                    <label class="col-sm-2">
                    <?= $form->field($model, 'saldo_acreedor')->checkbox() ?>
                    </label>
                    <label class="col-sm-2">
                    <?= $form->field($model, 'serie')->checkbox() ?>
                    </label>
                    <label class="col-sm-2">
                    <?= $form->field($model, 'tipo_garantia')->checkbox() ?>
                    </label>
                  </div>  
                </div>    
                </div>

                <div class="col-lg-offset-2 col-lg-7">
                   <?= Html::submitButton('Guardar', ['class' => 'btn bg-navy color-palette btn-block', 'name' => 'actualizar-button']) ?>
                </div>
                </div>                    
            </div>
        </div>                    
    </div>
    <?php ActiveForm::end(); ?>
    </div>
    </section>
    </div>
</div>
