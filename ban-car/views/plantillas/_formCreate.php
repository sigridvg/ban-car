<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Roles;
use app\models\Sectores;
use yii\helpers\ArrayHelper;
use app\models\Motivo;

/* @var $this yii\web\View */
/* @var $model app\models\Plantillas */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="plantillas-form">
    <div class="col-md-8 col-lg-12">
        <?php $form = ActiveForm::begin([
            'id' => 'plantillas-form',
            'method' =>'post',
        ]); ?>

        <div class="col-lg-offset-2 col-lg-8">
        <div  class="col-lg-6">
            <?= $form->field($model, 'archivo')->fileInput()?>
        </div>
        <div class="col-lg-6">
            <?= $form->field($model, 'sector', ['options' => ['class' => 'no-margin']])
                    ->dropDownList(ArrayHelper::map(Sectores::find()
                    ->all(), 'ID_SECTORES', 'Sectores'),['prompt' => 'Seleccione un sector'])
                ->label('Sector') ?>
                <?= $form->field($model, 'motivo_plantilla', ['options' => ['class' => 'no-margin']])
                    ->dropDownList(ArrayHelper::map(Motivo::find()
                    ->all(), 'id', 'motivos'),['prompt' => 'Seleccione motivo de plantilla'])
                ->label('Motivo Carta Docuemnto') ?>
            
         <div class="col-lg-offset-2 col-lg-7">
           <?= Html::submitButton('Guardar', ['class' => 'btn bg-navy color-palette btn-block', 'name' => 'actualizar-button']) ?>
        </div>
        <?php ActiveForm::end(); ?>
    </div>

</div>
