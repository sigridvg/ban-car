<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\plantillas */

$this->title = ' ';
$this->params['breadcrumbs'][] = ['label' => 'Plantillas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => 'Actualizar'];
?>
<link rel="stylesheet" type="text/css" href="../web/css/titulos.css">
<div class="plantillas-update">

   <h1 id="titulo" style="text-align: center;"><?php echo '<b>' . 'ACTUALIZAR' . '</b>' . 'PLANTILLAS' ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
