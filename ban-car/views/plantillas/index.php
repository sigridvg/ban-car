<?php
/* @var $this yii\web\View */

use app\models\Usuarios;
use yii\helpers\Html;

use yii\grid\GridView;

$this->title = 'PLANTILLAS';
$this->params['breadcrumbs'][] = ['label' => 'Plantillas', 'url' => ['index']];
?>

   
<link rel="stylesheet" type="text/css" href="../web/css/titulos.css">
<div class="plantillas-index">

    <h1 id="titulo"><?php echo '<b>' . 'PLAN' . '</b>' . 'TILLAS' ?></h1>

    <?php echo $this->render('_search', ['model' => $searchModel]); ?>
    <?php if (Usuarios::isUserAdmin(Yii::$app->user->identity->getId())) { ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'options' => ['style' => 'overflow-x:scroll;width:100%;'],
            'columns' => [
                [
                    'attribute' => 'id',
                    'headerOptions' => ['style' => 'text-align: center;'],
                    'label' => 'ID',
                ],
                'nombre_archivo',
                [
                    'attribute' => 'archivo',
                    'headerOptions' => ['style' => 'text-align: center;'],
                    'label' => 'Plantilla',
                ],
                [
                   'attribute' => 'sector',
                    'label' => 'Sector',                
                   'headerOptions' => ['style' => 'text-align: center;'],
                    'value' => 'sectores.Sectores',
                ],
                [
                   'attribute' => 'motivo_plantilla',
                    'label' => 'Motivo',                
                   'headerOptions' => ['style' => 'text-align: center;'],
                    'value' => 'motivos.motivos',
                ],
                ['class' => 'yii\grid\ActionColumn',
                    'template' => '{delete}{update}{view}',
                ],
            ],
        ]); ?>
    <?php } else { ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'options' => ['style' => 'overflow-x:scroll;width:100%;'],
            'columns' => [
                [
                    'attribute' => 'archivo',
                    'headerOptions' => ['style' => 'text-align: center;'],
                    'label' => 'Plantilla',
                ],
                'nombre_archivo',
                [
                    'attribute' => 'plantillas.Sector',
                    'headerOptions' => ['style' => 'text-align: center;'],
                    'label' => 'Sector',
                    'value' => 'sectores.Sectores',
                ],
                [
                   'attribute' => 'motivo_plantilla',
                    'label' => 'Motivo',                
                   'headerOptions' => ['style' => 'text-align: center;'],
                    'value' => 'motivos.motivos',
                ],
                ['class' => 'yii\grid\ActionColumn',
                    'template' => '{view}',
                ],
            ],
        ]); ?>
    <?php } ?>
