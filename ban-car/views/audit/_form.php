<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Audit */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="audit-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'usuario_legajo_AUD')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'accion_AUD')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'modulo_AUD')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'fecha_hora_AUD')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ip_AUD')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'descripcion_AUD')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
