<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Audit */

$this->title = 'Update Audit: ' . $model->ID_AUD;
$this->params['breadcrumbs'][] = ['label' => 'Audits', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->ID_AUD, 'url' => ['view', 'id' => $model->ID_AUD]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="audit-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
