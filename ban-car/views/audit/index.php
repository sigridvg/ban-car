<?php

use yii\helpers\Html;
//use yii\grid\GridView;
use kartik\export\ExportMenu;
use kartik\grid\GridView;


/* @var $this yii\web\View */
/* @var $searchModel app\models\AuditSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = ' ';
$this->params['breadcrumbs'][] = $this->title;
?>
 <link rel="stylesheet" type="text/css" href="../web/css/titulos.css">
 <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
 
 <div class="audit-index">

    <h1 id="titulo"><?php echo '<b>'.'AUDI'.'</b>'.'TORIA' ?></h1>

    <?php echo $this->render('_search', ['model' => $searchModel]); 
    $gridColumns = [
        ['class' => 'yii\grid\SerialColumn'],
        [
            'value' => 'usuario_legajo_AUD',
            'label' => 'Usuario por Legajo',
            //'value' => ,
        ],
        [
            'value' => 'accion_AUD',
            'label' => 'Actividad',
        ],
        [
            'value' => 'modulo_AUD',
            'label' => 'Modulo',
        ],
        [
            'value' => 'fecha_AUD',
            'label' => 'Fecha',
        ],
        [
            'value' => 'hora_AUD',
            'label' => 'Hora',
        ],
        [
           'value' => 'ip_AUD',
           'label' => 'IP del Usuario',
        ],
        [
            'value' => 'descripcion_AUD',
            'label' => 'Descripcion',
        ],
    ];?>
 
 
 <div class="table-bordered table-hover ">
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-body box-profile">
                            <div class="nav-tabs-custom">
                                <ul class="nav nav-tabs">

                                    <li class="active"><a href="#logins" data-toggle="tab"><i class="fa fa-fw fa-user"></i>LOG DE ACCESOS</a></li>
                                    <li><a href="#actions" data-toggle="tab"><i class="fa fa-fw fa-cog"></i>LOG DE ACCIONES</a></li>
                                </ul>
                                <div class="tab-content">
                                    <!--Admin-->

                                    <div class="tab-pane" id="actions">
                                        
                                      <?php                                         
                                        echo ExportMenu::widget([
                                            'dataProvider' => $dataProvider,
                                            'columns' => $gridColumns,
                                            'dropdownOptions' => [
                                                'label' => 'Exportar Auditoria',
                                                'class' => 'btn btn-outline-secondary btn-default'
                                            ]
                                        ]) . "<hr>\n".
                                        GridView::widget([
                                                'dataProvider' => $dataProvider,
                                                'columns' => $gridColumns,
                                            ]); 
                                        ?>
                                    </div>


                                    <!-- Users -->
                                    <div class="active tab-pane" id="logins">
                                        <?php                                         
                                        echo ExportMenu::widget([
                                            'dataProvider' => $dataProviderLogin,
                                            'columns' => $gridColumns,
                                            'dropdownOptions' => [
                                                'label' => 'Exportar Auditoria',
                                                'class' => 'btn btn-outline-secondary btn-default'
                                            ]
                                        ]) . "<hr>\n".
                                        GridView::widget([
                                                'dataProvider' => $dataProviderLogin,
                                                'columns' => $gridColumns,
                                            ]); 
                                        ?>
                                      </div>

                                </div>
                                <!-- /.tab-content -->
                            </div> 
                        <!-- /.nav tab custom -->
                    </div>
                    <!-- /. box profile -->
                </div>
                <!-- /.box.primary -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>

</div>
 
