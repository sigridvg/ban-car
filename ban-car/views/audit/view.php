<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\Usuarios;

/* @var $this yii\web\View */
/* @var $model app\models\Audit */

$usuario = Usuarios::find()->where(['LegajoUs'=>$model->usuario_legajo_AUD])->one();
$nombre  = $usuario->usernameUs; 

$this->title = '';
$this->params['breadcrumbs'][] = ['label' => 'Auditoria', 'url' => ['index']];
$this->params['breadcrumbs'][] = $nombre;
\yii\web\YiiAsset::register($this);
?>
<link rel="stylesheet" type="text/css" href="../web/css/titulos.css">

<br>
<div class="audit-view">
<section class="content">

      <div class="row">
        <div class="col-md-12">
          <div class="box box-primary">
            <div class="box-body box-profile">


                <p>
                    <h1 class="profile-username text-center"><?php echo '<b>'.'USUARIO: '.'</b>'.$nombre ?></h1>
                </p>


              <ul class="list-group list-group-unbordered">
                
                <li class="list-group-item">
                    <b>ACCION</b><a class="pull-right"><?= Html::encode($model->accion_AUD)?></a>         
                </li>
                <li class="list-group-item">
                    <b>MODULO</b> <a class="pull-right"><?= Html::encode($model->modulo_AUD) ?></a>
                </li> 
                <li class="list-group-item">
                    <b>FECHA/HORA</b><a class="pull-right"><?= Html::encode($model->fecha_AUD).' '. Html::encode(Yii::$app->formatter->asTime($model->hora_AUD))?></a> 
                </li> 
                
                <li class="list-group-item">
                    <b>IP</b><a class="pull-right"><?= Html::encode($model->ip_AUD)?></a> 
                </li>       
                 <li class="list-group-item">
                    <b>DESCRIPCION</b><a class="pull-right"><?= Html::encode($model->descripcion_AUD)?></a> 
                </li>   
              </ul>
            </div>
          </div>
          <!-- /.box -->
      </div>
      <!-- /.row -->
     </div>
  </section>
    <!-- /.content -->
</div>
