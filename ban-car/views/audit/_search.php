<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Audit;

/* @var $this yii\web\View */
/* @var $model app\models\AuditSearch */
/* @var $form yii\widgets\ActiveForm */
?>
<link rel="stylesheet" type="text/css" href="estilo-buscador">

<div class="audit-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => ['class' => 'form-horizontal'],
    ]); ?>
     <div class="form-row align-items-center">
        <div class="col-lg-5">
            <?= $form->field($model,'accion_AUD', [
                    'inputOptions' => [
                     'placeholder' => $model->getAttributeLabel('accion_AUD'),
                     'class' => 'form-control mb-2'],
                     'options' => ['class' => 'no-margin']
                    ])->label(false); ?>
        </div>
        <div class="col-lg-5">
            <?= $form->field($model,'modulo_AUD', [
                    'inputOptions' => [
                     'placeholder' => $model->getAttributeLabel('modulo_AUD'),
                     'class' => 'form-control mb-2'],
                     'options' => ['class' => 'no-margin']
                    ])->label(false); ?>
        </div>
        <div class="col-lg-2">
            <?= Html::submitButton('Buscar', ['class' => 'btn bg-navy-active']) ?>
            <?= Html::a('Borrar', ['index'], ['class' => 'btn bg-purple color-palette'])  ?>
        </div>
    </div>
   <?php ActiveForm::end(); ?>

</div>
