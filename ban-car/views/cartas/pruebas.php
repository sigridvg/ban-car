<?php



    public function actionDestinatariosList(){
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $parents = $_POST['depdrop_parents'];
            if ($parents != null) {
                $cat_id = $parents[0];
                $param1 = null;
                
                if (!empty($_POST['depdrop_params'])) {
                    $params = $_POST['depdrop_params'];
                    $param1 = $params[0]; // get the value of input-type-1    
                }
     
                $out = self::getCuentas($params, $param1); 
      
            
                $selected = 0;
          
            return ['output' => $out, 'selected' => $selected];
        }
    }
    return ['output' => 'No entro', 'selected' => 'al if'];

}


 public function getCuentas($q, $a){
         \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
         $out = [];
       
        if (!is_null($q)) {
            $datos = "http://localhost:8080/rest-api/rest/index/users?dni=".$q;
            $data = json_decode(file_get_contents($datos), true);

            foreach ($data as $key => $value) {
                $nombre = json_encode($data[$key]['nombre']);
                $campos = ['id' => $q, 'text' => $nombre, 'nombre'=>$data[$key]['nombre'], 'numero_documento' => $data[$key]['numero_documento'], 'direccion' => $data[$key]['direccion'], 'pais' => $data[$key]['pais_descripcion'], 'provincia' => $data[$key]['provincia_nombre'],'localidad' => $data[$key]['localidad_nombre']];
                array_push($out, $campos);
            }

        }
            
         return $out;

}

         public function actionCuentasList($q){
                 \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                 $out = [];
               
                if (!is_null($q)) {
                    $datos = "http://localhost:8080/rest-api/rest/index/users?dni=".$q;
                    $data = json_decode(file_get_contents($datos), true);

                    foreach ($data as $key => $value) {
                        $nombre = json_encode($data[$key]['nombre']);
                        $campos = ['id' => $q, 'text' => $nombre, 'nombre'=>$data[$key]['nombre'], 
                            'numero_documento' => $data[$key]['numero_documento'], 'direccion' => $data[$key]['direccion'], 
                            'pais' => $data[$key]['pais_descripcion'], 'provincia' => $data[$key]['provincia_nombre'],
                            'localidad' => $data[$key]['localidad_nombre']];
                        array_push($out, $campos);
                    }

                }
                    
                 return $out;

        }


public function actionPersonaList() {
    $out = [];
    $dato = Yii::$app->request->post('dni');
    if (isset($_POST['depdrop_parents'])) {
        $parents = $_POST['depdrop_parents'];
        if ($parents != null) {
            $id = $parents[0];
            $out = self::getDestinatarios($id); 
            
            echo Json::encode(['output'=>$out, 'selected'=>'']);
            return json_encode($out);
        }
    }
   echo Json::encode(['output'=>'', 'selected'=>'Aqui', 'dato'=>$dato]);
}

// Generate list of products based on cat and subcat
/*public function actionCuentaList() {
    $out = [];
    if (isset($_POST['depdrop_parents'])) {
        $ids = $_POST['depdrop_parents'];
        $id = empty($ids[0]) ? null : $ids[0];
        $nombre = empty($ids[1]) ? null : $ids[1];
        if ($id != null) {
           $data = self::getCuentas($id, $nombre);
            
             * the getProdList function will query the database based on the
             * cat_id and sub_cat_id and return an array like below:
             *  [
             *      'out'=>[
             *          ['id'=>'<prod-id-1>', 'name'=>'<prod-name1>'],
             *          ['id'=>'<prod_id_2>', 'name'=>'<prod-name2>']
             *       ],
             *       'selected'=>'<prod-id-1>'
             *  ]
             
           
           echo Json::encode(['output'=>$out, 'selected'=>'']);
           return;
        }
    }
    echo Json::encode(['output'=>'', 'selected'=>'']);
}*/

   
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */



 /*'pluginEvents' => [
                                // "select2:selecting"=> "$('#nombre').on('select2:selecting', function (e) {
                                //         var data = e.params.data;
                                //         alert(data);
                                //     })",
                                "select2:change"=>"function(e) { 
                                    var value = this.value;
                                    $('#input-type-1').val(this.value); 
                                    alert(value) }"
                           ],*/

$formatJs = <<< JS
var formatDest = function(dest) {
    if(dest.loading) {
        return dest.text;
    }
var markup =
 '<div class="row">' + 
     '<div class="col-sm-4">' +
        '<b style="margin-left:5px">' + dest.nombre + '</b>' + 
     '</div>' +
     '<div class="col-sm-2"> DNI: ' + dest.dni + '</div>' +
      '<div class="col-sm-2">Pais: ' + dest.pais + '</div>' +
     '<div class="col-sm-2"> Provincia: ' + dest.provincia + '</div>' +    
     '<div class="col-sm-2"> Localidad: ' + dest.localidad + '</div>' +
    
'</div>'
     if (dest.direccion) {
       markup += '<h5>'+' Direccion: '+ dest.direccion + '</h5>';
     }
     return '<div style="overflow:hidden;">' + markup + '</div>';
 };
var formatDestSelection = function(dest) {
    return dest.nombre || dest.text;
}
JS;

$this->registerJs($formatJs, View::POS_HEAD);

$resultsJs = <<< JS
 function (data, params) {
    params.page = params.page || 1;
    var result = $.map(data, function(item) { 
        return { 
            id: item.numero_documento, 
            text: item.nombre,
            dni: item.numero_documento,
            nombre: item.nombre,
            cuenta: item.cuenta,
            direccion: item.direccion,
            bloqueo: item.bloqueo,
            tipcuenta: item.tipocuenta,
            pais: item.pais,
            provincia: item.provincia,
            localidad: item.localidad
            }
        });
        
        return {
            results: result.item,
            pagination: {
            more: (params.page * 30) < data.total_count
        }
    };
}
JS;

echo  Select2::widget([
                        'name' => 'nombre',
                        'pluginOptions' => [
                            'allowClear' => true,
                            'minimumInputLength' => 7,
                            'ajax' => [
                                'url' => $url,
                                'type'=> 'GET',
                                'dataType' => 'json',
                                
                                'data' =>  new JsExpression('function(params) { return {q:params.term, page: params.page}; }'),
                                'processResults' => new JsExpression($resultsJs),
                                'cache' => true
                            ],
                            'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                            'templateResult' => new JsExpression('formatDest'),
                            'templateSelection' => new JsExpression('  function(dest) {
    return dest.nombre || ;
}'),
                        ],
                        'pluginEvents' => [
                                 "select2:selecting"=> "$('#nombre').on('select2:selecting', function (e) {
                                         var data = e.params.data;
                                         alert(data);
                                     })",
                                "select2:change"=>"function(e) { 
                                    var value = this.value;
                                    $('#input-type-1').val(this.value); 
                                    alert(value) }"
                        ],
                        'options' => [
                            'placeholder' => 'Ingrese DNI',
                            'multiple' => false,
                        ]
                    ]);
                        
                           echo Select2::widget([
                               'name' => 'choice-field-value',
                               'data' => ['Prueba', 'Prueba2'],  
                               'options' => ['placeholder' => 'Ingrese DNI ...'], 
                               'pluginOptions' => ['allowClear' => true]]);
                       // Additional input fields passed as params to the child dropdown's pluginOptions
                       // echo Html::hiddenInput('input-type-1', 'Additional value 1', ['id' => 'input-type-1']);                         

                    ?>
 \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = [];
        if (!is_null($q)) {
            $datos = "http://localhost:8080/rest-api/rest/index/users?dni=".$q;
            $data = json_decode(file_get_contents($datos), true);
            foreach ($data as $key=>$value) {
                $nombre = json_encode($data[$key]['nombre']);
                $campos = ['id' => $q, 'text' => $nombre, 'nombre'=>$data[$key]['nombre'], 
                    'numero_documento' => $data[$key]['numero_documento'], 'direccion' => $data[$key]['direccion'], 
                    'pais' => $data[$key]['pais_descripcion'], 'provincia' => $data[$key]['provincia_nombre'],
                    'localidad' => $data[$key]['localidad_nombre']];
                array_push($out, $campos);
              }
        }
        return $out;
        
        
          //echo Html::input('number', 'dni', '', ['class' => 'form-control', 'id' => 'dni', 'onchange'=>'myFunction()']);
                     echo $form->field($modelDest, 'DNI')->widget(Select2::classname(), [
                        'name' => 'dni',
                         'data' =>$data,
                        'options' => ['multiple'=>true, 'placeholder' => 'Ingresar DNI ...'],
                        'pluginOptions' => [
                            'allowClear' => true,
                            'minimumInputLength' => 7,
                            'language' => [
                                'errorLoading' => new JsExpression("function () { return 'Waiting for results...'; }"),
                            ],
                            'ajax' => [
                                'url' => $url,
                                'dataType' => 'json',
                                'data' =>  new JsExpression('function(params) { return {q:params.term, page: params.page}; }'),
                            ],
                            'escapeMarkup' => new JsExpression('function(markup) { return markup; }'),
                            'templateResult' => new JsExpression('formatDest'),
                            'templateSelection' => new JsExpression('function(dest) { return dest.text; }'),
                        ],
                    ]); 
                    
                     echo $form->field($modelDest, 'DNI')->widget(Select2::classname(), [
                        'options' => ['placeholder' => 'Ingrese DNI ...'],
                        'pluginOptions' => [
                            'allowClear' => true,
                            'minimumInputLength' => 7,
                            'ajax' => [
                                'url' => $url,
                                'dataType' => 'json',
                                'data' =>  new JsExpression('function(params) { return {q:params.term, page: params.page}; }'),
                                'processResults' => new JsExpression($resultsJs),
                                'cache' => true
                            ],
                            'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                            'templateResult' => new JsExpression('formatDest'),
                            'templateSelection' => new JsExpression('function(dest) {return dest.text;}'),
                        ],
                    ]);
                    
                    
                     echo $form->field($modelDest, 'Cuenta')->widget(DepDrop::classname(), [
                        'options' => ['placeholder' => 'Select ...'],
                        'type' => DepDrop::TYPE_SELECT2,
                        'select2Options' => ['pluginOptions' => ['allowClear' => true]],
                        'pluginOptions' => [
                            'depends' => ['modelDest-DNI'],
                            'url' => Url::to(['child-account']),
                            'loadingText' => 'Loading child level 1 ...',
                        ]
                    ]);
                    
                    
                    <?php 
                   echo $form->field($modelDest, 'DNI')->widget(Select2::classname(), [
                     'options' => ['placeholder' => 'Ingrese DNI ...'],
                        'pluginOptions' => [
                            'allowClear' => true,
                            'minimumInputLength' => 7,
                            'ajax' => [
                                'url' => $url,
                                'dataType' => 'json',
                                'data' =>  new JsExpression('function(params) { return {q:params.term, page: params.page}; }'),
                                'processResults' => new JsExpression($resultsJs),
                                'cache' => true
                            ],
                            'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                            'templateResult' => new JsExpression('formatDest'),
                            'templateSelection' => new JsExpression('formatDestSelection'),
                        ],
                    ]);
                   
                   ?>

                    
     public function actionDestinatariosList($q){
       \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = []; 
        
        $destinatario = Destinatarios::find()->andWhere(['DNI' => $q])->count();
        
        if ($destinatario>0) {
            //Busca
           $destinatario = Destinatarios::find()->where("DNI=:dni", [":dni" => $q])->one();
           $id = json_encode($destinatario['DNI']); 
           $text = json_encode($destinatario['Nombre_RazonSocial']);

            $out = ['id' => $q, 'text' => $text,'nombre'=>$destinatario['Nombre_RazonSocial'], 
                    'numero_documento' => $destinatario['DNI'], 'direccion' => $destinatario['Direccion'],
                    'pais' => $destinatario['Codigo_pais'], 'provincia' => $destinatario['ID_Provincia']];
                                   
        }elseif($destinatario==0){
            //Carga en la BD
            RegistroDestinatarios::registrarDestinatarios($q);
            $destinatario = Destinatarios::find()->where("DNI=:dni", [":dni" => $q])->one();
            $nombre = json_encode($destinatario['Nombre_RazonSocial']);
            $out = ['id' => $q, 'text' => $nombre,'nombre'=>Destinatarios::find($q)->Nombre_RazonSocial, 
                    'numero_documento' => Destinatarios::find($q)->DNI, 'direccion' => Destinatarios::find($q)->Direccion,
                    'pais' => Destinatarios::find($q)->Codigo_pais, 'provincia' => Destinatarios::find($q)->ID_Provincia,
                    'localidad' => Destinatarios::find($q)->ID_Localidad];
        };
                
        return $out;

    }
    
    public function actionChildAccount() {
    Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
    $out = [];
    if (isset($_POST['depdrop_parents'])) {
        $id = end($_POST['depdrop_parents']);
        $list = Destinatarios::find()->andWhere(['ID_DES' => $id])->asArray()->all();
        $selected  = null;
        if ($id != null && count($list) > 0) {
            $selected = '';
            foreach ($list as $i => $account) {
                $out[] = ['id' => $account['id'], 'name' => $account['name']];
                if ($i == 0) {
                    $selected = $account['id'];
                }
            }
            // Shows how you can preselect a value
            return ['output' => $out, 'selected' => $selected];
        }
    }
        return ['output' => '', 'selected' => ''];
    }
    
    
    $resultsJs = <<< JS
 function (data, params) {
    params.page = params.page || 1;
    var result = $.map(data, function(item) { 
        return { 
            id: item.numero_documento, 
            text: item.nombre,
            cuenta: item.cuenta
            }
        });
        
        return {
            results: result.item,
            pagination: {
            more: (params.page * 30) < data.total_count
        }
    };
}
JS;


var checkActive = false;
    $('#firmante').prop('disabled', true);

    $(document).ready(function() {

        $('#chec').change(function() {

            if(checkActive === true) {

                $('#firmante').prop('disabled', true);
                checkActive = false;

            } else {

                 $('#firmante').prop('disabled', false);
                 checkActive = true;

            }

        });

    });
    
    var markup ={
'<div class="row">' + 
    '<div class="col-sm-5">' +
        '<b style="margin-left:5px">' + dest.text + '</b>' + 
    '</div>' +
    '<div class="col-sm-3"><i class="fa fa-code-fork"></i> ' + dest.cuenta + '</div>' +
'</div>';
    return '<div style="overflow:hidden;">' + markup + '</div>';
};

'pluginEvents'=>["select2:select" => "function() { var variableJS = $(this).val();
                                                                                alert (variableJS);}",]    
                                                                                
                                                                                
                                                                                public function actionDestinatariosCuentas() {
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            $out = [];
            
            if (isset($_POST['depdrop_parents'])) {
                $parents = $_POST['depdrop_parents'];
                if ($parents != null) {
                    $id = $parents[0];
                    $cuil = null;
                    if (!empty($_POST['depdrop_params'])) {
                        $params = $_POST['depdrop_params'];
                        $cuil = $params[0]])->one();
                    }
                    //$selected = self::getCuenta($cat_id);
                    $out = self::getCuentas($id,$cuil);

                    return ['output' => $out, 'selected' => ''];
                }
                return ['output' => 'capaz', 'selected' => 'sale'];
            }
           return ['output' => $out, 'selected' => ''];
        }
        
        
        
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            $out = [];
            if (isset($_POST['depdrop_parents'])) {
                $parents = $_POST['depdrop_parents'];
                if ($parents != null) {
                    Yii::$app->response->format = Response::FORMAT_JSON;
                    $out = self::getCuenta($parents[0]);

                    return ['output' => $out, 'selected' => ' '];
                }
            }
        }
        
        
        echo $form->field($model, 'nro_cuenta')->widget(DepDrop::classname(), [
                        'options' => ['placeholder' => 'Seleccione cuenta ...'],
                        'type' => DepDrop::TYPE_SELECT2,
                        'select2Options' => ['pluginOptions' => ['allowClear' => true]],
                        'pluginOptions' => [
                            'depends' => ['cuil_destinatario'],
                            'url' => Url::to(['/cartas/destinatarios-cuentas']),
                            'loadingText' => 'Loading...'
                        ]
                    ]);
                    
                    
                    $templateWord->setValue('saldo', $model->saldo);
        $templateWord->setValue('saldo_letras', strtolower(NumerosEnLetras::convertir(model->saldo, 'pesos', false, 'centavos')));
        
        
        </script>

        
        <script  type="text/javascript">
    $(document).ready(function (item_id, checked) {
        var pick_id = getUrlVars()['id'];
        alert(checked);
        if(checked){
            console.log('Entre aqui');
            $.ajax({
            url: <?php Url::to(['firma-multiple']); ?>,
            method: 'get',
            dataType: 'text',
            data: {r:'cartascontroller/firma-multiple', item:item_id, pick:pick_id}
            }).done(function(){alert('added')}).error(function(){alert('there was a problem...!')});
        }
    };
</script>