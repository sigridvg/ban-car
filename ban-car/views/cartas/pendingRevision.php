<?php

use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CartasSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'REVISION';
$this->params['breadcrumbs'][] = ['label' => 'Cartas', 'url' => ['index']];
?>

<link rel="stylesheet" type="text/css" href="../web/css/titulos.css">
<div class="cartas-pendientes-revision">

    <h1 id="titulo" style="text-align: center;"><?php echo '<b>' . 'CARTAS PENDIENTES' . '</b>' . ' DE REVISION' ?></h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'options' => ['style' => 'overflow-x:scroll;width:100%;'],
        'columns' => [
            [
                'attribute' => 'id_OCA',
                'headerOptions' => ['style' => 'text-align: center;'],
                'label' => 'ID OCA',
            ],
            [
                'attribute' => 'emisor',
                'headerOptions' => ['style' => 'text-align: center;'],
                'label' => 'Emisor',
                'value' => 'usuarios.FullName'
            ],
            [
                'attribute' => 'destinatario',
                'headerOptions' => ['style' => 'text-align: center;justify-content:center;'],
                'label' => 'Destinatario',
                'value' => 'destinatarios.Nombre_RazonSocial'
            ],
            [
                'attribute' => 'id_plantilla',
                'headerOptions' => ['style' => 'text-align: center;width:35%'],
                'label' => 'Plantilla',
                'value' => 'plantillas.name'
            ],
            [
                'attribute' => 'status',
                'label' => 'Estado',
                'value' => 'StatusDescription'
            ],
            [
                'attribute' => 'fecha',
                'headerOptions' => ['style' => 'text-align: center;'],
                'label' => 'Fecha',
            ],
            [
                'attribute' => 'id_sucursal',
                'headerOptions' => ['style' => 'text-align: center;'],
                'label' => 'Sucursal',
                'value' => 'sucursales.nombre'

            ],
            [
                'attribute' => 'id_firmante',
                'headerOptions' => ['style' => 'text-align: center;'],
                'label' => 'Firmante',
                'value' => 'usuarios.FullName',
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update}{view}',
                'buttons' => [
                    'view' => function ($url) {
                        return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url,
                        [
                            'title' => Yii::t('app', 'Visualizar'),
                        ]);
                    },
                    'update'=> function ($url) {
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url,
                        [
                            'title' => Yii::t('app', 'Editar'),
                        ]);
                    },
                ],
                'urlCreator' => function ($action, $model) {
                    if ($action === 'view') {
                        return Url::to([$action, 'id' => $model->id]);
                    }elseif ($action === 'update') {
                        return Url::to([$action, 'id' => $model->id]);
                    }
                },
            ],
        ],
    ]); ?>


</div>
