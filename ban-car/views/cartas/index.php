<?php

use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CartasSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'CARTAS DOCUMENTO';
$this->params['breadcrumbs'][] = ['label' => 'Cartas', 'url' => ['index']];
?>

<link rel="stylesheet" type="text/css" href="../web/css/titulos.css">
<div class="cartas-index">

    <h1 id="titulo"><?php echo '<b>' . 'CARTAS' . '</b>' . 'DOCUMENTO' ?></h1>

    <?php echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'options' => ['style' => 'overflow-x:scroll;width:100%;'],
        'columns' => [

            [
                'attribute' => 'id_OCA',
                'headerOptions' => ['style' => 'text-align: center;'],
                'label' => 'ID OCA',
            ],
            [
                'attribute' => 'id_usuario',
                'headerOptions' => ['style' => 'text-align: center;'],
                'label' => 'Emisor',
                'value' => 'usuarios.FullName'
            ],
            [
                'attribute' => 'destinatario',
                'headerOptions' => ['style' => 'text-align: center;justify-content:center;'],
                'label' => 'Destinatario',
                'value' => 'destinatarios.Nombre_RazonSocial'
            ],
            [
                'attribute' => 'id_plantilla',
                'headerOptions' => ['style' => 'text-align: center;width:35%'],
                'label' => 'Plantilla',
                'value' => 'plantillas.name'
            ],
            [
                'attribute' => 'status',
                'label' => 'Estado',
                'value' => 'StatusDescription'
            ],
            [
                'attribute' => 'fecha',
                'headerOptions' => ['style' => 'text-align: center;'],
                'label' => 'Fecha',
            ],
            [
                'attribute' => 'id_sucursal',
                'headerOptions' => ['style' => 'text-align: center;'],
                'label' => 'Sucursal',
                'value' => 'sucursales.nombre'
            ],
            [
                'attribute' => 'id_firmante',
                'headerOptions' => ['style' => 'text-align: center;'],
                'label' => 'Firmante',
                'value' => 'firmante.FullName',
            ],
        ],
    ]); ?>

</div>
