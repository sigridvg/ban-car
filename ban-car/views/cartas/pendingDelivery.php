<?php

use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use app\models\DespachoSearch;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CartasSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'DESPACHO';
$this->params['breadcrumbs'][] = ['label' => 'Cartas', 'url' => ['index']];
?>

<link rel="stylesheet" type="text/css" href="../web/css/titulos.css">
<div class="cartas-pendientes-despacho">

    <h1 id="titulo" style="text-align: center;"><?php echo '<b>' . 'CARTAS PENDIENTES' . '</b>' . ' DE DESPACHO' ?></h1>

    <?php echo $this->render('despacho_search', ['model' => $searchModel]); ?>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'options' => ['style' => 'overflow-x:scroll;width:100%;'],
        'columns' => [
            [
                'attribute' => 'id_OCA',
                'headerOptions' => ['style' => 'text-align: center;'],
                'label' => 'ID OCA',
            ],
            [
                'attribute' => 'id_usuario',
                'headerOptions' => ['style' => 'text-align: center;'],
                'label' => 'Emisor',
                'value' => 'usuarios.FullName'
            ],
            [
                'attribute' => 'id_destinatario',
                'headerOptions' => ['style' => 'text-align: center;justify-content:center;'],
                'label' => 'Destinatario',
                'value' => 'destinatarios.Nombre_RazonSocial'
            ],
            [
                'attribute' => 'id_plantilla',
                'headerOptions' => ['style' => 'text-align: center;width:35%'],
                'label' => 'Plantilla',
                'value' => 'plantillas.name'
            ],
            [
                'attribute' => 'status',
                'label' => 'Estado',
                'value' => 'StatusDescription'
            ],
            [
                'attribute' => 'fecha',
                'headerOptions' => ['style' => 'text-align: center;'],
                'label' => 'Fecha',
            ],
            [
                'attribute' => 'id_sucursal',
                'headerOptions' => ['style' => 'text-align: center;'],
                'label' => 'Sucursal',
                'value' => 'sucursales.nombre'

            ],
            [
                'attribute' => 'id_firmante',
                'headerOptions' => ['style' => 'text-align: center;'],
                'label' => 'Firmante',
                'value' => 'firmante.FullName',
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{descargar} {imprimir} {view}',
                'buttons' => [
                    'descargar' => function ($url) {
                        return Html::a('<span class="glyphicon glyphicon-download"></span>', $url,
                            [
                                'title' => Yii::t('app', 'Descargar'),
                                'data-confirm' => "¿Desea descargar ésta carta?",
                            ]);
                    },
                    'view' => function ($url) {
                        return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url,
                            [
                                'title' => Yii::t('app', 'Visualizar'),
                                
                            ]);
                    },
                    'imprimir' => function($url, $model){
                        
                        return Html::a('<span class="glyphicon glyphicon-print"></span>', 
                                Url::to(['cartas/imprimir', 'id' => $model->id]),
                                ['target'=>'_blank']);
                    }
                ],
                'urlCreator' => function ($action, $model) {
                    if ($action === 'descargar') {
                        return Url::to([$action, 'id' => $model->id]);
                    } elseif ($action == 'view') {
                      return Url::to([$action, 'id' => $model->id]);
                    }
                }
            ],
        ],
    ]); ?>


</div>
