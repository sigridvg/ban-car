<?php

use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CartasSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'AUTORIZACION';
$this->params['breadcrumbs'][] = ['label' => 'Cartas', 'url' => ['index']];
?>

<link rel="stylesheet" type="text/css" href="../web/css/titulos.css">
<div class="cartas-pendientes">

    
 <?php if (Yii::$app->session->hasFlash('statusUpdated')) { ?>
        <div class="alert alert-success">
            <h6><?= Yii::$app->session->getFlash('statusUpdated') ?></h6>
        </div>  
<?php } ?>
    <h1 id="titulo"><?php echo '<b>' . 'CARTAS PENDIENTES' . '</b>' . ' DE AUTORIZACION' ?></h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'options' => ['style' => 'overflow-x:scroll;width:100%;'],
        'columns' => [
            [
                'class' => 'yii\grid\CheckboxColumn', 
                'checkboxOptions' => 
                [
                    'onclick' => 'js:signCartas(this.id)'
                ],
            ],         
            [
                'attribute' => 'id_OCA',
                'headerOptions' => ['style' => 'text-align: center;'],
                'label' => 'ID OCA',
            ],
            [
                'attribute' => 'emisor',
                'headerOptions' => ['style' => 'text-align: center;'],
                'label' => 'Emisor',
                'value' => 'usuarios.FullName'
            ],
            [
                'attribute' => 'destinatario',
                'headerOptions' => ['style' => 'text-align: center;justify-content:center;'],
                'label' => 'Destinatario',
                'value' => 'destinatarios.Nombre_RazonSocial'
            ],
            [
                'attribute' => 'id_plantilla',
                'headerOptions' => ['style' => 'text-align: center;width:35%'],
                'label' => 'Plantilla',
                'value' => 'plantillas.name'
            ],
            [
                'attribute' => 'status',
                'label' => 'Estado',
                'value' => 'StatusDescription'
            ],
            [
                'attribute' => 'fecha',
                'headerOptions' => ['style' => 'text-align: center;'],
                'label' => 'Fecha',
            ],
            [
                'attribute' => 'id_sucursal',
                'headerOptions' => ['style' => 'text-align: center;'],
                'label' => 'Sucursal',
                'value' => 'sucursales.nombre'

            ],
            [
                'attribute' => 'id_firmante',
                'headerOptions' => ['style' => 'text-align: center;'],
                'label' => 'Firmante',
                'value' => 'firmante.FullName',
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{firmar} {revisar} {view}',
                'buttons' => [
                    'firmar' => function ($url) {
                        return Html::a('<span class="glyphicon glyphicon-ok-circle"></span>', $url,
                            [
                                'title' => Yii::t('app', 'Firmar carta'),
                                'data-confirm' => "¿Desea firmar ésta carta?",
                            ]);
                    },
                    'view' => function ($url) {
                        return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url,
                            [
                                'title' => Yii::t('app', 'Visualizar'),
                            ]);
                    },
                    'revisar' => function ($url) {
                        return Html::a('<span class="glyphicon glyphicon-exclamation-sign"></span>', $url,
                            [
                                'title' => Yii::t('app', 'Solicitar cambios'),
                                'data-confirm' => "¿Desea solicitar cambios sobre ésta carta?",
                            ]);
                    },
                ],
                'urlCreator' => function ($action, $model) {
                    if ($action === 'firmar') {
                        return Url::to([$action, 'id' => $model->id]);
                    } else if ($action === 'revisar') {
                        return Url::to([$action, 'id' => $model->id]);
                    }
                    return Url::to([$action, 'id' => $model->id]);
                },
            ],
        ],
    ]); 
    echo Html::a('<span class="btn bg-navy color-palette "> FIRMAR SELECCIONADAS </span>', Url::to(['firma-multiple']),
        [
            'data-confirm' => "¿Desea firmar las cartas seleccionadas?", 
        ]);
?>
</div>
<script type="text/javascript" ></script>
<script>
        function signCartas(id_carta){
            alert(checked);
            $.ajax({
                url: <?php Url::to(['firma-multiple']); ?>,
                type: 'post',
                
                contentType: 'application/json;charset=utf-8',
                dataType: 'json',
                data: {id:id_carta},
                success: function () {
                    alert("Data inserted successfully");
                },

                error: function () {
                    alert("insert errorr");
                }
            });
        } 
</script>


