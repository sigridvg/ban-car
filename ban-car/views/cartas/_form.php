<?php

/* @var $this yii\web\View */
/* @var $form \yii\widgets\ActiveForm */

//use app\models\Destinatarios;
use app\models\Plantillas;
use app\models\Sucursales;
use app\models\Usuarios;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\jui\DatePicker;
use kartik\depdrop\DepDrop;
use yii\helpers\Html;
use yii\helpers\Url;
use kartik\select2\Select2;
use yii\web\JsExpression;
use app\models\Destinatarios;
use yii\web\View;

$Plantilla= Plantillas::find('id')->where('id=:id', [':id' => $model->id_plantilla])->one();
$motivo=$Plantilla->motivo_plantilla;

$url = Url::to(['destinatarios-list']);
$dataList = Destinatarios::find()->andWhere(['Cuit_Cuil' => $modelDest->Cuit_Cuil])->all();
$data = ArrayHelper::map($dataList, 'ID_DES', 'Nombre_RazonSocial', 'Cuenta');

?>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
<?php
$formatJs = <<< 'JS'
var formatRepo = function (repo) {
    if (repo.loading) {
        return repo.text;
    }
    var markup =
'<div class="row">' + 
    '<div class="col-sm-5">' +
        '<b style="margin-left:5px">' + dest.text + '</b>' + 
    '</div>' +
    '<div class="col-sm-3"><i class="fa fa-code-fork"></i> ' + dest.cuenta + '</div>' +
'</div>';
    return '<div style="overflow:hidden;">' + markup + '</div>';
};
var formatRepoSelection = function (repo) {
    return repo.nombre || repo.text;
}
JS;
$this->registerJs($formatJs, View::POS_HEAD);
 
// script to parse the results into the format expected by Select2

?>


<div class="cartas-create">
   
    <div class="col-md-auto col-lg-auto">
        <?php $form = ActiveForm::begin([
            'id' => 'carta-documento-form',
            'method' => 'post',
        ]); ?>
    <div class="table-bordered table-hover ">  
        <section class="content">
            <div class="row">
                <div class="col-auto col-md-12">
                    <h4><?= html::label('Plantilla: '.$Plantilla->nombre_archivo) ?></h4>
                    <?= $form->field($model, 'id_plantilla')->hiddenInput(['value'=> $Plantilla->id])->label(false);?>
                </div>
                <div class="align-items-center">
                    <div class="form-group">                
                         <div class="col-md-6">
                                <?= $form->field($model, 'fecha', ['options' => ['class' => 'no-margin']])->widget(DatePicker::className(), [
                                    'language' => 'es',
                                    'dateFormat' => 'yyyy-MM-dd',
                                    'options' => [ 'class' => 'form-control'],
                                ])->label('Fecha') ?>
                            </div>
                        <div class="col-auto col-md-6">
                            <?= $form->field($model, 'id_sucursal', ['options' => ['class' => 'no-margin']])
                                ->dropDownList(ArrayHelper::map(Sucursales::find()
                                    ->all(), 'id', 'nombre'), ['prompt' => 'Seleccione una sucursal']); ?>
                        </div> 
                    </div>
                    <div class="form-group">
                        <div class="col-auto col-md-6 form-group" > 
                             <?= $form->field($model, 'id_firmante', ['options' => ['class' => 'no-margin']])->dropDownList(
                                ArrayHelper::map(Usuarios::find()
                                    ->where(['IdRoles' => Usuarios::ROLE_FIRMANTE])
                                    ->andWhere(['IdSectores' => Yii::$app->user->identity->getIdSectores()])
                                    ->all(), 'IdUsuarios', 'fullName'), ['prompt' => 'Seleccione un firmante','id'=>'firmante'] );
                             ?>
                        </div> 
                        <div class="col-auto col-md-6 form-group" > 
                              <?= $form->field($model, 'documentacion', ['options' => ['class' => 'no-margin']])->textArea([]) ?>
                        </div> 
                    </div>
                    <div class="form-group">
                        <div class="col-md-6">
                            <?php    
                            echo $form->field($model, 'destinatario')->widget(Select2::classname(), [
                                    'data' => $data,
                                    'options' => ['multiple'=>false, 'placeholder' => 'Ingrese Cuil...', 'id'=> 'cuil_destinatario'],
                                    'pluginOptions' => [
                                        'allowClear' => true,
                                        'minimumInputLength' => 10,
                                        'language' => [
                                            'errorLoading' => new JsExpression("function () { return 'Waiting for results...'; }"),
                                        ],
                                        'ajax' => [
                                            'url' => $url,
                                            'dataType' => 'json',
                                            'data' => new JsExpression('function(params) { return {q:params.term}; }')
                                        ],
                                        'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                                        'templateResult' => new JsExpression('function(dest) { return dest.text + " Cuil: " + dest.id +" DNI: "+dest.dni; }'),
                                        'templateSelection' => new JsExpression('function (dest) { return dest.text; }'),
                                    ],
                                ]);
                            ?>

                        </div>  

                        <div class="col-md-6">
                            <?php
                            echo $form->field($model, 'nro_cuenta')->widget(DepDrop::className(),
                            [
                                'type' => DepDrop::TYPE_SELECT2,
                                'options' => ['placeholder' => 'Select ...'],
                                'select2Options' => ['pluginOptions' => ['allowClear' => true]],
                                'pluginOptions' => [
                                    'placeholder' => 'Seleccione una cuenta...',
                                    'depends' => ['cuil_destinatario'],
                                    'url' => Url::to(['/cartas/destinatarios-cuentas'])
                                ]
                            ])->label('Cuenta'); 
                            ?>
                        </div>
                    </div>

        <?php 
             if (isset($campos)) {
                $i = 1; 
                foreach ($campos as $key => $value):
                    if ($value==1 and $i<=count($campos)){
                        switch ($key) {
                            case 'fecha_inhabilitacion': case 'fecha_emision': case 'fecha_rechazo': case 'fecha_cierre': case 'fecha_rechazo':
                                $a = str_replace("_", " ", $key);
                                $label = ucfirst($a);

         ?> 
                                <div class="col-auto col-md-6">
                                    <label><?= $label ?></label>
                                    <?= html::input('date', $key,'',['class'=> 'form-control','id'=>'campos-id']) ?>
                                </div>

         <?php              
                            break;        
                            case 'cant_dias': case 'mes_deuda': case 'año_deuda': case 'numero_multa': case 'monto_garantia': 

                                if ($key == 'cant_dias') {
                                    $a = str_replace("cant_", "Cantidad ", $key);
                                    $label = ucfirst($a);
                                }else{
                                    $a = str_replace("_", " ", $key);
                                    $label = ucfirst($a);
                                }
        ?>
                                  <div class="col-auto col-md-6">
                                    <label><?= $label ?></label>
                                    <?= Html::input('decimal', $key, '',['class'=> 'form-control','id'=>'campos-id', 
                                        'format'=>['percent',2]]); ?>
                                </div>
        <?php 
                            break;
                            case 'nombre': case 'cuenta': case 'cuil': case 'sucursal': case 'documentacion':

        ?>

        <?php 
                            break;
                            case 'documentacion':
                                $a = str_replace("_", " ", $key);
                                $label = ucfirst($a);
        ?>              
                                  <div class="col-auto col-md-6">
                                    <label><?= $label ?></label>
                                    <?= html::textArea( $key,'',['class'=> 'form-control','id'=>'campos-id']) ?>
                                </div>
        <?php 
                            break;
                            default:
                                $a = str_replace("_", " ", $key);
                                $label = ucfirst($a);
        ?>              
                                  <div class="col-auto col-md-6">
                                    <label><?= $label ?></label>
                                    <?= html::input('text', $key,'',['class'=> 'form-control','id'=>'campos-id']) ?>
                                </div>
        <?php   
                            break;
                        }
                        $i++;
                    }elseif (is_string($value)){
                         switch ($key) {
                             case 'fecha_inhabilitacion': case 'fecha_emision': case 'fecha_rechazo': case 'fecha_cierre': case 'fecha_rechazo':
                                $a = str_replace("_", " ", $key);
                                $label = ucfirst($a);

         ?> 
                                <div class="col-auto col-md-6">
                                    <label><?= $label ?></label>
                                    <?= html::input('date', $key,$value,['class'=> 'form-control','id'=>'campos-id']) ?>
                                </div>

         <?php              
                            break;        
                            case 'cant_dias': case 'mes_deuda': case 'año_deuda': case 'numero_multa': case 'monto_garantia': 

                                if ($key == 'cant_dias') {
                                    $a = str_replace("cant_", "Cantidad ", $key);
                                    $label = ucfirst($a);
                                }else{
                                    $a = str_replace("_", " ", $key);
                                    $label = ucfirst($a);
                                }
        ?>
                                  <div class="col-auto col-md-6">
                                    <label><?= $label ?></label>
                                    <?= Html::input('decimal', $key, $value,['class'=> 'form-control','id'=>'campos-id', 
                                        'format'=>['percent',2]]); ?>
                                </div>
        <?php 
                            break;
                            case 'nombre': case 'cuenta': case 'cuil': case 'sucursal':

        ?>

        <?php 
                            break;
                            case 'documentacion':
                                $a = str_replace("_", " ", $key);
                                $label = ucfirst($a);
        ?>              
                                  <div class="col-auto col-md-6">
                                    <label><?= $label ?></label>
                                    <?= html::textArea( $key,$value,['class'=> 'form-control','id'=>'campos-id']) ?>
                                </div>
        <?php 
                            break;
                            default:
                                $a = str_replace("_", " ", $key);
                                $label = ucfirst($a);
        ?>              
                                  <div class="col-auto col-md-6">
                                    <label><?= $label ?></label>
                                    <?= html::input('text', $key,$value,['class'=> 'form-control','id'=>'campos-id']) ?>
                                </div>
        <?php   
                            break;
                        }
                        $i++;
                    }; 
                endforeach;
         };
         ?>

                  &nbsp; &nbsp;                
                <div class="col-lg-offset-2 col-lg-7" style="padding-top: 15px;">
                    <?= Html::submitButton('Emitir', ['class' => 'btn bg-navy color-palette btn-block', 'id' => 'create-button']) ?>
                </div>

        <?php ActiveForm::end(); ?>
                    </div>
                </section>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
    var checkActive = false;
    $('#firmante').prop('disabled', true);

    $(document).ready(function() {

        $('#chec').change(function() {

            if(checkActive === true) {

                $('#firmante').prop('disabled', true);
                checkActive = false;

            } else {

                 $('#firmante').prop('disabled', false);
                 checkActive = true;

            }

        });

    });

