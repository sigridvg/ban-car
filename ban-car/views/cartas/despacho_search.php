<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\CartasDocumento;

/* @var $this yii\web\View */
/* @var $model app\models\DespachoSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<!DOCTYPE html>
<html>
<head>
 
    <link rel="stylesheet" type="text/css" href="estilo-buscador">
</head>
<body>

    <div class="cartas-search">

        <?php $form = ActiveForm::begin([
            'action' => ['pending-delivery'],
            'method' => 'get',

        ]); ?> 
        <div class="form-row align-items-center">
            <div class="col-lg-5">
                <?php echo $form->field($model, 'id', [
                    'inputOptions' => [
                       'placeholder' => 'Id cartas',
                       'class' => 'form-control mb-2'],
                   ])->label(false); ?>
               </div>
               <div class="col-lg-5">  
                <?= $form->field($model, 'status')->dropDownList([
                    CartasDocumento::STATUS_SIGNED => 'FIRMADAS',
                    CartasDocumento::STATUS_PRINTED => 'IMPRESAS'
                ],
                ['prompt'=>'-- Estado --'])->label(false);?>      
            </div>
            <div class="form-group">
                <?= Html::submitButton('Buscar', ['class' => 'btn bg-navy color-palette']) ?>
                <?= Html::a('Borrar', ['pending-delivery'], ['class' => 'btn bg-purple-active color-palette']) ?>
            </div>
        </div>

        <?php ActiveForm::end(); ?>

    </div>



</body>
</html>

