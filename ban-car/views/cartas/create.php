<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\CartasDocumento */

$this->title = ' ';
$this->params['breadcrumbs'][] = ['label' => 'Cartas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => 'Crear', 'url' => ['/create']];
?>
    <link rel="stylesheet" type="text/css" href="../web/css/titulos.css">
    <div class="cartas-create">
<?php if (Yii::$app->session->hasFlash('cartaCreada')){ ?>
    <?php $this->title = 'Carta creada!' ?>
    <p>La carta se creo exitosamente. Mail enviado al Firmante</p>
    
    <?= Html::a('Redactar otra carta', ['cartas/create'], ['class' => 'btn bg-purple']) ?>
    
    
<?php }elseif(Yii::$app->session->hasFlash('creationFailed')){?>
        <?php $this->title = 'Algo salio mal!' ?>
    
        <p>No pudimos crear la carta documento</p>
        
        <?= Html::a('Reintentar', ['cartas/create'], ['class' => 'btn bg-purple']) ?>

<?php }else{ ?>
    <h1 id="titulo" style="text-align: center;"><?php echo '<b>CREAR' . '' . '</b>CARTAS' . '' ?></h1>
   
    <?= $this->render('_form', [
    'model' => $model, 'plantilla' =>$plantilla, 'campos' => $campos,'modelDest'=>$modelDest
]); ?>
    </div>
<?php }; ?>