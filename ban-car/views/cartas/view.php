<?php

use app\models\CartasDocumento;
use yii\helpers\Html;
use app\models\Usuarios;
use app\models\Plantillas;
use app\models\Destinatarios;
use app\models\Pais;
use app\models\Sucursales;
use yii\helpers\Url;
use yii\web\YiiAsset;

/* @var $this yii\web\View */
/* @var $model app\models\Destinatarios */


$this->title = ' ';
$this->params['breadcrumbs'][] = ['label' => 'Cartas Documento', 'url' => ['index']];
$this->params['breadcrumbs'][] = $model->id;
YiiAsset::register($this);
?>
<link rel="stylesheet" type="text/css" href="../web/css/titulos.css">


<br>
 
<section class="content">

      <div class="row">
        <div class="col-md-12">
          <div class="box box-primary">
            <div class="box-body box-profile">
             <!-- <img class="profile-user-img img-responsive img-circle" src="../../dist/img/user4-128x128.jpg" alt="User profile picture">-->
                <?php
                $dest = Destinatarios::find()->where("Cuit_Cuil=:Cuit_Cuil", [":Cuit_Cuil" => $model->destinatario])->andWhere("Cuenta=:cuenta", [":cuenta" => $model->nro_cuenta])->one();
                ?>

                <h3 class="profile-username text-center"><?= 'Destinatario: '. $dest->Nombre_RazonSocial . ' - Nro. Cuenta: ' . $model->nro_cuenta  ?></h3>


              <ul class="list-group-unbordered">
                    <li>
                        <i class="glyphicon glyphicon-user text-muted"></i> <strong>Usuario:</strong> 
                        <?= Usuarios::findOne($model->id_usuario)->NombreUs .' '. Usuarios::findOne($model->id_usuario)->ApellidoUs?> 
                    </li>
                    <li>
                        <i class="glyphicon glyphicon-calendar text-muted"></i><strong>Fecha de emision:</strong> <?= Html::encode($model->fecha)?>
                    </li>

                     <li>
                        <i class="fa fa-bank text-muted"></i><strong>Nro de Cuenta:</strong> <?= Html::encode($model->nro_cuenta) ?>
                    </li>  

                   
                    <li>
                        <i class="glyphicon glyphicon-user text-muted"></i> <b>Firmante: </b> 
                        
                        <?php
                        if ($model->id_firmante == null) { ?>
                          <strong>No se registro firmante</strong>                
                        <?php } else{
                            echo Usuarios::findOne($model->id_firmante)->NombreUs .' '. Usuarios::findOne($model->id_firmante)->ApellidoUs;
                            };
                        ?> 
                        
                    </li>
                    <li>
                        <i class="glyphicon glyphicon-envelope text-muted"></i> <b>Plantilla / Motivo: </b> 
                        <?= Plantillas::findOne($model->id_plantilla)->archivo ?> 
                    </li>
                    <li>
                        <i class="glyphicon glyphicon-user text-muted"></i> <b>Destinatario: </b> 
                        <?= $dest->Nombre_RazonSocial ?> 
                    </li>
                    <li>
                        <i class="glyphicon glyphicon-home text-muted"></i> <b>Sucursal: </b> 
                        <?= Sucursales::findOne($model->id_sucursal)->nombre ?> 
                    </li>
                    <?php if($model->motivo_revision != null){?>
                    <li style="color:red">
                        <i class="glyphicon glyphicon-home text-muted" style="color:red"></i> <b>Motivo revision: </b> 
                        <?= $model->motivo_revision ?> 
                    </li>
                        
                    <?php }?>
                    
                    
                    
              </ul>
              <div class="text-center" style="padding: 0.1%; border:1px solid black;margin: 5px;">
               
                        <h5><i class="fa fa-print text-muted"></i> <b> <a href=<?= Url::to('imprimir/'.$model->id) ?>  target="_blank"> Visualizar Carta </a></b> 
                         </h5>
                    
              </div>
              <div> 
              </div>

              <?php if (Yii::$app->user->identity->getIdRoles() == 3){ ?>
                  <?= Html::a('Firmar', ['firmar', 'id' => $model->id], ['class' => '.btn-sm btn bg-purple color-palette col-lg-12']) ?>
                  <?= Html::a('Solicitar Cambio', ['revisar', 'id' => $model->id], [
                'class' => '.btn-sm btn bg-navy color-palette col-lg-12','data' => [ 
                        'confirm' => 'Esta seguro que desea Solicitar cambios de esta carta?',
                        'method' => 'post',
                      ],
                ]) ?> 
              <?php } elseif (Yii::$app->user->identity->getIdRoles() == 2 && $model->status == CartasDocumento::STATUS_PENDING_REVISION) {?>
               <?= Html::a('Actualizar', ['update', 'id' => $model->id], ['class' => '.btn-sm btn bg-purple color-palette col-lg-12','data' => [ 
                        'confirm' => 'Esta seguro que desea actualizar esta carta?',
                        'method' => 'post',
                      ],]) ?>
                <?php } else {?>
                  <?= Html::a('Imprimir',['imprimir', 'id'=> $model->id], ['class'=>'.btn-sm btn bg-purple color-palette col-lg-12',
                              'data' => ['confirm' => "¿Desea Imprimir ésta carta?"],
                            ]);?>
                
                  <?=  Html::a('Descargar',['descargar', 'id'=> $model->id], ['class'=>'.btn-sm btn bg-navy color-palette col-lg-12', 'data' => ['confirm' => "¿Desea descargar ésta carta?"],
                            ]);?>
                  
               <?php }  ?>
            </div>
          </div>
          <!-- /.box -->

       
      </div>
      <!-- /.row -->

    </section>
    <!-- /.content -->
 