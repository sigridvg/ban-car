<?php

/* @var $this yii\web\View */
/* @var $form \yii\widgets\ActiveForm */

use app\models\Destinatarios;
use app\models\Plantillas;
use app\models\Sucursales;
use app\models\Usuarios;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\jui\DatePicker;
use app\models\Motivo;
use kartik\depdrop\DepDrop;

$this->title = 'CREAR CARTA DOCUMENTO';
$this->params['breadcrumbs'][] = ['label' => 'Cartas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => 'Armar Plantillas', 'url' => ['armar-plantilla']];
?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<?php if (Yii::$app->session->hasFlash('cartaCreada')): ?>
    <?php $this->title = 'Carta creada!' ?>
    <p>La carta se creo exitosamente. Mail enviado al Firmante</p>
    
    
    <?= Html::a('Redactar otra carta', ['cartas/armar-plantilla'], ['class' => 'btn bg-purple']) ?>
    <?= Html::a('Visualizar carta', Url::to(['cartas/imprimir', 'id' => $model->id]), ['class' => 'btn bg-black','target'=>'_blank']) ?>
    <?= Html::a('Actualizar', ['update', 'id' => $model->id], ['class' => '.btn-sm btn bg-yellow color-palette','data' => [ 
                        'confirm' => 'Esta seguro que desea actualizar esta carta?',
                        'method' => 'post',
                        'action'
                      ],]) ?>
    
    
    <?php elseif(Yii::$app->session->hasFlash('creationFailed')): ?>
        <?php $this->title = 'Algo salio mal!' ?>
        <p>No pudimos crear la carta documento</p>
        <?= Html::a('Reintentar', ['cartas/armar-plantilla'], ['class' => 'btn bg-purple']) ?>


    <?php else: ?>
    <link rel="stylesheet" type="text/css" href="../web/css/dropdown.css">
    <section>
    <h1 id="titulo" style="text-align: center;"><?php echo '<b>PLANTILLA' . ' ' . '</b> - MOTIVO' . '' ?></h1>

    <div class="cartas-create">
        <div class="col-md-12 col-lg-12">
            <?php $form = ActiveForm::begin([
                'id' => 'carta-documento-form',
                'method' => 'post',
            ]); ?>
            <div class="col-lg-offset-2 col-lg-8">
            <div class="col-md-6">
                <label>Motivo emision de Carta documento</label>
               <?= Html::dropDownList('id', null, ArrayHelper::map(Motivo::find()->all(), 'id', 'motivos'),[
                    'id' => 'id_motivo',
                    'class'=> 'form-control',
                    'prompt' => 'Seleccione un motivo'
            ]); ?>
            </div> 
            <div class="col-auto col-md-6">
                 <?= $form->field($model, 'id_plantilla')->widget(DepDrop::className(),
                    [
                        'pluginOptions' => [
                            'placeholder' => 'Elija una plantilla',
                            'depends' => ['id_motivo'],
                            'url' => Url::to(['/cartas/armar-plantilla'])
                        ]
                    ]
                )->label('Plantilla'); ?>
              
            </div>
            <div class="col-lg-12 col-md-3" style="margin: 1px;">
                <?= Html::submitButton('Seguir', ['class' => 'btn bg-navy color-palette btn-block', 'name' => 'motivo-button']) ?>
           
            </div>   
        </div>      
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
</section>
    </div>
<?php endif; ?>





