<?php
use app\models\Usuarios;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

$this->title = ' ';
$this->params['breadcrumbs'][] = ['label' => 'Cartas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => 'Crear', 'url' => ['/create']];
?>
    <link rel="stylesheet" type="text/css" href="../web/css/titulos.css">
    <div class="cartas-create">
        <h1 id="titulo" style="text-align: center;"><?php echo '<b>SOLICITAR' . '' . '</b>REVISION' . '' ?></h1>
    </div>
<div class="cartas-create">
   
    <div class="col-md-auto col-lg-auto">
        <?php $form = ActiveForm::begin([
            'id' => 'carta-revision-form',
            'method' => 'post',
        ]); ?>
    <div class="table-bordered table-hover ">  
        <section class="content">
            <div class="row">
                
                <div class="align-items-center">
                    <div class="form-group">                
                        <div class="col-md-12">
                            <?= $form->field($model, 'motivo_revision')->textarea(['rows' => '6']) ?>
                        </div>
                    </div>
                  &nbsp; &nbsp;                
                <div class="col-lg-offset-2 col-lg-7" style="padding-top: 15px;">
                    <?= Html::submitButton('Revisar', ['class' => 'btn bg-navy color-palette btn-block', 'id' => 'create-button']) ?>
                </div>

        <?php ActiveForm::end(); ?>
                </div>
                </section>
            </div>
        </div>
    </div>
<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

