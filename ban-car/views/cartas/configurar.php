<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;


/* @var $this yii\web\View */
/* @var $model app\models\Destinatarios */
/* @var $form yii\widgets\ActiveForm */
$this->title = ' ';
?>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>


<?php if (Yii::$app->session->hasFlash('configuracionExit')): ?>
    <?php $this->title = 'Configuracion cargada' ?>
    <p>Se actualizo la configuracion utilizada para las cartas documento.</p>
    
 
<?php else: ?>
    <link rel="stylesheet" type="text/css" href="../web/css/dropdown.css">
    <section>
    <h1 id="titulo" style="text-align: center;"><?php echo '<b>CONFIGURAR' . ' ' . '</b> CARTAS' . '' ?></h1>
        <div class="destinatarios-form">
            <div class="col-md-8 col-lg-12">
                <?php $form = ActiveForm::begin([
                    'enableAjaxValidation' => false, 
                    'id' => 'congiguracion-cartas-form',
                    'method' => 'post',
                ]);

                ?>
                <div class="align-items-center">
                    <div class="form-group">
                         <div class="col-auto col-md-4">
                            <?= $form->field($model, 'serie', ['options' => ['class' => 'no-margin']])->textInput() ?>
                        </div>
                        <div class="col-auto col-md-4">
                            <?= $form->field($model, 'kit', ['options' => ['class' => 'no-margin']])->textInput() ?>
                        </div>
                        <div class="col-auto col-md-4">
                            <?= $form->field($model, 'num_sig_carta', ['options' => ['class' => 'no-margin']])->textInput() ?>
                        </div>
                    </div>
                </div>
                <div class="col-lg-offset-2 col-lg-7">
                   <?= Html::submitButton('Configurar', ['class' => 'btn bg-navy color-palette btn-block', 'name' => 'actualizar-button']) ?>
                </div>

                <?php ActiveForm::end(); ?>
            </div>
        </div>
</section>
   
<?php endif; ?>
