<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\CartasSearch */
/* @var $form yii\widgets\ActiveForm */
?>

  <div class="cartas-search">
    <?php $form = ActiveForm::begin([
      'action' => ['index'],
      'method' => 'get',
      'options' => ['class' => 'form-horizontal'],
    ]); ?>
    <div class="form-row align-items-center">
      <div class="col-lg-4">
        <?= $form->field($model, 'id', [
          'inputOptions' => [
           'placeholder' => $model->getAttributeLabel('id'),
           'class' => 'form-control mb-2'],
           'options' => ['class' => 'no-margin']
         ])->label(false); ?>
       </div>
       <div class="col-lg-4">
        <?= $form->field($model, 'emisor_name', [
          'inputOptions' => [
           'placeholder' => $model->getAttributeLabel('emisor'),
           'class' => 'form-control mb-2',],
           'options' => ['class' => 'no-margin']
         ])->label(false); ?>
       </div>
       <div class="col-lg-4">
        <?= $form->field($model, 'destinatario_name', [
          'inputOptions' => [
            'placeholder' => 'Destinatario',
            'class' => 'form-control mb-2'],
            'options' => ['class' => 'no-margin']
          ])->label(false); ?>
        </div>
              <div class="col-lg-5">
        <?= Html::submitButton('Buscar', ['class' => 'btn bg-navy-active']) ?>
        <?= Html::a('Limpiar', ['index'], ['class' => 'btn bg-purple-active color-palette']) ?>
      </div>
      </div>

      <?php ActiveForm::end(); ?>

    </div>
