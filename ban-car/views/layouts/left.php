<?php

use app\models\Usuarios;
use app\models\Sectores;
use yii\helpers\Html;

?>
<aside class="main-sidebar">

    <section class="sidebar">

        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <?php echo Html::img('@web/img/BAN.CAR2.png', ['class' => 'img-circle user-image img-rounded img-responsive']); ?>
            </div>
            <div class="pull-left info">
                <p><?= Yii::$app->user->identity->NombreUs . ' ' . Yii::$app->user->identity->ApellidoUs ?> </p>


                <?php if (Yii::$app->user->identity->IdSectores == null) {
                    echo 'Sector sin definir';
                } else { ?>
                    <span> <?= Sectores::findOne(Yii::$app->user->identity->IdSectores)->Sectores ?> </span>
                <?php } ?>
            </div>
        </div>
        <?php
        $navItems = [1];
        if (Yii::$app->user->identity->IdRoles == Usuarios::ROLE_ADMIN) {
            array_shift($navItems);
            array_push($navItems,
                [

                    'label' => 'Cartas Documento',
                    'icon' => 'envelope',
                    'items' => [
                        [
                            'label' => 'Configurar',
                            'icon' => 'cog',
                            'url' => ['/cartas/configurar']
                        ],
                    ]
                ],
                [
                    'label' => 'Plantillas',
                    'icon' => 'list-alt',
                    'items' => [
                        [
                            'label' => 'Plantillas',
                            'icon' => 'tag',
                            'url' => ['/plantillas']
                        ],
                        [
                            'label' => 'Crear Plantilla',
                            'icon' => 'pencil',
                            'url' => ['/plantillas/create']
                        ],
                        [
                            'label' => 'Motivo de plantillas',
                            'icon' => 'bookmark',
                            'url' => ['/motivo']
                        ],
                    ],
                ],
                [
                    'label' => 'Destinatarios',
                    'icon' => 'font',
                    'url' => ['/destinatarios']
                ],
                [
                    'label' => 'Sectores',
                    'icon' => 'font',
                    'url' => ['/sectores']
                ],
                [
                    'label' => 'Usuarios',
                    'icon' => 'user',
                    'items' => [
                        [
                            'label' => 'Ver Usuarios',
                            'icon' => 'eye open',
                            'url' => ['/usuarios'],
                        ],
                        [
                            'label' => 'Crear Usuarios',
                            'icon' => 'pencil',
                            'url' => ['/usuarios/create']
                        ],
                        [
                            'label' => 'Auditoria',
                            'icon' => 'user',
                            'url' => ['/audit']
                        ],
                    ]
                ]);
        } elseif (Yii::$app->user->identity->IdRoles == Usuarios::ROLE_FIRMANTE) {
            array_shift($navItems);
            array_push($navItems,
                [

                    'label' => 'Cartas Documento',
                    'icon' => 'envelope',
                    'items' => [
                        [
                            'label' => 'Cartas Documento',
                            'icon' => 'tag',
                            'url' => ['/cartas']
                        ],
                        [
                            'label' => 'Crear Carta Documento',
                            'icon' => 'pencil',
                            'url' => ['/cartas/armar-plantilla']
                        ],
                        [
                            'label' => 'Pendiente de Autorizacion',
                            'icon' => 'hourglass',
                            'url' => ['/cartas/pending']
                        ],
                    ]
                ],
                [
                    'label' => 'Plantillas',
                    'icon' => 'list-alt',
                    'items' => [
                        [
                            'label' => 'Plantillas',
                            'icon' => 'tag',
                            'url' => ['/plantillas']
                        ],
                    ],
                ],
                [
                    'label' => 'Destinatarios',
                    'icon' => 'font',
                    'url' => ['/destinatarios']
                ]);
        } elseif (Yii::$app->user->identity->IdRoles == Usuarios::ROLE_OPERADOR) {
            array_shift($navItems);
            array_push($navItems,
                [

                    'label' => 'Cartas Documento',
                    'icon' => 'envelope',
                    'items' => [
                        [
                            'label' => 'Cartas Documento',
                            'icon' => 'tag',
                            'url' => ['/cartas']
                        ],
                        [
                            'label' => 'Crear Carta Documento',
                            'icon' => 'pencil',
                            'url' => ['/cartas/armar-plantilla']
                        ],
                        [
                            'label' => 'Pendiente de Revision',
                            'icon' => 'wrench',
                            'url' => ['/cartas/pending-revision']
                        ],
                        [
                            'label' => 'Pendiente de Despacho',
                            'icon' => 'send',
                            'url' => ['/cartas/pending-delivery']
                        ],
                    ]
                ],
                [

                    'label' => 'Plantillas',
                    'icon' => 'list-alt',
                    'items' => [
                        [
                            'label' => 'Plantillas',
                            'icon' => 'tag',
                            'url' => ['/plantillas']
                        ],
                    ],
                ],
                [
                    'label' => 'Destinatarios',
                    'icon' => 'font',
                    'url' => ['/destinatarios']
                ]);
        }

        ?>

        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu tree', 'data-widget' => 'tree'],
                'items' => $navItems,
            ]
        ) ?>

    </section>

</aside>
