<?php

use yii\helpers\Html;
use app\models\Usuarios;
use app\models\Sectores as Sectores;
use yii\helpers\ArrayHelper;

/* @var $this \yii\web\View */
/* @var $content string */
?>

<header class="main-header">

    <?= Html::a('<span class="logo-mini">BC</span><span class="logo-lg">' . Yii::$app->name . '</span>', Yii::$app->homeUrl, ['class' => 'logo']) ?>

    <nav class="navbar navbar-static-top" role="navigation">

        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>

        <div class="navbar-custom-menu">
            <?php if (!Yii::$app->user->isGuest): ?>
            <ul class="nav navbar-nav">
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">

                        <span class="hidden-xs">Usuario activo: <?= Yii::$app->user->identity->usernameUs ?></span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header">
                              <?php echo Html::img('@web/img/BAN.CAR2.png', ['class' => 'img-circle']); ?>

                            <p class="user-header">
                                <strong>
                                    - <?= Yii::$app->user->identity->NombreUs . ' ' . Yii::$app->user->identity->ApellidoUs ?> </strong>
                                <span> - </br><?php if (Yii::$app->user->identity->IdSectores == null) {
                                        echo 'Sector sin definir';
                                    } else {
                                        echo Sectores::find()->where(['ID_SECTORES' => Yii::$app->user->identity->IdSectores])->one()->Sectores;
                                    } ?></br></span>
                                <small> Miembro desde: <?php
                                    echo Yii::$app->formatter->asDate(Yii::$app->user->identity->tiempo_creacion, 'long'); ?> </small>

                            </p>
                        </li>
                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="pull-left">
                                <?= Html::a('Perfil', ['usuarios/profile', 'id' => Yii::$app->user->identity->IdUsuarios], ['class' => 'btn btn-default btn-flat']) ?>
                            </div>
                            <div class="pull-right">
                                <?= Html::a('Cerrar Sesion',['/site/logout','id'=>Yii::$app->user->identity->IdUsuarios],['data-method' => 'post', 'class' => 'btn btn-default btn-flat']
                                ) ?>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
<?php endif ?>
</header>