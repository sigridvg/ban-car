<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use yii\console\Controller;
use yii\console\ExitCode;

/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class HelloController extends Controller
{
    /**
     * This command echoes what you have entered as the message.
     * @param string $input
     * @param string $output
     * @param string $message the message to be echoed.
     * @return int Exit code
     */
    public function actionIndex($input, $output, $message = 'hello world')
    {
        $comando = "soffice --headless --convert-to pdf:writer_pdf_Export $input --outdir $output";
        echo shell_exec($comando);
        return ExitCode::OK;
    }
}
