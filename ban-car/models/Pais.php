<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pais".
 *
 * @property int $Cod_Pais
 * @property string $Nombre_Pais
 *
 * @property Destinatarios[] $destinatarios
 */
class Pais extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pais';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['Cod_Pais', 'Nombre_Pais'], 'required'],
            [['Cod_Pais'], 'integer'],
            [['Nombre_Pais'], 'string', 'max' => 50],
            [['Cod_Pais'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'Cod_Pais' => 'Cod Pais',
            'Nombre_Pais' => 'Nombre Pais',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDestinatarios()
    {
        return $this->hasMany(Destinatarios::className(), ['Codigo_Pais_DES' => 'Cod_Pais']);
    }
}
