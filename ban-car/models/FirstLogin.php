<?php

namespace app\models;

use yii\base\model;
use yii\db\ActiveRecord;

class FirstLogin extends ActiveRecord
{
    public $new_password;
    public $new_password_repeat;

    public function rules()
    {
        return [
            [['new_password', 'new_password_repeat'], 'required', 'message' => 'Campo requerido'],
            ['new_password', 'match', 'pattern' => "/^.{6,16}$/", 'message' => 'Mínimo 6 y máximo 16 caracteres'],
            ['new_password_repeat', 'compare', 'compareAttribute' => 'new_password', 'message' => 'Los passwords no coinciden'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'new_password' => 'Nueva contraseña',
            'new_password_repeat' => 'Repetir nueva contraseña',
        ];
    }
}
