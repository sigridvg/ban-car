<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "provincia".
 *
 * @property int $ID_PROV
 * @property string $Provincia
 * @property int $Codigo_Postal
 *
 * @property Ciudad[] $ciudads
 * @property Destinatarios[] $destinatarios
 */
class Provincia extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'provincia';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ID_PROV', 'Provincia', 'Codigo_Postal'], 'required'],
            [['ID_PROV', 'Codigo_Postal'], 'integer'],
            [['Provincia'], 'string', 'max' => 255],
            [['ID_PROV'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID_PROV' => 'Id Prov',
            'Provincia' => 'Provincia',
            'Codigo_Postal' => 'Codigo Postal',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCiudads()
    {
        return $this->hasMany(Ciudad::className(), ['ID_PROV' => 'ID_PROV']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDestinatarios()
    {
        return $this->hasMany(Destinatarios::className(), ['ID_Provincia_DES' => 'ID_PROV']);
    }
}
