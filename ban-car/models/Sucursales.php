<?php

namespace app\models;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "sucursales".
 *
 * @property int $id
 * @property string $nombre
 * @property int $id_pais
 * @property int $id_localidad
 * @property int $id_provincia
 * @property string|null $direccion
 */
class Sucursales extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'sucursales';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre', 'id_pais', 'id_localidad', 'id_provincia'], 'required'],
            [['id_pais', 'id_localidad', 'id_provincia'], 'integer'],
            [['nombre'], 'string', 'max' => 100],
            [['direccion'], 'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'id_pais' => 'Id Pais',
            'id_localidad' => 'Id Localidad',
            'id_provincia' => 'Id Provincia',
            'direccion' => 'Direccion',
        ];
    }

}
