<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\CartasDocumento;

class DespachoSearch extends CartasDocumento
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'nro_cuenta', 'id_firmante'], 'integer'],
            [['fecha', 'emisor', 'destinatario', 'plantilla', 'sucursal'], 'safe'],
            [['status'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $estados=[CartasDocumento::STATUS_PRINTED,CartasDocumento::STATUS_SIGNED];
        $query = CartasDocumento::find()->where(['sector' => Yii::$app->user->identity->getIdSectores()]);
        $query->andFilterWhere(['in','status', $estados]);
        
    
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 5,
            ],

        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
        ]);
        $query->andFilterWhere(['like', 'status', $this->status]);

        return $dataProvider;
    }
}
