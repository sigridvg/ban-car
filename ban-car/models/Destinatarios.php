<?php

namespace app\models;

use yii\db\ActiveQuery;
use yii\db\ActiveRecord;


/**
 * This is the model class for table "destinatarios".
 *
 * @property int $ID_DES
 * @property int $DNI
 * @property string $Nombre_RazonSocial
 * @property double $Cuit_Cuil
 * @property int $ID_Tipo
 * @property int $ID_Provincia
 * @property int $ID_Localidad
 * @property int $Direccion
 * @property string $Email
 * @property CartasDocumento[] $cartasdocumento
 * @property Provincias $provinciaDES
 * @property Localidades $localidadDES
 * @property TipoDestinatario $tipoDES
 */
class Destinatarios extends ActiveRecord
{   const SCENARIO_UPDATE = "update";
    const SCENARIO_UPDATE_LOCALIDAD = "update_localidad";
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'destinatarios';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['DNI','Nombre_RazonSocial', 'Cuit_Cuil', 'Tipo_documento', 'Sucursal','Cuenta','Grupo','Subgrupo','Tipo_cuenta','Direccion','ID_Provincia','Codigo_pais','ID_Provincia','Tipo_domicilio'], 'required'],
            [['DNI','Tipo_documento','Fecha_Nacimiento','Cuit_Cuil','Telefono', 'Celular','Cod_postal','Estado_civil','Cod_actividad','Sucursal','Cuenta','ID_Provincia','Codigo_pais','ID_Localidad','Tipo_domicilio'], 'integer'],
            ['Email','email','message'=>'Formato no valido'],
            [['ID_Localidad'], 'required', 'when'=> function($model){
                    return isset($model->ID_Provincia);
            }],
            [['Nombre_RazonSocial','Direccion'], 'string', 'max' => 100],
            [['Subgrupo','Grupo','Tipo_cuenta','Sexo','Ciudadania'], 'string', 'max' => 50],
            [['Codigo_pais'], 'exist', 'skipOnError' => true, 'targetClass' => Pais::className(), 'targetAttribute' => ['Codigo_pais' => 'Cod_Pais']],
            [['ID_Provincia'], 'exist', 'skipOnError' => true, 'targetClass' => Provincias::className(), 'targetAttribute' => ['ID_Provincia' => 'id']],
            [['ID_Localidad'], 'exist', 'skipOnError' => true, 'targetClass' => Localidades::className(), 'targetAttribute' => ['ID_Localidad' => 'id']],
        ];
    }
    
     public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_UPDATE_LOCALIDAD] = ['Nombre_RazonSocial', 'Sucursal','Cuenta','Grupo','Tipo_cuenta','Direccion','ID_Provincia','Codigo_pais','ID_Provincia','Tipo_domicilio','ID_Localidad'];
        $scenarios[self::SCENARIO_UPDATE] = ['Nombre_RazonSocial', 'Sucursal','Cuenta','Grupo','Tipo_cuenta','Direccion','ID_Provincia','Codigo_pais','ID_Provincia','Tipo_domicilio'];
        return $scenarios;
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'DNI'                              =>'Numero de Documento',
            'Tipo_documento'                   =>'Tipo de Documento',
            'Nombre_RazonSocial'               =>'Nombre',
            'Cuit_Cuil'                        =>'Cuil/Cuit',
            'Sexo'                             =>'Sexo',
            'Fecha_Nacimiento'                 =>'Fecha de Nacimiento',
            'Ciudadania'                       =>'Ciudadania',
            'Telefono'                         =>'Telefono',
            'Celular'                          =>'Celular',
            'Cod_postal'                       =>'Codigo Postal',
            'Email'                            =>'Email',
            'Estado_civil'                     =>'Estado Civil',
            'Cod_actividad'                    =>'Codigo de Actividad',
            'Direccion'                        =>'Direccion', 
            'Sucursal'                         =>'Sucursal',
            'Cuenta'                           =>'Cuenta',
            'Grupo'                            =>'Grupo',
            'Subgrupo'                         =>'Subgrupo',
            'Tipo_cuenta'                      =>'Tipo de Cuenta',
            'Codigo_pais'                      =>'Codigo de Pais',
            'ID_Provincia'                     =>'Provincia',
            'ID_Localidad'                     =>'Localidad'
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getCartasdocumento()
    {
        return $this->hasMany(CartasDocumento::className(), ['destinatarios' => 'Cuit_cuil']);
    }

    public function getPais()
    {
        return $this->hasOne(Pais::className(), ['Cod_Pais' => 'Codigo_pais']);
    }
    
    public function getProvincia()
    {
        return $this->hasOne(Provincias::className(), ['id' => 'ID_Provincia']);
    }
    public function getLocalidades()
    {
        return $this->hasOne(Localidades::className(), ['id' => 'ID_Localidades']);
    }
    public function getSucursales()
    {
        return $this->hasOne(Sucursales::className(), ['id' => 'Sucursal']);
    }
    public function getName()
    {
        $destinatario = findOne(Destinatarios::className(), ['Cuit_Cuil' => 'destinatario']);
        return $destinatario->Nombre_RazonSocial;
    }
    
    public function count()
    {
      $cantidad = Destinatarios::find()->count();
      return  $cantidad;
    }
    

}
