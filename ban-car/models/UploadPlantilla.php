<?php

namespace app\models;

use PhpOffice\PhpWord\Settings;
use Yii;
use yii\base\Model;
use yii\web\UploadedFile;

class UploadPlantilla extends Model
{
    /**
     * @var UploadedFile
     */
    public $plantilla;

    public function rules()
    {
        return [
            [['plantilla'], 'file', 'skipOnEmpty' => false, 'extensions' => 'docx'],
        ];
    }

    public function upload()
    {
        if ($this->validate()) {
            $filename = Yii::getAlias('@app') . '/cartas/' . $id . '.docx';

            Settings::setTempDir(Yii::getAlias('@app') . '/tmp');
            $this->plantilla->saveAs(Yii::getAlias('@app') . '/plantillas/' . $this->plantilla->baseName . '.' . $this->plantilla->extension);
            return true;
        } else {
            return false;
        }
    }
}