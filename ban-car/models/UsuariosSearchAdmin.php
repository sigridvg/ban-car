<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Usuarios;

/**
 * UsuariosSearch represents the model behind the search form of `app\models\Usuarios`.
 */
class UsuariosSearchAdmin extends Usuarios
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['IdUsuarios', 'LegajoUs', 'IdRoles', 'IdSectores'], 'integer'],
            [['NombreUs', 'ApellidoUs', 'usernameUs', 'emailUs', 'password_hash', 'tiempo_creacion', 'tiempo_ultima_modificacion', 'tiempo_ultimo_logueo', 'ip_registrada', 'authkey', 'codigo_verificacion'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Usuarios::find()->andFilterWhere(['=', 'IdRoles', 1]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 5,
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'IdUsuarios' => $this->IdUsuarios,
            'LegajoUs' => $this->LegajoUs,
            'IdRoles' => $this->IdRoles,
            'IdSectores' => $this->IdSectores,
        ]);

        $query->andFilterWhere(['like', 'NombreUs', $this->NombreUs])
            ->andFilterWhere(['like', 'ApellidoUs', $this->ApellidoUs])
            ->andFilterWhere(['like', 'usernameUs', $this->usernameUs])
            ->andFilterWhere(['like', 'emailUs', $this->emailUs])
            ->andFilterWhere(['like', 'password_hash', $this->password_hash])
            ->andFilterWhere(['like', 'tiempo_creacion', $this->tiempo_creacion])
            ->andFilterWhere(['like', 'tiempo_ultima_modificacion', $this->tiempo_ultima_modificacion])
            ->andFilterWhere(['like', 'tiempo_ultimo_logueo', $this->tiempo_ultimo_logueo])
            ->andFilterWhere(['like', 'ip_registrada', $this->ip_registrada])
            ->andFilterWhere(['like', 'authkey', $this->authkey])
            ->andFilterWhere(['like', 'codigo_verificacion', $this->codigo_verificacion]);

        return $dataProvider;
    }
}
