<?php

namespace app\models;

use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "cartasdocumento".
 *
 * @property int $id
 * @property int $id_OCA
 * @property int $id_plantilla
 * @property int $id_firmante
 * @property int $id_usuario
 * @property int $id_sucursal
 * @property int|null $nro_cuenta
 * @property string|null $fecha
 * @property string|null $status
 * @property string|null $update
 * @property string|null $serie
 * @property int $kit
 * @property int $num_carta
 * @property int $num_sig_carta
 * @property string|null $motivo_revision
 * @property string|null $documentacion
 * 
 * @property Usuarios $usuarios
 * @property Sucursales $sucursales
 * @property Destinatarios $destina
 * tarios
 * @property Plantillas $plantillas
 */
class CartasDocumento extends ActiveRecord
{
    const STATUS_PENDING_SIGNATURE = 'PENDING_SIGNATURE';
    const STATUS_PENDING_REVISION = 'PENDING_REVISION';
    const STATUS_SIGNED = 'SIGNED';
    const STATUS_PRINTED = 'PRINTED';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cartasdocumento';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_plantilla', 'id_usuario', 'id_sucursal',  'fecha'], 'required'],
            [['nro_cuenta', 'id_firmante','kit','num_carta','num_sig_carta'], 'integer'],
            ['status', 'default', 'value' => CartasDocumento::STATUS_PENDING_SIGNATURE],
            [['status', 'id_OCA','serie'], 'string', 'max' => 50],
            [['campos','motivo_revision','documentacion'], 'string', 'max' => 254],
            [['update'], 'string', 'max' => 1],
            [['id_plantilla'], 'exist', 'skipOnError' => true, 'targetClass' => Plantillas::className(), 'targetAttribute' => ['id_plantilla' => 'id']],
            [['id_sucursal'], 'exist', 'skipOnError' => true, 'targetClass' => Sucursales::className(), 'targetAttribute' => ['id_sucursal' => 'id']],
            [['destinatario'], 'exist', 'skipOnError' => true, 'targetClass' => Destinatarios::className(), 'targetAttribute' => ['destinatario' => 'Cuit_Cuil']],
            [['id_firmante'], 'exist', 'skipOnError' => true, 'targetClass' => Usuarios::className(), 'targetAttribute' => ['id_firmante' => 'IdUsuarios']],
            ['id_firmante', function ($attribute, $params, $validator) {
                $cuil = $this->destinatario;
                $destinatario = Destinatarios::find()->where(['Cuit_Cuil'=>$cuil])->one();
                $firmante = Usuarios::find()->where(['IdUsuarios'=>$this->$attribute])->one();
                if ( $firmante->documento == $destinatario->DNI) {
                    $this->addError($attribute, 'El Firmante no puede ser igual al destinatario.');
                }
            }],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_OCA' =>'ID OCA',
            'id_plantilla' => 'Plantilla',
            'id_firmante' => 'Firmante',
            'destinatario' => 'Destinatario',
            'id_sucursal' => 'Sucursal',
            'nro_cuenta' => 'Nro. de cuenta',
            'fecha' => 'Fecha',
            'status' => 'Estado',
            'update' => 'Update',
            'serie' =>'Serie',
            'kit' => 'Numero de identificacion del Banco ',
            'num_carta' => 'Numero de carta',
            'num_sig_carta' => 'Numero de siguiente carta',
            'motivo_revision' => 'Motivo solicitud de Revision',
            'documentacion' => 'Documentacion'
        ];
    }

    public static function findIdentity($IdCarta)
    {
        return self::findOne($IdCarta);
    }
    /**
     * @return ActiveQuery
     */
    public function getPlantillas()
    {
        return $this->hasOne(Plantillas::className(), ['id' => 'id_plantilla']);
    }

    public static function findByDestinatario($destinatario)
    {
        return self::findOne(['destinatario' => $destinatario]);
    }

    /**
     * @return ActiveQuery
     */
    public function getUsuarios()
    {
        return $this->hasOne(Usuarios::className(), ['IdUsuarios' => 'id_usuario']);
    }

    public function getFirmante()
    {
        return $this->hasOne(Usuarios::className(), ['IdUsuarios' => 'id_firmante']);
    }

    /**
     * @return ActiveQuery
     */
    public function getDestinatarios()
    {
        return $this->hasOne(Destinatarios::className(), ['Cuit_Cuil' => 'destinatario']);
    }

    /**
     * @return ActiveQuery
     */
    public function getSucursales()
    {
        return $this->hasOne(Sucursales::className(), ['id' => 'id_sucursal']);
    }

    /**
     * @return string
     */
    public function getStatusDescription()
    {
        switch ($this->status) {
            case $this::STATUS_PENDING_SIGNATURE:
                return 'Pendiente de firma';
            case $this::STATUS_PENDING_REVISION:
                return 'Pendiente de revision';
            case $this::STATUS_SIGNED:
                return 'Firmado';
            case $this::STATUS_PRINTED:
                return 'Impreso';
        }
        return null;
    }

    public function cantidadCartas(){
            $mes = 1;
            $arrayCD = [];
            do {
                $params = [':accionAUD'=>'CREAR', ':moduloAUD' =>'CARTA DOCUMENTO', ':mes'=>$mes, ':idSector' => Yii::$app->user->identity->getIdSectores()];
                $query = Yii::$app->db->createCommand('Select count(*) from ((SELECT * FROM audit UNION SELECT * FROM audit_history) audit), usuarios where accion_AUD =:accionAUD AND modulo_AUD =:moduloAUD AND MONTH(fecha_AUD) =:mes and usuario_legajo_AUD = LegajoUs and IdSectores =:idSector',$params)->queryScalar();
                $arrayCD[] = $query;
                $mes++;
                
            }while ($mes <= 12);

            return $arrayCD;  
        
    }
      
}