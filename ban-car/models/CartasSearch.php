<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * CartasSearch represents the model behind the search form of `app\models\CartaDocumento`.
 * @property string|null $emisor_name
 * @property string|null $destinatario_name
 */
class CartasSearch extends CartasDocumento
{
    public $emisor_name;
    public $destinatario_name;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id','id_firmante'], 'number'],
            [['destinatario_name', 'emisor_name'], 'safe'],
            [['status'],'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CartasDocumento::find()->andFilterWhere(['sector' => Yii::$app->user->identity->getIdSectores()]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 5,
            ],
        ]);

        if (!($this->load($params) && $this->validate())) {
            //$query->where('0=1');
            return $dataProvider;
        }

        if (!empty($this->id)) {
            $query->andFilterWhere(['id' => $this->id]);
        }

        if (!empty($this->destinatario_name)) {
            $destinatarios = Destinatarios::find()
                ->where(['like', 'Nombre_RazonSocial', $this->destinatario_name])
                ->select('Cuit_Cuil');

            $query->andFilterWhere(['in', 'destinatario', $destinatarios]);
        }

        if (!empty($this->emisor_name)) {
            $emisores = Usuarios::find()
                ->where(['IdSectores' => Yii::$app->user->identity->getIdSectores()])
                ->andFilterWhere(['like', 'NombreUs', $this->emisor_name])
                ->orFilterWhere(['like', 'ApellidoUs', $this->emisor_name])
                ->select('IdUsuarios');
            $query->andFilterWhere(['in', 'id_usuario', $emisores]);
        }

        if (!empty($this->status)) {
            $query->andFilterWhere(['status' => $this->status]);
        }

        if (!empty($this->id_firmante)) {
            $query->andFilterWhere(['id_firmante' => $this->id_firmante]);
        }

        return $dataProvider;
    }
}
