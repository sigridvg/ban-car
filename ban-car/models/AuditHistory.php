<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "audit_history".
 *
 * @property int $ID_AUD
 * @property int $usuario_legajo_AUD
 * @property string $accion_AUD
 * @property string $modulo_AUD
 * @property string $fecha_AUD
 * @property string $hora_AUD
 * @property string $ip_AUD
 * @property string $descripcion_AUD
 */
class AuditHistory extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'audit_history';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['usuario_legajo_AUD'], 'integer'],
            [['accion_AUD'], 'required'],
            [['accion_AUD', 'modulo_AUD', 'fecha_AUD', 'hora_AUD', 'ip_AUD', 'descripcion_AUD'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
     public function attributeLabels()
    {
        return [
           'ID_AUD' => 'Id Aud',
            'usuario_legajo_AUD' => 'Usuario',
            'accion_AUD' => 'Accion',
            'modulo_AUD'=> 'Modulo',
            'fecha_AUD' => 'Fecha',
            'hora_AUD' => 'Hora',
            'ip_AUD' => 'Ip de Registracion',
            'descripcion_AUD' => 'Descripcion',
        ];
    }

    public function getUsuario()
    {
        return $this->hasOne(Usuarios::className(), ['LegajoUs' => 'usuario_legajo_AUD']);
    }
}
