<?php

namespace app\models;

use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use yii\web\UploadedFile;

/**
 * This is the model class for table "usuarios".
 *
 * @property int $IdUsuarios
 * @property string|null $NombreUs
 * @property string|null $ApellidoUs
 * @property int $LegajoUs
 * @property int $IdRoles
 * @property int|null $IdSectores
 * @property string $usernameUs
 * @property string $emailUs
 * @property int $flag
 * @property string|null $password_hash
 * @property string|null $tiempo_creacion
 * @property string|null $tiempo_ultima_modificacion
 * @property string|null $tiempo_ultimo_logueo
 * @property string|null $ip_registrada
 * @property string|null $authkey
 * @property string|null $codigo_verificacion
 * @property UploadedFile $signature
 * @property Roles $roles
 * @property Sectores $sectores
 *
 */
class Usuarios extends ActiveRecord implements IdentityInterface
{
    const ROLE_ADMIN = 1;
    const ROLE_OPERADOR = 2;
    const ROLE_FIRMANTE = 3;
    const DEFAULT_PASS = "usuario_bancar";
    const SCENARIO_UPDATE = "update";
    const SCENARIO_INSERT = "insert";

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'usuarios';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ApellidoUs', 'NombreUs', 'LegajoUs', 'IdSectores', 'usernameUs', 'emailUs','IdRoles','documento'], 'required', 'on' => 'insert'],
            [['LegajoUs', 'IdRoles', 'IdSectores','documento'], 'integer'],
            [['tiempo_creacion'], 'safe'],
            [['ApellidoUs', 'authKey', 'tiempo_creacion', 'tiempo_ultimo_logueo', 'ip_registrada', 'authkey', 'codigo_verificacion'], 'string', 'max' => 50],
            [['NombreUs', 'usernameUs'], 'string', 'max' => 45],
            [['emailUs'], 'email'],
            [['signature'], 'image', 'minWidth' => 225, 'maxWidth' => 355, 'minHeight' => 225, 'maxHeight' => 355, 'skipOnEmpty' => false, 'extensions' => 'jpg, png', 'on' => 'insert'],
            [['signature'], 'image', 'minWidth' => 225, 'maxWidth' => 355, 'minHeight' => 225, 'maxHeight' => 355, 'skipOnEmpty' => false, 'extensions' => 'jpg, png', 'on' => 'update'],
            [['password_hash', 'tiempo_ultima_modificacion'], 'string', 'max' => 255],
            [['usernameUs', 'LegajoUs', 'emailUs', 'signature'], 'unique'],
            [['IdRoles'], 'exist', 'skipOnError' => true, 'targetClass' => Roles::className(), 'targetAttribute' => ['IdRoles' => 'ID_ROLES']],
            [['IdSectores'], 'exist', 'skipOnError' => true, 'targetClass' => Sectores::className(), 'targetAttribute' => ['IdSectores' => 'ID_SECTORES']],
        ];
    }

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_UPDATE] = ['ApellidoUs', 'NombreUs', 'LegajoUs', 'IdSectores', 'usernameUs', 'emailUs','IdRoles','documento']; //remove signature to make it work
        $scenarios[self::SCENARIO_INSERT] = ['ApellidoUs', 'NombreUs', 'LegajoUs', 'IdSectores', 'usernameUs', 'emailUs', 'IdRoles','documento']; //remove signature to make it work
        return $scenarios;
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'IdUsuarios' => 'ID del usuario',
            'NombreUs' => 'Nombre',
            'ApellidoUs' => 'Apellido',
            'LegajoUs' => 'Legajo',
            'IdRoles' => 'ID de Rol',
            'IdSectores' => 'ID de Sector',
            'usernameUs' => 'Nombre de usuario',
            'emailUs' => 'Email',
            'password_hash' => 'Password Hash',
            'tiempo_creacion' => 'Tiempo de Creacion',
            'tiempo_ultima_modificacion' => 'Tiempo de Ultima Modificacion',
            'tiempo_ultimo_logueo' => 'Tiempo de Ultimo Logueo',
            'ip_registrada' => 'Ip Registrada',
            'authkey' => 'Authkey',
            'codigo_verificacion' => 'Codigo de Verificacion',
            'signature' => 'Archivo de firma',
            'documento'=> 'Nro de docuemento',
        ];
    }

    public function getAuthKey()
    {
        return $this->authkey;
    }

    public function getId()
    {
        return $this->IdUsuarios;
    }

    public function getIdSectores()
    {
        return $this->IdSectores;
    }

    public function validateAuthKey($authKey)
    {
        return $this->authkey === $authKey;
    }

    public static function findIdentity($IdUsuarios)
    {
        return self::findOne($IdUsuarios);
    }

    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new yii\base\NotSupportedException();
    }

    public static function findByUsername($usernameUs)
    {
        return self::findOne(['usernameUs' => $usernameUs]);
    }

    /**
     * @return ActiveQuery
     */
    public function getRoles()
    {
        return $this->hasOne(Roles::className(), ['ID_ROLES' => 'IdRoles']);
    }

    public function getIdRoles()
    {
        return $this->IdRoles;
    }

    public function getFirmanteName()
    {
        $usuario = findOne(Usuarios::className(), ['IdUsuarios' => 'id_firmante']);
        $nombreCompleto = $usuario->NombreUs . ' ' . $usuario->ApellidoUs;
        return $nombreCompleto;
    }

    /**
     * @return ActiveQuery
     */
    public function getSectores()
    {
        return $this->hasOne(Sectores::className(), ['ID_SECTORES' => 'IdSectores']);
    }

    /**
     * @return string
     */
    public function getFullName()
    {
        return $this->NombreUs . ' ' . $this->ApellidoUs;
    }

    /**
     * @param $IdUsuarios
     * @return bool
     */
    public static function isUserAdmin($IdUsuarios)
    {
        if (Usuarios::findOne(['IdUsuarios' => $IdUsuarios, 'IdRoles' => 1])) {
            return true;
        } else {
            return false;
        }

    }

    public static function isUserOperador($IdUsuarios)
    {
        if (Usuarios::findOne(['IdUsuarios' => $IdUsuarios, 'IdRoles' => 2])) {
            return true;
        } else {

            return false;
        }
    }

    public static function isUserFirmante($IdUsuarios)
    {
        if (Usuarios::findOne(['IdUsuarios' => $IdUsuarios, 'IdRoles' => 3])) {
            return true;
        } else {

            return false;
        }
    }

    public function ultimosDias()
    {
        return self::find()->where(['fecha_hora_AUD' => 'DATE(NOW()) + INTERVAL -7 DAY']);

    }

    public function count()
    {
        $cantidad = Usuarios::find()->count();
        return $cantidad;
    }

    public function countLegales()
    {
        $cantidad = Usuarios::find()->where(['IdSectores' => 1])->count();
        return $cantidad;
    }

    public function countCC()
    {
        $cantidad = Usuarios::find()->where(['IdSectores' => 3])->count();
        return $cantidad;
    }

    public function countBP()
    {
        $cantidad = Usuarios::find()->where(['IdSectores' => 2])->count();
        return $cantidad;
    }
    public function countKYC()
    {
        $cantidad = Usuarios::find()->where(['IdSectores' => 4])->count();
        return $cantidad;
    }

    public function cantidadLogueosBP()
    {
        $mes = 1;
        $arrayBP = [];
        do {
            $params = [':accionAUD' => 'LOGUEO', ':mes' => $mes, ':idSector' => 2];
            $query = Yii::$app->db->createCommand('Select count(*) from ((SELECT * FROM audit UNION SELECT * FROM audit_history) audit), usuarios where accion_AUD =:accionAUD AND MONTH(fecha_AUD) =:mes and usuario_legajo_AUD = LegajoUs and IdSectores =:idSector', $params)->queryScalar();
            $arrayBP[] = $query;
            $mes++;

        } while ($mes <= 12);

        return $arrayBP;
    }
    public function cantidadLogueosKYC()
    {
        $mes = 1;
        $arrayBP = [];
        do {
            $params = [':accionAUD' => 'LOGUEO', ':mes' => $mes, ':idSector' => 4];
            $query = Yii::$app->db->createCommand('Select count(*) from ((SELECT * FROM audit UNION SELECT * FROM audit_history) audit), usuarios where accion_AUD =:accionAUD AND MONTH(fecha_AUD) =:mes and usuario_legajo_AUD = LegajoUs and IdSectores =:idSector', $params)->queryScalar();
            $arrayBP[] = $query;
            $mes++;

        } while ($mes <= 12);

        return $arrayBP;
    }

    public function cantidadLogueosLG()
    {
        $mes = 1;
        $arrayBP = [];
        do {
            $params = [':accionAUD' => 'LOGUEO', ':mes' => $mes, ':idSector' => 1];
            $query = Yii::$app->db->createCommand('Select count(*) from ((SELECT * FROM audit UNION SELECT * FROM audit_history) audit), usuarios where accion_AUD =:accionAUD AND MONTH(fecha_AUD) =:mes and usuario_legajo_AUD = LegajoUs and IdSectores =:idSector', $params)->queryScalar();
            $arrayBP[] = $query;
            $mes++;

        } while ($mes <= 12);

        return $arrayBP;
    }

    public function cantidadLogueosCC()
    {
        $mes = 1;
        $arrayBP = [];
        do {
            $params = [':accionAUD' => 'LOGUEO', ':mes' => $mes, ':idSector' => 3];
            $query = Yii::$app->db->createCommand('Select count(*) from ((SELECT * FROM audit UNION SELECT * FROM audit_history) audit), usuarios where accion_AUD =:accionAUD AND MONTH(fecha_AUD) =:mes and usuario_legajo_AUD = LegajoUs and IdSectores =:idSector', $params)->queryScalar();
            $arrayBP[] = $query;
            $mes++;

        } while ($mes <= 12);

        return $arrayBP;
    }

    public function firmante()
    {
        $cantidad = Usuarios::find()->where(['IdRoles' => 3])->andWhere(['IdSectores' => Yii::$app->user->identity->getIdSectores()])->count();
        return $cantidad;
    }

    public function operadores()
    {
        $cantidad = Usuarios::find()->where(['IdRoles' => 2])->andWhere(['IdSectores' => Yii::$app->user->identity->getIdSectores()])->count();
        return $cantidad;
    }

}

