<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "sectores".
 *
 * @property int $ID_SECTORES
 * @property string $Sectores
 *
 * @property SectoresPlantillas[] $sectoresPlantillas
 * @property Usuarios[] $usuarios
 */
class Sectores extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'sectores';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['Sectores'], 'required'],
            [['Sectores'], 'string', 'max' => 45],
            [['Sectores'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID_SECTORES' => 'Id Sectores',
            'Sectores' => 'Sectores',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSectoresPlantillas()
    {
        return $this->hasMany(SectoresPlantillas::className(), ['ID_SECTOR' => 'ID_SECTORES']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsuarios()
    {
        return $this->hasMany(Usuarios::className(), ['IdSectores' => 'ID_SECTORES']);
    }
}
