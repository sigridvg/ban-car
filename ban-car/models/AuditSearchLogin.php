<?php

namespace app\models;

use yii\base\Model;
use yii\data\SqlDataProvider;

/**
 * AuditSearch represents the model behind the search form of `app\models\Audit`.
 */
class AuditSearchLogin extends AuditHistory
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ID_AUD'], 'integer'],
            [['usuario_legajo_AUD', 'accion_AUD', 'modulo_AUD', 'fecha_AUD', 'hora_AUD', 'ip_AUD', 'descripcion_AUD'], 'safe'],
        ];
    }


    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return SqlDataProvider
     */
    public function search($params)
    {
        $this->load($params);
        $accion = "LOGUEO";
        $modulo = "USUARIOS";

        if (!($this->load($params) && $this->validate())) {

            $query = 'SELECT ID_AUD, usuario_legajo_AUD, accion_AUD, modulo_AUD, fecha_AUD, hora_AUD, ip_AUD, descripcion_AUD FROM ((SELECT * FROM audit UNION SELECT * FROM audit_history ) audit)WHERE accion_AUD = "LOGUEO" ORDER BY fecha_AUD, hora_AUD asc';


            $dataProvider = new SqlDataProvider([
                'sql' => $query,
                'key' => 'ID_AUD',
                'pagination' => [
                   'pageSize' => 8,
                ],
            ]);

            return $dataProvider;

        } else {

            $this->load($params);

            $query = 'SELECT ID_AUD, usuario_legajo_AUD, accion_AUD, modulo_AUD, fecha_AUD, hora_AUD, ip_AUD, descripcion_AUD FROM ((SELECT * FROM audit WHERE modulo_AUD LIKE :modulo and accion_AUD LIKE :accion UNION SELECT * FROM audit_history WHERE modulo_AUD LIKE :modulo and accion_AUD LIKE :accion) audit) ORDER BY fecha_AUD, hora_AUD asc';


            $dataProvider = new SqlDataProvider([
                'sql' => $query,
                'key' => 'ID_AUD',
                'params' => [':accion' => '%'.$accion.'%', ':modulo' => '%'.$modulo.'%'],
                'pagination' => [
                    'pageSize' => 8,
                ],
            ]);

            return $dataProvider;
        }
    }

}


