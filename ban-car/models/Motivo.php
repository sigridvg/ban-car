<?php

namespace app\models;

use Yii;
use app\models\Sectores;

/**
 * This is the model class for table "motivo".
 *
 * @property int $id
 * @property string $motivos

 *
 * @property Plantillas[] $plantillas
 */
class Motivo extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'motivo';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['motivos'], 'required'],
            [['motivos'], 'string', 'max' => 50],
         ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'motivos' => 'Motivos',

        ];
    }

    /**
     * Gets query for [[Plantillas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPlantillas()
    {
        return $this->hasMany(Plantillas::className(), ['motivo_plantilla' => 'id']);
    }
 
    
    
}
