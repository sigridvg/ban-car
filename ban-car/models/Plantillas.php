<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "plantillas".
 *
 * @property int $id
 * @property string $archivo
 * @property int $motivo_plantilla
 * @property string $nombre_archivo
 * @property int|null $sector
 * @property int|null $nombre
 * @property int|null $cuenta
 * @property int|null $cuil
 * @property int|null $sucursal
 * @property int|null $saldo
 * @property int|null $saldo_comision
 * @property int|null $fecha_emision
 * @property int|null $fecha_rechazo
 * @property int|null $fecha_inhabilitacion
 * @property int|null $fecha_cierre
 * @property int|null $motivo_cierre
 * @property int|null $cheque_diferido
 * @property int|null $cheque_comun
 * @property int|null $entidad
 * @property int|null $documentacion
 * @property int|null $cant_dias
 * @property int|null $saldo_letras
 * @property int|null $operacion
 * @property int|null $dias_letras
 * @property int|null $expediente
 * @property int|null $caratulado
 * @property int|null $letra
 * @property int|null $año_deuda
 * @property int|null $mes_deuda
 * @property int|null $cuotas
 * @property int|null $cuotas_vencidas
 * @property int|null $fecha_vencimiento
 * @property int|null $numero_multa
 * @property int|null $monto_garantia_letras
 * @property int|null $monto_garantia
 * @property int|null $tipo_garantia
 * @property int|null $motivo_rechazo
 * @property int|null $saldo_comision_letras
 * @property int|null $saldo_acreedor_letras
 * @property int|null $saldo_acreedor
 * @property int|null $saldo_deudor
 * @property int|null $saldo_deudor_letras
 * @property int|null $serie
 * @property int|null $a_favor
 * @property int|null $presentador
 * @property int|null $libradores
 * @property int|null $motivo
 * 
 * @property Motivo $motivoPlantilla
 */
class Plantillas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'plantillas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['archivo', 'motivo_plantilla', 'nombre_archivo'], 'required'],
            [['motivo_plantilla', 'sector', 'nombre', 'cuenta', 'cuil', 'sucursal', 'saldo', 'saldo_comision', 'fecha_emision', 'fecha_rechazo', 'fecha_inhabilitacion', 'fecha_cierre', 'motivo_cierre', 'cheque_diferido', 'cheque_comun', 'entidad', 'documentacion', 'cant_dias', 'saldo_letras', 'operacion', 'dias_letras', 'expediente', 'caratulado', 'letra', 'año_deuda', 'mes_deuda', 'cuotas', 'cuotas_vencidas', 'fecha_vencimiento', 'numero_multa', 'saldo_garantia_letras', 'saldo_garantia', 'tipo_garantia','motivo_rechazo', 'saldo_deudor',
                'saldo_acreedor','serie','libradores','presentador','a_favor','motivo'], 'integer'],
            [['archivo', 'nombre_archivo'], 'string', 'max' => 100],
            [['motivo_plantilla'], 'exist', 'skipOnError' => true, 'targetClass' => Motivo::className(), 'targetAttribute' => ['motivo_plantilla' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'archivo' => 'Archivo',
            'motivo_plantilla' => 'Motivo Plantilla',
            'nombre_archivo' => 'Nombre Archivo',
            'sector' => 'Sector',
            'nombre' => 'Nombre',
            'cuenta' => 'Cuenta',
            'cuil' => 'Cuil',
            'sucursal' => 'Sucursal',
            'saldo' => 'Saldo',
            'saldo_comision' => 'Saldo Comision',
            'fecha_emision' => 'Fecha Emision',
            'fecha_rechazo' => 'Fecha Rechazo',
            'fecha_inhabilitacion' => 'Fecha Inhabilitacion',
            'fecha_cierre' => 'Fecha Cierre',
            'motivo_cierre' => 'Motivo Cierre',
            'cheque_diferido' => 'Cheque Diferido',
            'cheque_comun' => 'Cheque Comun',
            'entidad' => 'Entidad',
            'documentacion' => 'Documentacion',
            'cant_dias' => 'Cant Dias',
            'saldo_letras' => 'Saldo Letras',
            'operacion' => 'Operacion',
            'dias_letras' => 'Dias Letras',
            'expediente' => 'Expediente',
            'caratulado' => 'Caratulado',
            'letra' => 'Letra',
            'año_deuda' => 'Año Deuda',
            'mes_deuda' => 'Mes Deuda',
            'cuotas' => 'Cuotas',
            'cuotas_vencidas' => 'Cuotas Vencidas',
            'fecha_vencimiento' => 'Fecha Vencimiento',
            'numero_multa' => 'Numero Multa',
            'saldo_garantia_letras' => 'Monto Garantia Letras',
            'saldo_garantia' => 'Saldo Garantia',
            'tipo_garantia' => 'Tipo Garantia',
            'motivo_rechazo' =>'Motivo de Rechazo',
            'saldo_comision' => 'Saldo Comision',
            'saldo_deudor' => 'Saldo Deudor',
            'saldo_deudor_letras' => 'Saldo Deudor Letras',
            'saldo_acreedor' => 'Saldo Acreedor',
            'saldo_acreedor_letras' => 'Saldo Acreedor Letras',
            'serie' => 'Serie',
            'libradores' => 'Librador/es',
            'presentador'=>'Presentador',
            'a_favor' =>'A favor de',
            'motivo' => 'Motivo',
        ];
    }

    /**
     * Gets query for [[MotivoPlantilla]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMotivoPlantilla()
    {
        return $this->hasOne(Motivo::className(), ['id' => 'motivo_plantilla']);
    }
    public function getName()
    {
        return $this->nombre_archivo;
    }
    public function getSectores()
    {
        return $this->hasOne(Sectores::className(), ['ID_SECTORES' => 'sector']);
    }
    public function getMotivos()
    {
        return $this->hasOne(Motivo::className(), ['id' => 'motivo_plantilla']);
    }
    
}
