<?php

namespace app\models;

use Yii;
use yii\base\Model;
use app\models\Usuarios;
use yii\db\ActiveRecord;
use yii\web\UploadedFile;

class RegistroForm extends ActiveRecord
{
    public $NombreUs;
    public $ApellidoUs;
    public $LegajoUs;
    public $usernameUs;
    public $passwordUs;
    public $emailUs;
    public $tiempo_creacion;
    public $ip_registrada;
    public $rememberMe = true;
    public $signature;


    public function rules()
    {
        return [
            //Requerido
            [['NombreUs', 'LegajoUs', 'usernameUs', 'emailUs', 'passwordUs', 'ApellidoUs', 'IdRoles'], 'required'],

            //No puede repetirse
            [['usernameUs'], 'unique'],
            [['LegajoUs'], 'unique'],
            [['emailUs'], 'unique'],

            //Tipo de Datos
            [['NombreUs', 'usernameUs', 'emailUs', 'passwordUs', 'ApellidoUs'], 'string'],
            [['emailUs'], 'email', 'message' => 'Formato no valido'],
            [['LegajoUs'], 'integer'],
            ['tiempo_creacion', 'time'],
            [['rememberMe'], 'boolean'],
            ['ip_registrada', 'ip'],

            //Max y Minimo
            [['usernameUs'], 'match', 'pattern' => '/^.{5,20}$/', 'message' => 'Minimo 5 y maximo 20 Caracteres'],
            [['emailUs'], 'match', 'pattern' => '/^.{20,80}$/', 'message' => 'Minimo 20 y maximo 80 Caracteres'],
            [['passwordUs'], 'match', 'pattern' => '/^.{5,20}$/', 'message' => 'Minimo 5 y maximo 20 Caracteres'],

            //Caracteres Especiales
            [['usernameUs'], 'match', 'pattern' => '/^[-a-zA-Z0-9]+$/', 'message' => 'Solo se aceptan numeros y letras'],
            ['emailUs', 'match', 'pattern' => '/^[-a-zA-Z_\0-9.@]+$/', 'message' => 'No se aceptan numeros'],

        ];
    }
    public function attributeLabels()
    {
        return [
            'NombreUs' => 'Nombre',
            'ApellidoUs' => 'Apellido',
            'LegajoUs' => 'Legajo',
            'usernameUs' => 'Username',
            'passwordUs' => 'Constraseña',
            'emailUs' => 'Email',
        ];
    }
    public static function getDB()
    {
        return Yii::$app->db;
    }
    public static function tableName()
    {
        return 'usuarios';
    }


}
