<?php

namespace app\models;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "tipo_destinatario".
 *
 * @property int $ID_TIPO
 * @property string $Tipo
 *
 * @property Destinatarios[] $destinatarios
 */
class TipoDestinatario extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tipo_destinatario';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ID_TIPO', 'Tipo'], 'required'],
            [['ID_TIPO'], 'integer'],
            [['Tipo'], 'string', 'max' => 50],
            [['ID_TIPO'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID_TIPO' => 'Id Tipo',
            'Tipo' => 'Tipo',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDestinatarios()
    {
        return $this->hasMany(Destinatarios::className(), ['ID_Tipo_DES' => 'ID_TIPO']);
    }
}
