<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Plantillas;
use Yii;

/**
 * plantillasSearch represents the model behind the search form of `app\models\plantillas`.
 */
class plantillasSearch extends Plantillas
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'sector', 'nombre', 'cuenta', 'cuil', 'sucursal', 'saldo', 'saldo_comision', 'fecha_emision', 'fecha_rechazo', 'fecha_inhabilitacion', 'fecha_cierre', 'motivo_cierre', 'cheque_diferido', 'cheque_comun','motivo_plantilla'], 'integer'],
            [['archivo'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        if (Usuarios::isUserAdmin(Yii::$app->user->identity->id)) {
            $query = plantillas::find();
        } else {
            $query = plantillas::find()->andFilterWhere(['sector' => Yii::$app->user->identity->getIdSectores()]);
        }
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
                      'id' => $this->id,
            'motivo_plantilla' => $this->motivo_plantilla,
            'sector' => $this->sector,
            'nombre' => $this->nombre,
            'cuenta' => $this->cuenta,
            'cuil' => $this->cuil,
            'sucursal' => $this->sucursal,
            'saldo' => $this->saldo,
            'saldo_comision' => $this->saldo_comision,
            'saldo_comision_letras' => $this->saldo_comision_letras,
            'fecha_emision' => $this->fecha_emision,
            'fecha_rechazo' => $this->fecha_rechazo,
            'fecha_inhabilitacion' => $this->fecha_inhabilitacion,
            'fecha_cierre' => $this->fecha_cierre,
            'motivo_cierre' => $this->motivo_cierre,
            'cheque_diferido' => $this->cheque_diferido,
            'cheque_comun' => $this->cheque_comun,
            'entidad' => $this->entidad,
            'documentacion' => $this->documentacion,
            'cant_dias' => $this->cant_dias,
            'saldo_letras' => $this->saldo_letras,
            'operacion' => $this->operacion,
            'dias_letras' => $this->dias_letras,
            'expediente' => $this->expediente,
            'caratulado' => $this->caratulado,
            'letra' => $this->letra,
            'año_deuda' => $this->año_deuda,
            'mes_deuda' => $this->mes_deuda,
            'cuotas' => $this->cuotas,
            'cuotas_vencidas' => $this->cuotas_vencidas,
            'fecha_vencimiento' => $this->fecha_vencimiento,
            'numero_multa' => $this->numero_multa,
            'saldo_garantia_letras' => $this->saldo_garantia_letras,
            'saldo_garantia' => $this->saldo_garantia,
            'tipo_garantia' => $this->tipo_garantia,
            'motivo_rechazo' => $this->motivo_rechazo,
        ]);

       $query->andFilterWhere(['like', 'archivo', $this->archivo])
            ->andFilterWhere(['like', 'nombre_archivo', $this->nombre_archivo]);

        return $dataProvider;
    }
}
