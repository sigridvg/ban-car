<?php

namespace app\models;
use Yii;
use yii\db\ActiveRecord;
use yii\base\model;

class RestablecerContra extends ActiveRecord
{
    public $passwordUs;
    public $password_repeat;
    public $codigo_verificacion;
    public $recover;

    public function rules()
    {
        return [
            [['passwordUs', 'password_repeat', 'codigo_verificacion', 'recover'], 'required', 'message' => 'Campo requerido'],
            ['passwordUs', 'match', 'pattern' => "/^.{6,16}$/", 'message' => 'Mínimo 6 y máximo 16 caracteres'],
            ['password_repeat', 'compare', 'compareAttribute' => 'passwordUs', 'message' => 'Los passwords no coinciden'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'passwordUs' => 'Nueva contraseña',
            'password_repeat' => 'Repetir nueva contraseña',
            'codigo_verificacion' => 'Codigo de verificacion'
        ];
    }
}
