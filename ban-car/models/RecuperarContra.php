<?php

namespace app\models;

use Yii;
use yii\base\Model;
use app\models\Usuarios;
use yii\db\ActiveRecord;
/**
 * 
 *
 * @property string $emailUs
 *
 */
class RecuperarContra extends ActiveRecord 
{
    public $emailUs;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            
            ['emailUs', 'email'],
            ['emailUs', 'required'],
        ];
    }
        /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'emailUs' => 'Email',
        ];
    }

    /**
     * Logs in a user using the provided username and password.
     * @return bool whether the user is logged in successfully
     */
    
//      public function actionRecuperarContra()
//     {
//         $msg = null;
//         $model = new RecuperarContra;

//         return $this->render('usuarios/RecuperarContra',['model'=>$model, 'msg' => $msg]);
// }

    public function randKey($str='', $long=0)
    {
        $key = null;
        $str = str_split($str);
        $start = 0;
        $limit = count($str)-1;
        for($x=0; $x<$long; $x++)
        {
            $key .= $str[rand($start, $limit)];
        }
        return $key;
    }
   
    
}
