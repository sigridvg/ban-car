<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Destinatarios;

/**
 * DestinatariosSearch represents the model behind the search form of `app\models\Destinatarios`.
 */
class DestinatariosSearch extends Destinatarios
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ID_DES', 'Numero_Casa_DES'], 'integer'],
            [['Nombre_RazonSocial_DES', 'Direccion_DES'], 'safe'],
            [['Cuit_Cuil_DES'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Destinatarios::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 5,
            ],

        ]);

        $this->load($params);

        if (!$this->validate()) {
            $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'ID_Tipo_DES' => $this->ID_Tipo_DES,
        ])
            ->andFilterWhere(['like', 'Cuit_Cuil_DES', $this->Cuit_Cuil_DES]);

        return $dataProvider;
    }
}
