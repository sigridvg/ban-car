<?php

namespace app\models;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "localidades".
 *
 * @property int $id
 * @property int|null $codigo_postal
 * @property string $nombre
 * @property int $id_provincia
 */
class Localidades extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'localidades';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo_postal'], 'integer'],
            [['nombre'], 'required'],
            [['nombre'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'codigo_postal' => 'Codigo Postal',
            'nombre' => 'Nombre',
        ];
    }
}
