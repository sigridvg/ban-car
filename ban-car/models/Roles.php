<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "roles".
 *
 * @property int $ID_ROLES
 * @property string $Roles
 *
 * @property FuncionesPerfilPermisos[] $funcionesPerfilPermisos
 * @property Permisos[] $pERMISOSs
 * @property User[] $users
 * @property Usuarios[] $usuarios
 */
class Roles extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'roles';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['Roles'], 'required'],
            [['Roles'], 'string', 'max' => 45],
            [['Roles'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID_ROLES' => 'Id Roles',
            'Roles' => 'Roles',
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getFuncionesPerfilPermisos()
    {
        return $this->hasMany(FuncionesPerfilPermisos::className(), ['ID_ROLES' => 'ID_ROLES']);
    }

    /**
     * @return ActiveQuery
     */
    public function getPERMISOS()
    {
        return $this->hasMany(Permisos::className(), ['ID_PERMISOS' => 'ID_PERMISOS'])->viaTable('funciones_perfil_permisos', ['ID_ROLES' => 'ID_ROLES']);
    }

    /**
     * @return ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(User::className(), ['ID_Perfil' => 'ID_ROLES']);
    }

    /**
     * @return ActiveQuery
     */
    public function getUsuarios()
    {
        return $this->hasMany(Usuarios::className(), ['IdRoles' => 'ID_ROLES']);
    }
}
