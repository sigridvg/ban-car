<?php 
namespace app\models;

use Yii;
use yii\base\Model;
use app\models\Usuarios;
/**
 * 
 */
class RegistroForm extends Model
{
	public $NombreUs;
	public $ApellidoUs;
    public $LegajoUs;
	public $usernameUs;
	public $passwordUs;
	public $emailUs;

	public function rules()
	{

		return [

			//Regla para usuarios
			['usernameUs','required','message' =>'Este campo es requerido'],
			['usernameUs', 'validarUsuario'],
			['usernameUs', 'string', 'min' => 5, 'max' => 255],
			['usernameUs','unique','message' => 'This username has already been taken'],

			//Reglas para Email
			['emailUs','required','message' =>'Este campo es requerido'],
			['emailUs', 'email'],
			['emailUs', 'validarEmail'],
			[['emailUs'],'unique','message' => 'This email has already been taken'],

			//Reglas para password
			['passwordUs','required','message' =>'Este campo es requerido'],

            ['LegajoUs','required','message' =>'Este campo es requerido'],
            [['LegajoUs'],'unique'],
];
	}
	 public function attributeLabels()
    {
        return [
        	'NombreUs'   => 'Nombre',
        	'ApellidoUs' =>	'Apellido',
            'LegajoUs'   => 'Legajo',
            'usernameUs' => 'Usuario',
            'passwordUs' => 'Contraseña',
            'emailUs'	 => 'Email',
        ];
    }

    public function formName()
    {
        return 'registro-form';
    }

    public function validarUsuario($attribute, $params)
    {
         $UsuariosCount = Usuarios::find()
                         ->where(['usernameUs'=>$usuario->usernameUs])
                        ->count();

                 if($UsuariosCount>0)
                  {
                    throw new NotFoundHttpException('Error.! Ya existe un Usuario con ese Nombre de Usuario.');
                  }
    }

    public function validarEmail()
    {
    	$UsuariosCount = Usuarios::find()
                         ->where(['emailUs'=>$usuario->emailUs])
                        ->count();

                 if($UsuariosCount>0)
                  {
                    throw new NotFoundHttpException('Error.! Ya existe un Usuario con este email.');
                  }
    }
    
    public function registrar()
    {
        if (!$this->validate()) {
            return false;
        }

        /** @var Usuarios $usuario */
        $usuario = Yii::createObject(Usuarios::className());
        $usuario->setScenario('registrar');
        $this->loadAttributes($usuaio);

        $usuario->password_hash=hash($usuario->passwordUs);
        $usuario->authKey=randKey();
        $usuario->tiempo_creacion=time();

        $ip=$this->setAttribute('ip_registrada', \Yii::$app->request->userIP);
        $usuario->ip_registrada=$ip;        

        if (!$user->registrar()) {
            return false;
        }

        Yii::$app->session->setFlash(
            'info',
            Yii::t(
                'user',
                'Your account has been created and a message with further instructions has been sent to your email'
            )
        );

    }
     public function getUser()
    {
        if ($this->_user === false) {
            $this->_user = Usuarios::findByUsername($this->usernameUs);
        }

        return $this->_user;
    }

}?>

///////////////////////////////////////////////////////////////////////

SITE CONTROLLER
public function actionRegistro()
        {
            $usuario = new Usuarios();
            $model= new RegistroForm();

            $post = Yii::$app->request->post();
            $usuarioLoaded = $usuario->load($post);

            // validate for ajax request
            if (Yii::$app->request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return RegistroForm::validate($usuario);
            }

            // vaidate for normal request
            if ($usuarioLoaded && $usuario->validate()) {
                $usuario->save();
                return $this->redirect('login');
            }

            // render
            return $this->render('registro', ['usuario' => $usuario,'model'=>$model]);
              
    }

    public function actionValidateEmail()
        {
        // validate for ajax request
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            $usuario= new Usuarios();
            $post = Yii::$app->request->post();
            $usuario->load($post);

            return RegistroForm::validate($usuario);
        }       
}
           
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// //Reglas para nombres y apellido
            // [['NombreUs'], 'required','message'=>'Campo requerido'],
            // [['NombreUs'], 'string', 'max' => 50],

            // //Reglas para Legajo
            // [['LegajoUs'],'required','message'=>'Campo requerido'],
            // [['LegajoUs'], 'integer'],
            // [['LegajoUs'], 'unique'],

            // //Reglas para Username
            // [['usernameUs'],'required','message'=>'Campo requerido'],
            // [['usernameUs'],'match', 'pattern' => '/^.{3,50}$/','message'=>'Minimo 3 y maximo 50 Caracteres'],
            // [['usernameUs'],'match','pattern'=>'/^.[0-9a-z]$/i','message'=>'Solo se aceptan numeros y letras'],
            // [['usernameUs'], 'unique'],

            // //Reglas para Email
            // [['emailUs'].'required','message'=>'Campo requerido'],
            // [['usernameUs'],'match', 'pattern' => '/^.{15,80}$/','message'=>'Minimo 15 y maximo 80 Caracteres'],
            // [['emailUs'],'email','message'=>'Formato no valido'],
            // [['emailUs'], 'unique'],

/////////////////////////////////////////////////////////////////////////////////////////////////////
            // if ($model_registro->load(Yii::$app->request->post()) && Yii::$app->request->isAjax) {
        //         Yii::$app->response->format = Response::FORMAT_JSON;
        //         ActiveForm::validate($model_registro);
        //     }

//////////////////////////////////////////////////////////////
        $ip=$usuarios->setAttribute('ip_registrada', \Yii::$app->request->userIP);
//////////////////////////////////////////////////////////////

$tabla_usuario= new Usuarios;
                    $tabla_usuario->NombreUs=$model_registro->NombreUs;
                    $tabla_usuario->ApellidoUs=$model_registro->ApellidoUs;
                    $tabla_usuario->LegajoUs=$model_registro->LegajoUs;
                    $tabla_usuario->emailUs=$model_registro->emailUs;
                    $tabla_usuario->usernameUs=$model_registro->usernameUs;
                    $tabla_usuario->passwordUs=$model_registro->passwordUs;
                    $tabla_usuario->tiempo_creacion=time();
                    //$tabla_usuario->password_hash=hash($model_registro->passwordUs, PASSWORD_DEFAULT);
                    //$tabla_usuario->authKey=randKey();
                    $tabla_usuario->save();

//////////////////////////////////////////////////////////////
                    $InsertArray[]=[
                       'NombreUs'=>$model_registro->NombreUs,
                       'ApellidoUs'=>$model_registro->ApellidoUs,
                       'LegajoUs'=>$model_registro->LegajoUs,
                       'usernameUs'=>$model_registro->usernameUs,
                       'emailUs'=>$model_registro->emailUs,
                       'passwordUs'=>$model_registro->passwordUs,
                       'tiempo_creacion'=>time(),

 //////////////////////////////////////////////////////////////////
 <?php
use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $content string */
?>

<header class="main-header">

        echo Html::a('<span class="logo-mini">APP</span><span class="logo-lg">' . Yii::$app->name . '</span>', Yii::$app->homeUrl, ['class' => 'logo']) ?>

    <nav class="navbar navbar-static-top" role="navigation">

        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>

        <div class="navbar-custom-menu">

            <ul class="nav navbar-nav">

                <!-- Messages: style can be found in dropdown.less-->
                <li class="dropdown messages-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-envelope-o"></i>
                        <span class="label label-success">44</span>
                    </a>
                    <ul class="dropdown-menu">
                        <li class="header">You have 7 messages</li>
                        <li>
                            <!-- inner menu: contains the actual data -->
                            <ul class="menu">
                                <li><!-- start message -->
                                    <a href="#">
                                        <div class="pull-left">
                                            <img src="<?= $directoryAsset ?>/img/user2-160x160.jpg" class="img-circle"
                                                 alt="User Image"/>
                                        </div>
                                        <h4>
                                            Support Team
                                            <small><i class="fa fa-clock-o"></i> 5 mins</small>
                                        </h4>
                                        <p>Why not buy a new awesome theme?</p>
                                    </a>
                                </li>
                                <!-- end message -->
                             </ul>   

                        </li>
                        <li class="footer">
                            <a href="#">View all tasks</a>
                        </li>
                    </ul>
                </li>
                <!-- User Account: style can be found in dropdown.less -->

                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <img src="<?= $directoryAsset ?>/img/user2-160x160.jpg" class="user-image" alt="User Image"/>
                        <span class="hidden-xs">Alexander Pierce</span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header">
                            <img src="<?= $directoryAsset ?>/img/user2-160x160.jpg" class="img-circle"
                                 alt="User Image"/>

                            <p>
                                Alexander Pierce - Web Developer
                                <small>Member since Nov. 2012</small>
                            </p>
                        </li>
                        <!-- Menu Body -->
                        <li class="user-body">
                            <div class="col-xs-4 text-center">
                                <a href="#">Followers</a>
                            </div>
                            <div class="col-xs-4 text-center">
                                <a href="#">Sales</a>
                            </div>
                            <div class="col-xs-4 text-center">
                                <a href="#">Friends</a>
                            </div>
                        </li>
                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="pull-left">
                                <a href="#" class="btn btn-default btn-flat">Profile</a>
                            </div>
                            <div class="pull-right">
                                <?= Html::a(
                                    'Sign out',
                                    ['/site/logout'],
                                    ['data-method' => 'post', 'class' => 'btn btn-default btn-flat']
                                ) ?>
                            </div>
                        </li>
                    </ul>
                </li>

                <!-- User Account: style can be found in dropdown.less -->
                <li>
                    <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
                </li>
            </ul>
        </div>
    </nav>
</header>
////////////////////////////////////////////////////////
 <?php NavBar::begin([  
        'options' => [
            'class'=>'navbar-inverse navbar-fixed-top',
        ],
    ]);
    $navItems=[1];
    if (Yii::$app->user->isGuest) {
        $bandera=1;
        do {
            array_shift($navItems);
            if (empty($navItems)) {
                $bandera=0;
                array_push($navItems,[
                    'label' => 'Login',

                    'url' => ['site/login']
                ],
                ['label' => 'Registrar',
                'url' => ['site/registro']
                ]
            );                 
             }
        } while ($bandera==1);
    
    } elseif (Yii::$app->user->identity->IdRoles==Usuarios::ROLE_ADMIN) {
        array_shift($navItems);
        array_push($navItems,
            [   
                'label' => 'Cartas Documento',
                'items' => [
                 [
                    'label' => 'Cartas Documento', 
                    'url' => ['/carta-documento']
                 ],
                 [
                    'label' => 'Crear Carta Documento', 
                    'url' => ['/carta-documento/create']
                 ],
                 [
                    'label' => 'Pendiente de Autorizacion',
                    'url' => '#'
                ],
                [
                    'label' => 'Pendiente de Despacho',
                    'url' => '#'
                ],
                [
                    'label' => 'Auditoria Carta Documento',
                    'url' => '#'
                ],
                ]
            ],    
            [
                
                'label' => 'Plantillas',
                'items' =>[
                    [
                        'label' => 'Plantillas',
                        'url' => ['/plantillas']
                    ],
                    [
                        'label' => 'Crear Plantilla',
                        'url' =>['/plantillas/create']
                    ],
                    [
                        'label' => 'Auditoria Plantillas',
                        'url' => '#'
                    ],
                    
                ],
            ],
            [
                'label' => 'Usuarios',
                'url' => ['#']
            ],
            [
                'label'=>'Destinatarios',
                'url' =>['/destinatarios']
            ],
            [
                'label' => 'Usuario activo: ' . Yii::$app->user->identity->usernameUs,
                'items' => [
                    [
                        'label' =>'Mi Cuenta',
                        'url' =>['site/Cuenta']
                    ],
                    [
                        'label' => 'Cerrar Sesion', 
                        'url' => ['site/logout'],
                        'linkOptions' => ['data-method' => 'post']
                    ],
               
                ],
            ]);
      }elseif (Yii::$app->user->identity->IdRoles==Usuarios::ROLE_FIRMANTE) {
        array_shift($navItems);
        array_push($navItems,
            [   
                
                'label' => 'Cartas Documento',
                'items' => [
                 [
                    'label' => 'Cartas Documento', 
                    'url' => ['/carta-documento']
                 ],
                 [
                    'label' => 'Crear Carta Documento', 
                    'url' => ['/carta-documento/create']
                 ],
                 [
                    'label' => 'Pendiente de Autorizacion',
                    'url' => '#'
                ],
                ]
            ],    
            [
                
                'label' => 'Plantillas',
                'items' =>[
                    [
                        'label' => 'Plantillas',
                        'url' => ['/plantillas']
                    ],
                    [
                        'label' => 'Crear Plantilla',
                        'url' =>['/plantillas/create']
                    ],
                    
                ],
            ],
            [
                'label' => 'Usuario activo: ' . Yii::$app->user->identity->usernameUs,
                'items' => [
                    [
                        'label' =>'Mi Cuenta',
                        'url' =>['site/Cuenta']
                    ],
                    [
                        'label' => 'Cerrar Sesion', 
                        'url' => ['site/logout'],
                        'linkOptions' => ['data-method' => 'post']
                    ],
               
                ],
            ]);
      }elseif (Yii::$app->user->identity->IdRoles==Usuarios::ROLE_OPERADOR) {
          array_shift($navItems);
        array_push($navItems,
            [   
                
                'label' => 'Cartas Documento',
                'items' => [
                 [
                    'label' => 'Cartas Documento', 
                    'url' => ['/carta-documento']
                 ],
                 [
                    'label' => 'Crear Carta Documento', 
                    'url' => ['/carta-documento/create']
                 ],

                 [
                     'label' => 'Pendiente de Despacho',
                     'url' => '#'
                 ],
                ]
            ],    
            [
                
                'label' => 'Plantillas',
                'items' =>[
                    [
                        'label' => 'Plantillas',
                        'url' => ['/plantillas']
                    ],
                    [
                        'label' => 'Crear Plantilla',
                        'url' =>['/plantillas/create']
                    ],                    
                ],
            ],
            [
                'label' => 'Usuario activo: ' . Yii::$app->user->identity->usernameUs,
                'items' => [
                    [
                        'label' =>'Mi Cuenta',
                        'url' =>['site/Cuenta']
                    ],
                    [
                        'label' => 'Cerrar Sesion', 
                        'url' => ['site/logout'],
                        'linkOptions' => ['data-method' => 'post']
                    ],
               
                ],
            ]);
      }
echo Nav::widget([
    'options' => ['class' => 'navbar-nav navbar-right'],
    'items' => $navItems,
]);
    NavBar::end();
    ?>
////////////////////////////////////////////////////////////////
[
                    ['label' => 'Menu Yii2', 'options' => ['class' => 'header']],
                    ['label' => 'Gii', 'icon' => 'file-code-o', 'url' => ['/gii']],
                    ['label' => 'Debug', 'icon' => 'dashboard', 'url' => ['/debug']],
                    ['label' => 'Login', 'url' => ['site/login'], 'visible' => Yii::$app->user->isGuest],
                    [
                        'label' => 'Some tools',
                        'icon' => 'share',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Gii', 'icon' => 'file-code-o', 'url' => ['/gii'],],
                            ['label' => 'Debug', 'icon' => 'dashboard', 'url' => ['/debug'],],
                            [
                                'label' => 'Level One',
                                'icon' => 'circle-o',
                                'url' => '#',
                                'items' => [
                                    ['label' => 'Level Two', 'icon' => 'circle-o', 'url' => '#',],
                                    [
                                        'label' => 'Level Two',
                                        'icon' => 'circle-o',
                                        'url' => '#',
                                        'items' => [
                                            ['label' => 'Level Three', 'icon' => 'circle-o', 'url' => '#',],
                                            ['label' => 'Level Three', 'icon' => 'circle-o', 'url' => '#',],
                                        ],
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
////////////////////////////////////////////////////////////////

<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\Usuarios;

/* @var $this yii\web\View */
/* @var $searchModel app\models\UsuariosSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Usuarios';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="usuarios-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Usuarios', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'IdUsuarios',
            'NombreUs',
            'LegajoUs',
            'IdRoles',
            'IdSectores',
            //'usernameUs',
            //'passwordUs',
            //'emailUs:email',
            //'authKey',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>

//////////////////////////////////////////////////////////////
public function actionResetContra()
        {
            //Instancia para validar el formulario
            $model = new ResetContraForm;
            $this->layout = 'main-login';
            //Mensaje que será mostrado al usuario
            $msg = null;
  
            //Abrimos la sesión
            $session = new Session;
            $session->open();
              
            //Si no existen las variables de sesión requeridas lo expulsamos a la página de inicio
            if (empty($session["recover"]) || empty($session["id_recover"]))
                {         
                    return $this->redirect(["site/index"]);
                }
                else
                {
                   $recover = $session["recover"];
                   //El valor de esta variable de sesión la cargamos en el campo recover del formulario
                   $model->recover = $recover;
                   
                   //Esta variable contiene el id del usuario que solicitó restablecer el password
                   //La utilizaremos para realizar la consulta a la tabla users
                   $id_recover = $session["id_recover"];
                   
                  }
  
          //Si el formulario es enviado para resetear el password
          if ($model->load(Yii::$app->request->post()))
          {
           if ($model->validate())
           {
            //Si el valor de la variable de sesión recover es correcta
            if ($recover == $model->recover)
            {
             //Preparamos la consulta para resetear el password, requerimos el email, el id 
             //del usuario que fue guardado en una variable de session y el código de verificación
             //que fue enviado en el correo al usuario y que fue guardado en el registro
             $table = Usuarios::findOne(["emailUs" => $model->emailUs, "IdUsuarios" => $id_recover, "codigo_verificacion" => $model->codigo_verificacion]);
             
             //Encriptar el password
             $model->password_hash = Yii::$app->getSecurity()->generatePasswordHash($model->password);
             
                //Si la actualización se lleva a cabo correctamente
                if ($table->save())
                    {
      
                    //Destruir las variables de sesión
                    $session->destroy();
      
                  //Vaciar los campos del formulario
                  $model->emailUs = null;
                  $model->password = null;
                  $model->password_repeat = null;
                  $model->recover = null;
                  $model->codigo_verificacion = null;
                  
                  $msg = "Enhorabuena, password reseteado correctamente, redireccionando a la página de login ...";
                  $msg .= "<meta http-equiv='refresh' content='5; ".Url::toRoute("site/login")."'>";
                }
                else
                {
                    $msg = "Ha ocurrido un error";
                }
     
            }
            else
            {
                $model->getErrors();
            }
        }
  }
  
        return $this->render("restaurar_contraseña", ["model" => $model, "msg" => $msg]);
  
 }   

//////////////////////////////////////////////////////////////
//Instancia para validar el formulario
        $model = new RecuperarContra;
  
        //Mensaje que será mostrado al usuario en la vista
        $msg = null;
  
        if ($model->load(Yii::$app->request->post()))
        {
            if ($model->validate())
                {
            //Buscar al usuario a través del email
                    $table = Usuarios::find()->where("emailUs=:emailUs", [":emailUs" => $model->emailUs]);
    
            //Si el usuario existe
                if ($table->count() == 1)
                    {
                    //Crear variables de sesión para limitar el tiempo de restablecido del password
                    //hasta que el navegador se cierre
                    $session = new Session;
                    $session->open();
     
                    //Esta clave aleatoria se cargará en un campo oculto del formulario de reseteado
                    $session["recover"] = $this->randKey("abcdef0123456789", 200);
                    $recover = $session["recover"];
     
                    //También almacenaremos el id del usuario en una variable de sesión
                    //El id del usuario es requerido para generar la consulta a la tabla users y 
                    //restablecer el password del usuario
                    $table = Usuarios::find()->where("emailUs=:emailUs", [":emailUs" => $model->emailUs])->one();
                    $session["id_recover"] = $table->id;
     
                    //Esta variable contiene un número hexadecimal que será enviado en el correo al usuario 
                    //para que lo introduzca en un campo del formulario de reseteado
                    //Es guardada en el registro correspondiente de la tabla users
                    $codigo_verificacion = $this->randKey("abcdef0123456789", 8);
                    //Columna verification_code
                    $table->codigo_verificacion = $codigo_verificacion;
                    //Guardamos los cambios en la tabla users
                    $table->save();
     
                    //Creamos el mensaje que será enviado a la cuenta de correo del usuario
                    $subject = "Recuperar Contraseña";
                    $body = "<p>Copie el siguiente código de verificación para restablecer su password ... ";
                    $body .= "<strong>".$codigo_verificacion."</strong></p>";
                    $body .= "<p><a href='http://yii.local/index.php?r=site/resetpass'>Recuperar password</a></p>";

                    //Enviamos el correo
                    Yii::$app->mailer->compose()
                    ->setTo($model->emailUs)
                    ->setFrom([Yii::$app->params["adminEmail"] => Yii::$app->params["title"]])
                    ->setSubject($subject)
                    ->setHtmlBody($body)
                    ->send();
     
                    //Vaciar el campo del formulario
                    $model->emailUs = null;
             
                    //Mostrar el mensaje al usuario
                    $msg = "Le hemos enviado un mensaje a su cuenta de correo para que pueda resetear su password";
                 }
                else //El usuario no existe
                    {
                        $msg = "Ha ocurrido un error";
                    }
                }
                else
                    {
                    $model->getErrors();
                    }
            }
            return $this->render("recuperar_contraseña", ["model" => $model, "msg" => $msg]);
        }
    
//////////////////////////////////////////////////////////////
 public function actionView($id) { if (Yii::$app->request->isAjax) { return $this->renderAjax('view', [ 'model' => $this->findModel($id), ]); } else { return $this->render('view', [ 'model' => $this->findModel($id), ]); } } 

 /////////////////////////////////////////////////////////////
  < ?= GridView::widget([ 'dataProvider' => $dataProvider, 'filterModel' => $searchModel, 'columns' => [ ................... [ 'class' => 'yii\grid\ActionColumn', 'buttons' => [ 'view' => function ($url, $model) { return Html::a('', $url , ['class' => 'view', 'data-pjax' => '0']); }, ], .................... ]); $this->registerJs( "$(document).on('ready pjax:success', function() { // 'pjax:success' use if you have used pjax $('.view').click(function(e){ e.preventDefault(); $('#pModal').modal('show') .find('.modal-content') .load($(this).attr('href')); }); }); "); yii\bootstrap\Modal::begin([ 'id'=>'pModal', ]); yii\bootstrap\Modal::end(); ?> 

  <script type="text/javascript">
            $(function(){
               $('#linkModal').click(funcition(){
                    $('#myModal').modal('show')
                    .find('#modalContent')
                    .load($(this).attr('value'));
               });      
            });
        </script>

         public function actionView($id) { 
            if (Yii::$app->request->isAjax) { 
                return $this->renderAjax('view', [ 'model' => $this->findModel($id), ]); 
            } else { 
                return $this->render('view', [ 'model' => $this->findModel($id), ]); 
            } 
        } 

        <?php// echo Html::a('Recuperar Contraseña','', ["onclick"=>'$("#myModal").dialog("open"); return false;']) ?>

//////////////////////////////////////////////////////////////

 <?php echo Html::a('<span class="glyphicon glyphicon-comment"> Recuperar Contraseña</span>',
                    ['/RecuperarContra'], 
                    [
                        'title' => 'Recuperar Contraseña',
                        'data-toggle'=>'modal',
                        'data-target'=>'#myModal',
                    ]
                   );

?>

if ($model->load(Yii::$app->request->post())) {
            $modelRC->attributes($_POST['RecuperarContra']);
                if(!$modelRC->validate()){
                   $msg=Yii::$app->session->setFlash(
                    'info',
                    Yii::t('app',
                        'No se pudo cargar el email'));
                }
                else{
                    //enviar mail
                }
            }else{
                $msg='Error';
            }
        return Yii::$app->controller->renderPartial('RecuperarContra',['model' => $modelRC, 'msg'=>$msg,
        ]);

$modelRC = new RecuperarContra();

        $msg=null;

        if ($modelRC->load(Yii::$app->request->post()) && $modelRC->save()){
            $msg=Yii::$app->session->setFlash('info',Yii::t('app','No se pudo cargar el email'));
                
        }else{
                $msg='Error';
        }
        //return $this->renderAjax('RecuperarContra',['model' => $modelRC, 'msg'=>$msg,]);
        return Yii::$app->controller->renderPartial('RecuperarContra',['modelRC' => $modelRC,'msg'=>$msg]);
//////////////////////////////////////////////////////////////


        <?php 
       //  Modal::begin([
       //      'header'=>'<h4>Recuperar Contraseña</h4>',
       //      'id' => 'myModal',
       //      'size' => 'modal-dialog',
       //  ]);
        
       // echo '<div id="modalContent"></div>';
        
       //  Modal::end();
        ?>

        <!--<a href="?r=usuarios/RecuperarContra" data-toggle="modal" data-target="#myModal" id="linkModal">Recuperar Contraseña</a>-->
<?php
        $this->registerJs(
            "$(document).on('click', '#modal', (function() {
            $.get(
                $(this).data('url'),
            function (data) {
                $('.modal-body').html(data);
                $('#modal').modal();
                }
             );
            }));"
        ); ?>



$this->registerJs('
    // obtener la id del formulario y establecer el manejador de eventos
        $("form#modal-recuperar-contra").on("beforeSubmit", function(e) {
            var form = $(this);
            $.post(
                form.attr("action")+"&submit=true",
                form.serialize()
            )
            .done(function(result) {
                form.parent().html(result.message);
                $.pjax.reload({container:"#solicitante-grid"});
            });
            return false;
        }).on("submit", function(e){
            e.preventDefault();
            e.stopImmediatePropagation();
            return false;
        });
    ');
?>

//echo Yii::$app->controller->renderPartial('RecuperarContra',['model' => $modelRC, 'msg'=>$msg,]);
//////////////////////////////////////////////////////////////
public function actionRecoverpass()
 {
  //Instancia para validar el formulario
  $model = new FormRecoverPass;
  
  //Mensaje que será mostrado al usuario en la vista
  $msg = null;
  
  if ($model->load(Yii::$app->request->post()))
  {
   if ($model->validate())
   {
    //Buscar al usuario a través del email
    $table = Users::find()->where("email=:email", [":email" => $model->email]);
    
    //Si el usuario existe
    if ($table->count() == 1)
    {
     //Crear variables de sesión para limitar el tiempo de restablecido del password
     //hasta que el navegador se cierre
     $session = new Session;
     $session->open();
     
     //Esta clave aleatoria se cargará en un campo oculto del formulario de reseteado
     $session["recover"] = $this->randKey("abcdef0123456789", 200);
     $recover = $session["recover"];
     
     //También almacenaremos el id del usuario en una variable de sesión
     //El id del usuario es requerido para generar la consulta a la tabla users y 
     //restablecer el password del usuario
     $table = Users::find()->where("email=:email", [":email" => $model->email])->one();
     $session["id_recover"] = $table->id;
     
     //Esta variable contiene un número hexadecimal que será enviado en el correo al usuario 
     //para que lo introduzca en un campo del formulario de reseteado
     //Es guardada en el registro correspondiente de la tabla users
     $verification_code = $this->randKey("abcdef0123456789", 8);
     //Columna verification_code
     $table->verification_code = $verification_code;
     //Guardamos los cambios en la tabla users
     $table->save();
     
     //Creamos el mensaje que será enviado a la cuenta de correo del usuario
     $subject = "Recuperar password";
     $body = "<p>Copie el siguiente código de verificación para restablecer su password ... ";
     $body .= "<strong>".$verification_code."</strong></p>";
     $body .= "<p><a href='http://yii.local/index.php?r=site/resetpass'>Recuperar password</a></p>";

     //Enviamos el correo
     Yii::$app->mailer->compose()
     ->setTo($model->email)
     ->setFrom([Yii::$app->params["adminEmail"] => Yii::$app->params["title"]])
     ->setSubject($subject)
     ->setHtmlBody($body)
     ->send();
     
     //Vaciar el campo del formulario
     $model->email = null;
     
     //Mostrar el mensaje al usuario
     $msg = "Le hemos enviado un mensaje a su cuenta de correo para que pueda resetear su password";
    }
    else //El usuario no existe
    {
     $msg = "Ha ocurrido un error";
    }
   }
   else
   {
    $model->getErrors();
   }
  }
  return $this->render("recoverpass", ["model" => $model, "msg" => $msg]);
 }
 ///////////////////////////////////////////////////////////
 public function actionResetpass()
 {
  //Instancia para validar el formulario
  $model = new FormResetPass;
  
  //Mensaje que será mostrado al usuario
  $msg = null;
  
  //Abrimos la sesión
  $session = new Session;
  $session->open();
  
  //Si no existen las variables de sesión requeridas lo expulsamos a la página de inicio
  if (empty($session["recover"]) || empty($session["id_recover"]))
  {
   return $this->redirect(["site/index"]);
  }
  else
  {
   
   $recover = $session["recover"];
   //El valor de esta variable de sesión la cargamos en el campo recover del formulario
   $model->recover = $recover;
   
   //Esta variable contiene el id del usuario que solicitó restablecer el password
   //La utilizaremos para realizar la consulta a la tabla users
   $id_recover = $session["id_recover"];
   
  }
  
  //Si el formulario es enviado para resetear el password
  if ($model->load(Yii::$app->request->post()))
  {
   if ($model->validate())
   {
    //Si el valor de la variable de sesión recover es correcta
    if ($recover == $model->recover)
    {
     //Preparamos la consulta para resetear el password, requerimos el email, el id 
     //del usuario que fue guardado en una variable de session y el código de verificación
     //que fue enviado en el correo al usuario y que fue guardado en el registro
     $table = Users::findOne(["email" => $model->email, "id" => $id_recover, "verification_code" => $model->verification_code]);
     
     //Encriptar el password
     $table->password = crypt($model->password, Yii::$app->params["salt"]);
     
     //Si la actualización se lleva a cabo correctamente
     if ($table->save())
     {
      
      //Destruir las variables de sesión
      $session->destroy();
      
      //Vaciar los campos del formulario
      $model->email = null;
      $model->password = null;
      $model->password_repeat = null;
      $model->recover = null;
      $model->verification_code = null;
      
      $msg = "Enhorabuena, password reseteado correctamente, redireccionando a la página de login ...";
      $msg .= "<meta http-equiv='refresh' content='5; ".Url::toRoute("site/login")."'>";
     }
     else
     {
      $msg = "Ha ocurrido un error";
     }
     
    }
    else
    {
     $model->getErrors();
    }
   }
  }
  
  return $this->render("resetpass", ["model" => $model, "msg" => $msg]);
  
 }
 ////////////////////////////////////////////////////////////
 //Enviamos el correo
                        // Yii::$app->mailer->compose()
                        // ->setTo($modelRC->emailUs)
                        // ->setFrom([Yii::$app->params["adminEmail"] => Yii::$app->params["title"]])
                        // ->setSubject($subject)
                        // ->setHtmlBody($body)
                        // ->send();

/////////////////////////////////////////////////////////////

$this->title = 'RECUPERAR CONTRASEÑA';
?>
<body class="login-page">
     <div class="login-logo">
        <img src="../web/BAN.CAR2.png" class="rounded d-block" style="max-height:60px;"><br>
        <a href="#"><b>BAN</b>CAR</a>
    </div>

<?php if (Yii::$app->session->hasFlash('error')){ ?>
    <div class="alert alert-danger">
         <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
         <h4><i class="icon fa fa-times"></i>Error</h4>
         <?= Yii::$app->session->getFlash('error') ?>
     </div>
    
<?php };?>

<div class="row">
    <div class="col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h1 class="panel-title col-lg-offset-5"><?= Html::encode($this->title) ?></h1>
            </div>
            
                <div class="panel-body">
                <?php $form = ActiveForm::begin([
                    'id' => 'recuperar-contra-form',
                    'method' =>'post',
                    'enableClientValidation' => true,
                    'enableAjaxValidation' => true,
                ]); ?>
                
                <div class="form-group col-md-12" >   
                    <?=$form->field($modelRC, 'emailUs')->textInput()?>
                </div>
                
                    <div class="form-group">
                        <div class="col-lg-offset-2 col-lg-7">
                            <?= Html::submitButton('Enviar', ['class' => 'btn btn-warning btn-block', 'name' => 'recuperar-contra <div class="form-group"></div>-button']) ?>
                        </div>
                    </div>
                    </div>

        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>


</body>
</html>

/////////////////////////////////////////////
[
    'attribute' => 'coefTK',
    'label' => '<abbr title="Koefisien Jumlah Tenaga Kerja">TK</abbr>',
    'encodeLabel' => false,
    'headerOptions' => ['style'=>'text-align:center'],
    'contentOptions' => function ($model, $key, $index, $column) {
        return ['style' => 'background-color:' 
            . (!empty($model->coefTK_se) && $model->coefTK / $model->coefTK_se < 2
                ? 'red' : 'blue')];
    },
],
///////////////////////////////////////////////////////
/*echo substr($archivo, 0).'<br>';
                echo substr($archivo, 72).'<br>';
                echo substr($archivo, 84).'<br>';
                echo substr($archivo, 85).'<br>';
                echo substr($archivo, 144).'<br>';*/
            
           // 
            //if(!$file){
            //    echo "No se encontro el archivo";
            //}else{
            //    $archivo = fgets($file);
            //    if(!$archivo){
            //        echo "Archivo Vacio";   
            //    }else{
            //        echo $archivo;  
            //    }
            //}    

            //if($file){
                //$file->saveAs('/../web/sideba/tmp.txt');
            //}

                //$model->file->saveAs('sideba/' . $model->file->baseName . '.' . $model->file->extension);
                //return $this->refresh();
//          }

        //return $this->render('carga', ['model' => $model]);


         <? Html::a('Cargar Destinatarios desde SIDEBA', ['carga'], ['class' => 'btn btn-warning']) ?>
////////////////////////////////////////////////////////

< ?= GridView::widget([ 'dataProvider' => $dataProvider, 'filterModel' => $searchModel, 'columns' => [ ................... [ 'class' => 'yii\grid\ActionColumn', 'buttons' => [ 'view' => function ($url, $model) { return Html::a('', $url , ['class' => 'view', 'data-pjax' => '0']); }, ], .................... ]); $this->registerJs( "$(document).on('ready pjax:success', function() { // 'pjax:success' use if you have used pjax $('.view').click(function(e){ e.preventDefault(); $('#pModal').modal('show') .find('.modal-content') .load($(this).attr('href')); }); }); "); yii\bootstrap\Modal::begin([ 'id'=>'pModal', ]); yii\bootstrap\Modal::end(); ?> 


<div id="modalContent"> 
<?php
if (Yii::$app->session->hasFlash('carga')){ ?>
        <div class="alert alert-success">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <?= Yii::$app->session->getFlash('carga'); ?>
        </div>
<?php }; ?>
</div>
////////////////////////////////////////////////////////

TipoDestinatario::findOne($model->ID_Tipo_DES)->Tipo

////////////////////////////////////////////////////////
<?php 
// Bootstrap Table - detail view without plus icon, to answer https://github.com/wenzhixin/bootstrap-table/issues/964, uses bootstrap-table.10.1.min.js, simple handler, simple css


/* Latest compiled and minified JavaScript included as External Resource */

var $table = $('#myTable');

$table.on('expand-row.bs.table', function(e, index, row, $detail) {
  var res = $("#desc" + index).html();
  $detail.html(res);
});

$table.on("click-row.bs.table", function(e, row, $tr) {

  // prints Clicked on: table table-hover, no matter if you click on row or detail-icon
  console.log("Clicked on: " + $(e.target).attr('class'), [e, row, $tr]);

  // In my real scenarion, trigger expands row with text detailFormatter..
  //$tr.find(">td>.detail-icon").trigger("click");
  // $tr.find(">td>.detail-icon").triggerHandler("click");
  if ($tr.next().is('tr.detail-view')) {
    $table.bootstrapTable('collapseRow', $tr.data('view'));
  } else {
    $table.bootstrapTable('expandRow', $tr.data('id'));
  }
});
 ?>
 