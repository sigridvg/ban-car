<?php

namespace app\models;

use Yii;
use yii\base\Model;
use app\models\Usuarios;
use yii\web\Session;

/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class LoginForm extends Model
{
    const DEFAULT_PASS = "usuario_bancar";

    public $usernameUs;
    public $passwordUs;
    public $IdUsuario;
    public $rememberMe = true;
    private $_user = false;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['usernameUs', 'passwordUs'], 'required'],
            // rememberMe must be a boolean value
            ['rememberMe', 'boolean'],
            // password is validated by validatePassword()
            ['passwordUs','validatePassword'],
        ];
    }
        /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'usernameUs' => 'Usuario',
            'passwordUs' => 'Contraseña',
            'rememberMe' => 'Recuerdame',
        ];
    }

    public function isFirstLogin(){
        $user = $this->getUser();

        if (isset($user) && Yii::$app->security->validatePassword(self::DEFAULT_PASS, $this->user->password_hash)) {
            return true;
        }

        return false;
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword()
    {
        $user = $this->getUser();
        
        if (isset($user) && Yii::$app->security->validatePassword($this->passwordUs, $this->user->password_hash)) {
              return true;
        }
        
        return false;
    }

    /**
     * Logs in a user using the provided username and password.
     * @return bool whether the user is logged in successfully
     */
    public function login()
    {

        if ($this->user && $this->validatePassword()) {
            
            $logeado= Yii::$app->user->login($this->getUser(), $this->rememberMe ? 3600*24*30 : 0);
            if ($logeado) {
                $this->user->updateAttributes(['tiempo_ultimo_logueo' => date('d-M-Y H:i:s')]);
                
            }

            return $logeado;
        }
        return false;
    }

    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    public function getUser()
    {
        if ($this->_user === false) {
            $usuario = Usuarios::findByUsername($this->usernameUs);
        }

        return $usuario;
    }
    public function randKey($str='', $long=0)
    {
        $key = null;
        $str = str_split($str);
        $start = 0;
        $limit = count($str)-1;
        for($x=0; $x<$long; $x++)
        {
            $key .= $str[rand($start, $limit)];
        }
        return $key;
    }
    public function tiempo_expiracion()
    {
        
    }

}
