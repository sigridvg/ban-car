<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Destinatarios;

/**
 * destinatariosSearch represents the model behind the search form of `app\models\destinatarios`.
 */
class destinatariosSearch extends Destinatarios
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ID_DES', 'DNI', 'Tipo_documento', 'Cuit_Cuil', 'Fecha_Nacimiento', 'Cod_postal', 'Cod_actividad', 'Sucursal', 'Cuenta', 'Codigo_pais', 'ID_Provincia', 'ID_Localidad', 'Tipo_domicilio'], 'integer'],
            [['Nombre_RazonSocial', 'Sexo', 'Ciudadania', 'Telefono', 'Celular', 'Direccion', 'Email', 'Estado_civil', 'Grupo', 'Subgrupo', 'Tipo_cuenta'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = destinatarios::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'ID_DES' => $this->ID_DES,
            'DNI' => $this->DNI,
            'Tipo_documento' => $this->Tipo_documento,
            'Cuit_Cuil' => $this->Cuit_Cuil,
            'Fecha_Nacimiento' => $this->Fecha_Nacimiento,
            'Cod_postal' => $this->Cod_postal,
            'Cod_actividad' => $this->Cod_actividad,
            'Sucursal' => $this->Sucursal,
            'Cuenta' => $this->Cuenta,
            'Codigo_pais' => $this->Codigo_pais,
            'ID_Provincia' => $this->ID_Provincia,
            'ID_Localidad' => $this->ID_Localidad,
            'Tipo_domicilio' => $this->Tipo_domicilio,
        ]);

        $query->andFilterWhere(['like', 'Nombre_RazonSocial', $this->Nombre_RazonSocial])
            ->andFilterWhere(['like', 'Sexo', $this->Sexo])
            ->andFilterWhere(['like', 'Ciudadania', $this->Ciudadania])
            ->andFilterWhere(['like', 'Telefono', $this->Telefono])
            ->andFilterWhere(['like', 'Celular', $this->Celular])
            ->andFilterWhere(['like', 'Direccion', $this->Direccion])
            ->andFilterWhere(['like', 'Email', $this->Email])
            ->andFilterWhere(['like', 'Estado_civil', $this->Estado_civil])
            ->andFilterWhere(['like', 'Grupo', $this->Grupo])
            ->andFilterWhere(['like', 'Subgrupo', $this->Subgrupo])
            ->andFilterWhere(['like', 'Tipo_cuenta', $this->Tipo_cuenta]);

        return $dataProvider;
    }
}
