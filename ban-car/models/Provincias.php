<?php

namespace app\models;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "provincia".
 *
 * @property int $ID_PROV
 * @property string $Provincia
 *
 */
class Provincias extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'provincias';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ID_PROV', 'Provincia', 'Codigo_Postal'], 'required'],
            [['ID_PROV'], 'integer'],
            [['Provincia'], 'string', 'max' => 255],
            [['ID_PROV'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID_PROV' => 'Id Prov',
            'Provincia' => 'Provincia',
        ];
    }
}
